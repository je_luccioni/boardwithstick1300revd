EESchema Schematic File Version 2
LIBS:74LVT
LIBS:74xgxx
LIBS:74xx
LIBS:ac-dc
LIBS:actel
LIBS:adc-dac
LIBS:ADC-DeltaSigma
LIBS:ADC-SAR
LIBS:Altera
LIBS:analog_devices
LIBS:AnalogMuxDemux
LIBS:analog_switches
LIBS:AopAudio
LIBS:AopComparador
LIBS:AopPrecision
LIBS:AopRailToRailPG
LIBS:atmel
LIBS:audio
LIBS:battery_management
LIBS:bbd
LIBS:BJT-GeneralPurpose 
LIBS:BOR-SIL
LIBS:BridgeUSB
LIBS:brooktre
LIBS:CapAluminioSMD
LIBS:CapCeramicoHVC
LIBS:CapCeramicos
LIBS:CapCer_THT
LIBS:CapElectroliticos
LIBS:CapPoly_THT
LIBS:CapTantalio
LIBS:Ccer_SMD
LIBS:cmos4000
LIBS:cmos_ieee
LIBS:CommonModeChoke 
LIBS:CompuertasLogicas
LIBS:CON-DIL_F
LIBS:CON-DIL_M
LIBS:Conectores
LIBS:ConectoresDB
LIBS:conn
LIBS:CON-SIL_F
LIBS:CON-SIL_M
LIBS:contrib
LIBS:CON-USB
LIBS:Cristal
LIBS:cypress
LIBS:dc-dc
LIBS:device
LIBS:digital-audio
LIBS:diode
LIBS:DiodoPequeñaSeñal
LIBS:DiodoRectificador
LIBS:DiodoSchottkyRect
LIBS:DiodosSupresorTransitorios
LIBS:DiodosZener
LIBS:display
LIBS:dsp
LIBS:EEpromSerie
LIBS:EIA232
LIBS:EIA485
LIBS:elec-unifil
LIBS:ESD_Protection
LIBS: EsdProtectionDiode
LIBS:FerriteBead
LIBS:FotoDiodos
LIBS:ftdi
LIBS:Fusibles
LIBS:gennum
LIBS:graphic
LIBS:hc11
LIBS:Inductor
LIBS:intel
LIBS:interface
LIBS:ir
LIBS:IRF-MOSFET
LIBS:jumpers
LIBS:Lattice
LIBS:LDR_THT
LIBS:LEDs
LIBS:linear
LIBS:LoadGeneric
LIBS:Logic-MOSFET
LIBS:logo
LIBS:LPC17xx
LIBS:LPC43xx
LIBS:MagneticsProtection
LIBS:maxim
LIBS:memory
LIBS:microchip
LIBS:microchip_dspic33dsc
LIBS:microchip_pic10mcu
LIBS:microchip_pic12mcu
LIBS:microchip_pic16mcu
LIBS:microchip_pic18mcu
LIBS:microchip_pic32mcu
LIBS:microcontrollers
LIBS:motor_drivers
LIBS:motorola
LIBS:msp430
LIBS:nordicsemi
LIBS:NTR-MOSFET
LIBS:nxp_armmcu
LIBS:onsemi
LIBS:opto
LIBS:OptoTransistor
LIBS:OptoTriac
LIBS:Oscillators
LIBS:philips
LIBS:PicoFusePTC
LIBS:Pilas
LIBS:Potenciometro
LIBS:power
LIBS:PowerDistributionSwitch
LIBS:powerint
LIBS:Power_Management
LIBS:pspice
LIBS:Puentes
LIBS:R
LIBS:references
LIBS:RegLinealPositivos
LIBS:regul
LIBS:ReguladorLDO
LIBS:ReguladorSwitcher
LIBS:relays
LIBS:rfcom
LIBS:RMII_Ethernet
LIBS:R_SMD
LIBS:R_ThruHole
LIBS:R_THT
LIBS:SensorCurrent
LIBS:sensors
LIBS:SensorTemperatura
LIBS:ShifterLevel
LIBS:silabs
LIBS:siliconi
LIBS:SourceCurrent
LIBS:StickLPC
LIBS:stm8
LIBS:stm32
LIBS:supertex
LIBS:switches
LIBS:TactSwitch
LIBS:TestPoint
LIBS:texas
LIBS:TrafoReductor
LIBS:TransceiverCAN
LIBS:transf
LIBS:TransistorArrays
LIBS:transistors
LIBS:Triac
LIBS:ttl_ieee
LIBS:valves
LIBS:Varistor
LIBS:video
LIBS:VoltageReference
LIBS:Worldsemi
LIBS:Xicor
LIBS:xilinx
LIBS:xtal
LIBS:Zilog
LIBS:MemoryZock
LIBS:BoardMicro-cache
EELAYER 25 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 6 21
Title "BoardWithStick1300revD"
Date "12-05-2017"
Rev "v1.10b"
Comp "J.E.L"
Comment1 "\\n Luccioni, Jesus Emanuel"
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L GND #PWR050
U 1 1 5729CE75
P 3200 3050
F 0 "#PWR050" H 3200 2800 50  0001 C CNN
F 1 "GND" H 3200 2900 50  0000 C CNN
F 2 "" H 3200 3050 50  0000 C CNN
F 3 "" H 3200 3050 50  0000 C CNN
	1    3200 3050
	1    0    0    -1  
$EndComp
Wire Wire Line
	2950 2950 3200 2950
Connection ~ 3200 2950
Wire Wire Line
	2950 2300 2950 2450
Wire Wire Line
	3200 2300 3200 2550
Connection ~ 3200 2350
Wire Wire Line
	3200 2750 3200 3050
$Comp
L +3V3 #PWR051
U 1 1 5729CE83
P 3200 1800
F 0 "#PWR051" H 3200 1650 50  0001 C CNN
F 1 "+3V3" H 3200 1950 39  0000 C CNN
F 2 "" H 3200 1800 50  0000 C CNN
F 3 "" H 3200 1800 50  0000 C CNN
	1    3200 1800
	1    0    0    -1  
$EndComp
Wire Wire Line
	3200 1800 3200 1900
$Comp
L 100nF_25V_X7R C26
U 1 1 5729CE8C
P 3200 2650
F 0 "C26" H 3400 2650 50  0000 C CNN
F 1 "100nF_25V_X7R" H 3600 2550 50  0000 C CNN
F 2 "CapacitorCeramicSMD:CcSMD_0805_HandSold" H 3450 2450 50  0001 C CNN
F 3 "" H 3200 2650 60  0000 C CNN
	1    3200 2650
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR052
U 1 1 5729D09A
P 6950 5700
F 0 "#PWR052" H 6950 5450 50  0001 C CNN
F 1 "GND" H 6950 5550 50  0000 C CNN
F 2 "" H 6950 5700 50  0000 C CNN
F 3 "" H 6950 5700 50  0000 C CNN
	1    6950 5700
	1    0    0    -1  
$EndComp
$Comp
L +3V3 #PWR053
U 1 1 5729D0A8
P 6950 4600
F 0 "#PWR053" H 6950 4450 50  0001 C CNN
F 1 "+3V3" H 6950 4750 39  0000 C CNN
F 2 "" H 6950 4600 50  0000 C CNN
F 3 "" H 6950 4600 50  0000 C CNN
	1    6950 4600
	1    0    0    -1  
$EndComp
$Comp
L 100nF_25V_X7R C27
U 1 1 5729D0B1
P 6950 5400
F 0 "C27" H 7150 5400 50  0000 C CNN
F 1 "100nF_25V_X7R" H 7350 5300 50  0000 C CNN
F 2 "CapacitorCeramicSMD:CcSMD_0805_HandSold" H 7200 5200 50  0001 C CNN
F 3 "" H 6950 5400 60  0000 C CNN
	1    6950 5400
	1    0    0    -1  
$EndComp
$Comp
L 10K_125mW_5% R22
U 1 1 5729D0F1
P 6950 4900
F 0 "R22" H 6700 5000 50  0000 L CNN
F 1 "10K_125mW_5%" H 6250 4900 50  0000 L CNN
F 2 "ResistorChipSMD:RchipSMD_0805_HandSold" H 6700 4800 50  0001 C CNN
F 3 "" H 6950 4900 30  0000 C CNN
	1    6950 4900
	1    0    0    -1  
$EndComp
Wire Wire Line
	6700 5200 6700 5150
Wire Wire Line
	6700 5600 6700 5650
Wire Wire Line
	6700 5650 6950 5650
Wire Wire Line
	6950 5500 6950 5700
Connection ~ 6950 5650
Wire Wire Line
	6950 5100 6950 5300
Wire Wire Line
	6700 5150 7100 5150
Connection ~ 6950 5150
Wire Wire Line
	6950 4600 6950 4700
$Comp
L 1K_125mW_5% R23
U 1 1 5729D3E7
P 7300 5150
F 0 "R23" V 7050 5200 50  0000 C CNN
F 1 "1K_125mW_5%" V 7150 5000 50  0000 C CNN
F 2 "ResistorChipSMD:RchipSMD_0805_HandSold" V 7230 5150 50  0001 C CNN
F 3 "" H 7300 5150 30  0000 C CNN
	1    7300 5150
	0    -1   1    0   
$EndComp
Text HLabel 7750 5150 2    60   Output ~ 0
SW2
Wire Wire Line
	7750 5150 7500 5150
$Comp
L GND #PWR054
U 1 1 5729DD9E
P 9150 5700
F 0 "#PWR054" H 9150 5450 50  0001 C CNN
F 1 "GND" H 9150 5550 50  0000 C CNN
F 2 "" H 9150 5700 50  0000 C CNN
F 3 "" H 9150 5700 50  0000 C CNN
	1    9150 5700
	1    0    0    -1  
$EndComp
$Comp
L +3V3 #PWR055
U 1 1 5729DDA4
P 9150 4600
F 0 "#PWR055" H 9150 4450 50  0001 C CNN
F 1 "+3V3" H 9150 4750 39  0000 C CNN
F 2 "" H 9150 4600 50  0000 C CNN
F 3 "" H 9150 4600 50  0000 C CNN
	1    9150 4600
	1    0    0    -1  
$EndComp
$Comp
L 100nF_25V_X7R C28
U 1 1 5729DDAA
P 9150 5400
F 0 "C28" H 9350 5400 50  0000 C CNN
F 1 "100nF_25V_X7R" H 9550 5300 50  0000 C CNN
F 2 "CapacitorCeramicSMD:CcSMD_0805_HandSold" H 9400 5200 50  0001 C CNN
F 3 "" H 9150 5400 60  0000 C CNN
	1    9150 5400
	1    0    0    -1  
$EndComp
$Comp
L 10K_125mW_5% R24
U 1 1 5729DDB1
P 9150 4900
F 0 "R24" H 8950 5000 50  0000 L CNN
F 1 "10K_125mW_5%" H 8500 4900 50  0000 L CNN
F 2 "ResistorChipSMD:RchipSMD_0805_HandSold" H 8950 4800 50  0001 C CNN
F 3 "" H 9150 4900 30  0000 C CNN
	1    9150 4900
	1    0    0    -1  
$EndComp
Wire Wire Line
	8900 5200 8900 5150
Wire Wire Line
	8900 5600 8900 5650
Wire Wire Line
	8900 5650 9150 5650
Wire Wire Line
	9150 5500 9150 5700
Connection ~ 9150 5650
Wire Wire Line
	9150 5100 9150 5300
Wire Wire Line
	8900 5150 9300 5150
Connection ~ 9150 5150
Wire Wire Line
	9150 4600 9150 4700
$Comp
L 1K_125mW_5% R25
U 1 1 5729DDC1
P 9500 5150
F 0 "R25" V 9250 5200 50  0000 C CNN
F 1 "1K_125mW_5%" V 9350 5000 50  0000 C CNN
F 2 "ResistorChipSMD:RchipSMD_0805_HandSold" V 9430 5150 50  0001 C CNN
F 3 "" H 9500 5150 30  0000 C CNN
	1    9500 5150
	0    -1   1    0   
$EndComp
Text HLabel 9950 5150 2    60   Output ~ 0
SW3
Wire Wire Line
	9950 5150 9700 5150
Text GLabel 3650 2350 2    60   Output ~ 0
RESET
Wire Wire Line
	2950 2350 3650 2350
Wire Notes Line
	5900 6100 10450 6100
Wire Notes Line
	10450 6100 10450 4050
Wire Notes Line
	10450 4300 5900 4300
Wire Notes Line
	5900 4050 5900 6100
Wire Notes Line
	10450 4050 5900 4050
Text Notes 6400 4200 0    60   ~ 0
Pulsadores, SW2- SW3 (Proposito General)
Wire Notes Line
	2150 1300 2150 3350
Wire Notes Line
	2150 3350 6700 3350
Wire Notes Line
	6700 3350 6700 1300
Wire Notes Line
	6700 1300 2150 1300
Wire Notes Line
	6700 1550 2150 1550
Text Notes 2350 1450 0    60   ~ 0
Pulsadores, RESET - ISP
Wire Wire Line
	2950 2950 2950 2850
$Comp
L MBR0520 D12
U 1 1 57E6437C
P 3200 2100
F 0 "D12" H 3350 2100 50  0000 C CNN
F 1 "MBR0520" H 3450 2200 50  0000 C CNN
F 2 "DiodosSMD:SOD123-HandSold" H 3450 2000 50  0001 C CNN
F 3 "" V 3200 2100 60  0000 C CNN
	1    3200 2100
	1    0    0    -1  
$EndComp
$Comp
L PTS645-6mm SW2
U 1 1 590915B5
P 6700 5400
F 0 "SW2" H 6500 5400 50  0000 C CNN
F 1 "PTS645-6mm" H 6400 5300 50  0000 C CNN
F 2 "SW_SMD:PTS645-06-4.5mm-HandSold" H 6700 5150 50  0001 C CNN
F 3 "" H 6500 5250 60  0001 C CNN
F 4 "Pushbutton1" H 6350 5400 50  0001 C CNN "Name"
	1    6700 5400
	1    0    0    -1  
$EndComp
$Comp
L PTS645-6mm SW1
U 1 1 59091755
P 2950 2650
F 0 "SW1" H 2700 2650 50  0000 C CNN
F 1 "RESET" H 2700 2550 50  0000 C CNN
F 2 "SW_SMD:PTS645-06-4.5mm-HandSold" H 2950 2400 50  0001 C CNN
F 3 "" H 2750 2500 60  0001 C CNN
F 4 "RESET" H 2600 2650 50  0001 C CNN "Name"
	1    2950 2650
	1    0    0    -1  
$EndComp
$Comp
L PTS645-6mm SW3
U 1 1 590922E4
P 8900 5400
F 0 "SW3" H 8700 5400 50  0000 C CNN
F 1 "PTS645-6mm" H 8600 5300 50  0000 C CNN
F 2 "SW_SMD:PTS645-06-4.5mm-HandSold" H 8900 5150 50  0001 C CNN
F 3 "" H 8700 5250 60  0001 C CNN
F 4 "Pushbutton2" H 8550 5400 50  0001 C CNN "Name"
	1    8900 5400
	1    0    0    -1  
$EndComp
Text Notes 3050 7350 0    60   ~ 0
FootPrint para SW <=> PTS645_06x04.3mm
Text GLabel 5850 2350 2    60   Output ~ 0
ISP
$Comp
L 100K_125mW_5% R1
U 1 1 594B46DB
P 2950 2100
F 0 "R1" H 2800 2100 50  0000 C CNN
F 1 "100K_125mW_5%" H 2600 2350 50  0000 C CNN
F 2 "ResistorChipSMD:RchipSMD_0805_HandSold" V 2880 2100 50  0001 C CNN
F 3 "" H 2950 2100 30  0000 C CNN
	1    2950 2100
	1    0    0    -1  
$EndComp
Wire Wire Line
	2950 1900 2950 1850
Wire Wire Line
	2950 1850 3200 1850
Connection ~ 3200 1850
Connection ~ 2950 2350
Text Notes 3050 7150 0    60   ~ 0
Aconsejable R Pull-Up de\nReset de 100K
$Comp
L GND #PWR056
U 1 1 594B6036
P 5400 3050
F 0 "#PWR056" H 5400 2800 50  0001 C CNN
F 1 "GND" H 5400 2900 50  0000 C CNN
F 2 "" H 5400 3050 50  0000 C CNN
F 3 "" H 5400 3050 50  0000 C CNN
	1    5400 3050
	1    0    0    -1  
$EndComp
Wire Wire Line
	5150 2950 5400 2950
Connection ~ 5400 2950
Wire Wire Line
	5400 2300 5400 2550
Connection ~ 5400 2350
Wire Wire Line
	5400 2750 5400 3050
$Comp
L +3V3 #PWR057
U 1 1 594B6042
P 5400 1800
F 0 "#PWR057" H 5400 1650 50  0001 C CNN
F 1 "+3V3" H 5400 1950 39  0000 C CNN
F 2 "" H 5400 1800 50  0000 C CNN
F 3 "" H 5400 1800 50  0000 C CNN
	1    5400 1800
	1    0    0    -1  
$EndComp
Wire Wire Line
	5400 1900 5400 1800
$Comp
L 100nF_25V_X7R C13
U 1 1 594B6049
P 5400 2650
F 0 "C13" H 5600 2650 50  0000 C CNN
F 1 "100nF_25V_X7R" H 5800 2550 50  0000 C CNN
F 2 "CapacitorCeramicSMD:CcSMD_0805_HandSold" H 5650 2450 50  0001 C CNN
F 3 "" H 5400 2650 60  0000 C CNN
	1    5400 2650
	1    0    0    -1  
$EndComp
Wire Wire Line
	5150 2350 5850 2350
Wire Wire Line
	5150 2950 5150 2850
$Comp
L PTS645-6mm SW4
U 1 1 594B6058
P 5150 2650
F 0 "SW4" H 4900 2650 50  0000 C CNN
F 1 "ISP" H 4900 2550 50  0000 C CNN
F 2 "SW_SMD:PTS645-06-4.5mm-HandSold" H 5150 2400 50  0001 C CNN
F 3 "" H 4950 2500 60  0001 C CNN
F 4 "RESET" H 4800 2650 50  0001 C CNN "Name"
	1    5150 2650
	1    0    0    -1  
$EndComp
$Comp
L 10K_125mW_5% R2
U 1 1 594B605E
P 5400 2100
F 0 "R2" H 5500 2000 50  0000 C CNN
F 1 "10K_125mW_5%" H 5800 2100 50  0000 C CNN
F 2 "ResistorChipSMD:RchipSMD_0805_HandSold" V 5330 2100 50  0001 C CNN
F 3 "" H 5400 2100 30  0000 C CNN
	1    5400 2100
	1    0    0    -1  
$EndComp
Wire Wire Line
	5150 2450 5150 2350
Text Notes 7400 1950 0    60   ~ 0
Con ISP presionado, Presionamos RESET\nDe esta forma, el dispositivo ingresa en modo ISP.\n # FlashMagic bajo Windows\n # LPC21ISP bajo Linux 
Text Notes 7300 1450 0    60   ~ 0
 @ Procedimiento, Carga Firmware :
Wire Notes Line
	7300 1300 7300 2050
Wire Notes Line
	7300 2050 9850 2050
Wire Notes Line
	9850 2050 9850 1300
Wire Notes Line
	9850 1300 7300 1300
Wire Notes Line
	7300 1500 9850 1500
Text Notes 3000 6800 0    60   ~ 0
 @ note :
Wire Notes Line
	2950 6650 2950 7450
Wire Notes Line
	2950 7450 5150 7450
Wire Notes Line
	5150 7450 5150 6650
Wire Notes Line
	5150 6650 2950 6650
Wire Notes Line
	2950 6850 5150 6850
$EndSCHEMATC
