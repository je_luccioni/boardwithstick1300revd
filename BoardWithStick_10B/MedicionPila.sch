EESchema Schematic File Version 2
LIBS:74LVT
LIBS:74xgxx
LIBS:74xx
LIBS:ac-dc
LIBS:actel
LIBS:adc-dac
LIBS:ADC-DeltaSigma
LIBS:ADC-SAR
LIBS:Altera
LIBS:analog_devices
LIBS:AnalogMuxDemux
LIBS:analog_switches
LIBS:AopAudio
LIBS:AopComparador
LIBS:AopPrecision
LIBS:AopRailToRailPG
LIBS:atmel
LIBS:audio
LIBS:battery_management
LIBS:bbd
LIBS:BJT-GeneralPurpose 
LIBS:BOR-SIL
LIBS:BridgeUSB
LIBS:brooktre
LIBS:CapAluminioSMD
LIBS:CapCeramicoHVC
LIBS:CapCeramicos
LIBS:CapCer_THT
LIBS:CapElectroliticos
LIBS:CapPoly_THT
LIBS:CapTantalio
LIBS:Ccer_SMD
LIBS:cmos4000
LIBS:cmos_ieee
LIBS:CommonModeChoke 
LIBS:CompuertasLogicas
LIBS:CON-DIL_F
LIBS:CON-DIL_M
LIBS:Conectores
LIBS:ConectoresDB
LIBS:conn
LIBS:CON-SIL_F
LIBS:CON-SIL_M
LIBS:contrib
LIBS:CON-USB
LIBS:Cristal
LIBS:cypress
LIBS:dc-dc
LIBS:device
LIBS:digital-audio
LIBS:diode
LIBS:DiodoPequeñaSeñal
LIBS:DiodoRectificador
LIBS:DiodoSchottkyRect
LIBS:DiodosSupresorTransitorios
LIBS:DiodosZener
LIBS:display
LIBS:dsp
LIBS:EEpromSerie
LIBS:EIA232
LIBS:EIA485
LIBS:elec-unifil
LIBS:ESD_Protection
LIBS: EsdProtectionDiode
LIBS:FerriteBead
LIBS:FotoDiodos
LIBS:ftdi
LIBS:Fusibles
LIBS:gennum
LIBS:graphic
LIBS:hc11
LIBS:Inductor
LIBS:intel
LIBS:interface
LIBS:ir
LIBS:IRF-MOSFET
LIBS:jumpers
LIBS:Lattice
LIBS:LDR_THT
LIBS:LEDs
LIBS:linear
LIBS:LoadGeneric
LIBS:Logic-MOSFET
LIBS:logo
LIBS:LPC17xx
LIBS:LPC43xx
LIBS:MagneticsProtection
LIBS:maxim
LIBS:memory
LIBS:microchip
LIBS:microchip_dspic33dsc
LIBS:microchip_pic10mcu
LIBS:microchip_pic12mcu
LIBS:microchip_pic16mcu
LIBS:microchip_pic18mcu
LIBS:microchip_pic32mcu
LIBS:microcontrollers
LIBS:motor_drivers
LIBS:motorola
LIBS:msp430
LIBS:nordicsemi
LIBS:NTR-MOSFET
LIBS:nxp_armmcu
LIBS:onsemi
LIBS:opto
LIBS:OptoTransistor
LIBS:OptoTriac
LIBS:Oscillators
LIBS:philips
LIBS:PicoFusePTC
LIBS:Pilas
LIBS:Potenciometro
LIBS:power
LIBS:PowerDistributionSwitch
LIBS:powerint
LIBS:Power_Management
LIBS:pspice
LIBS:Puentes
LIBS:R
LIBS:references
LIBS:RegLinealPositivos
LIBS:regul
LIBS:ReguladorLDO
LIBS:ReguladorSwitcher
LIBS:relays
LIBS:rfcom
LIBS:RMII_Ethernet
LIBS:R_SMD
LIBS:R_ThruHole
LIBS:R_THT
LIBS:SensorCurrent
LIBS:sensors
LIBS:SensorTemperatura
LIBS:ShifterLevel
LIBS:silabs
LIBS:siliconi
LIBS:SourceCurrent
LIBS:StickLPC
LIBS:stm8
LIBS:stm32
LIBS:supertex
LIBS:switches
LIBS:TactSwitch
LIBS:TestPoint
LIBS:texas
LIBS:TrafoReductor
LIBS:TransceiverCAN
LIBS:transf
LIBS:TransistorArrays
LIBS:transistors
LIBS:Triac
LIBS:ttl_ieee
LIBS:valves
LIBS:Varistor
LIBS:video
LIBS:VoltageReference
LIBS:Worldsemi
LIBS:Xicor
LIBS:xilinx
LIBS:xtal
LIBS:Zilog
LIBS:MemoryZock
LIBS:BoardMicro-cache
EELAYER 25 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 8 21
Title "BoardWithStick1300revD"
Date "12-05-2017"
Rev "v1.10b"
Comp "J.E.L"
Comment1 "\\n Luccioni, Jesus Emanuel"
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L 100K_125mW_5% R37
U 1 1 57A9AFF5
P 6650 4000
F 0 "R37" V 6750 4000 50  0000 C CNN
F 1 "100K_125mW_5%" V 6458 4000 50  0000 C CNN
F 2 "ResistorChipSMD:RchipSMD_0805_HandSold" V 6549 4000 50  0001 C CNN
F 3 "" H 6650 4000 30  0000 C CNN
	1    6650 4000
	0    1    -1   0   
$EndComp
$Comp
L 1M_125mW_5% R39
U 1 1 57A9B02D
P 8050 4000
F 0 "R39" H 8200 4000 50  0000 C CNN
F 1 "1M_125mW_5%" H 7700 3900 50  0000 C CNN
F 2 "ResistorChipSMD:RchipSMD_0805_HandSold" H 7750 4000 50  0001 C CNN
F 3 "" H 8050 4000 30  0000 C CNN
	1    8050 4000
	1    0    0    1   
$EndComp
Wire Wire Line
	8050 3650 8050 3800
Connection ~ 8050 3650
Text HLabel 8450 3650 2    60   Output ~ 0
MedPila
$Comp
L NTR4101 Q3
U 1 1 57A9DAF7
P 7100 3650
F 0 "Q3" V 7250 3650 50  0000 C CNN
F 1 "NTR4101" V 7400 3650 50  0000 C CNN
F 2 "SOT_SMD:SOT23_Handsold" H 7120 3655 50  0001 C CNN
F 3 "" H 7120 3655 60  0000 C CNN
	1    7100 3650
	0    -1   -1   0   
$EndComp
$Comp
L GND #PWR082
U 1 1 57AA2141
P 5900 4250
F 0 "#PWR082" H 5900 4000 50  0001 C CNN
F 1 "GND" H 5905 4077 50  0000 C CNN
F 2 "" H 5900 4250 50  0000 C CNN
F 3 "" H 5900 4250 50  0000 C CNN
	1    5900 4250
	1    0    0    -1  
$EndComp
Wire Wire Line
	5900 4250 5900 4150
Wire Wire Line
	6850 4000 7050 4000
Connection ~ 7050 4000
Wire Wire Line
	6350 3650 6350 4000
Wire Wire Line
	6350 4000 6450 4000
Wire Wire Line
	5350 3650 6850 3650
Wire Wire Line
	5900 3550 5900 3750
Connection ~ 5900 3650
$Comp
L PWR_FLAG #FLG083
U 1 1 57AA3090
P 4050 3700
F 0 "#FLG083" H 4050 3795 50  0001 C CNN
F 1 "PWR_FLAG" H 4050 3900 50  0000 C CNN
F 2 "" H 4050 3700 50  0000 C CNN
F 3 "" H 4050 3700 50  0000 C CNN
	1    4050 3700
	1    0    0    1   
$EndComp
$Comp
L +3V3 #PWR084
U 1 1 57AA3096
P 5400 3350
F 0 "#PWR084" H 5400 3200 50  0001 C CNN
F 1 "+3V3" H 5400 3500 39  0000 C CNN
F 2 "" H 5400 3350 50  0000 C CNN
F 3 "" H 5400 3350 50  0000 C CNN
	1    5400 3350
	-1   0    0    -1  
$EndComp
$Comp
L GND #PWR085
U 1 1 57AA309C
P 4700 4150
F 0 "#PWR085" H 4700 3900 50  0001 C CNN
F 1 "GND" H 4700 4000 50  0000 C CNN
F 2 "" H 4700 4150 50  0000 C CNN
F 3 "" H 4700 4150 50  0000 C CNN
	1    4700 4150
	-1   0    0    -1  
$EndComp
Wire Wire Line
	4050 3600 4950 3600
Wire Wire Line
	4850 3600 4850 3850
$Comp
L 100nF_25V_X7R C41
U 1 1 57AA30A4
P 4850 3950
F 0 "C41" H 5100 3950 50  0000 C CNN
F 1 "100nF_25V_X7R" H 5150 3750 50  0000 C CNN
F 2 "CapacitorCeramicSMD:CcSMD_0805_HandSold" H 5150 3750 50  0001 C CNN
F 3 "" H 4850 3950 60  0000 C CNN
	1    4850 3950
	1    0    0    -1  
$EndComp
Connection ~ 4850 3600
Wire Wire Line
	4050 3450 4050 3700
Connection ~ 4050 3600
Wire Wire Line
	5400 3350 5400 3550
$Comp
L +BATT #PWR086
U 1 1 57AA30C0
P 4050 3450
F 0 "#PWR086" H 4050 3300 50  0001 C CNN
F 1 "+BATT" H 4065 3623 50  0000 C CNN
F 2 "" H 4050 3450 50  0000 C CNN
F 3 "" H 4050 3450 50  0000 C CNN
	1    4050 3450
	-1   0    0    -1  
$EndComp
Wire Notes Line
	6150 2600 6150 4550
Wire Notes Line
	6150 4550 3800 4550
Wire Notes Line
	3800 4550 3800 2600
Wire Notes Line
	3800 2600 6150 2600
Wire Notes Line
	6150 2800 3800 2800
Text Notes 5100 2750 2    60   ~ 0
BATTERY
Wire Wire Line
	4550 4000 4550 4100
Wire Wire Line
	4550 4100 4850 4100
Wire Wire Line
	4850 4100 4850 4050
Wire Wire Line
	4700 4150 4700 4100
Connection ~ 4700 4100
Wire Wire Line
	4550 3600 4550 3600
Wire Wire Line
	4550 3600 4550 3800
Connection ~ 4550 3600
Wire Wire Line
	7050 4700 7400 4700
Text GLabel 7400 4700 2    60   Input ~ 0
HabMedPila
Wire Wire Line
	7350 3650 8450 3650
$Comp
L CR2032 BAT1
U 1 1 57D4D0C8
P 5900 3950
F 0 "BAT1" H 6200 4000 50  0000 C CNN
F 1 "CR2032" H 6250 3900 50  0000 C CNN
F 2 "PortaPila:PortaPilaCR2032-THT" H 6350 3800 50  0001 C CNN
F 3 "" H 5900 3900 60  0000 C CNN
	1    5900 3950
	-1   0    0    -1  
$EndComp
Connection ~ 6350 3650
$Comp
L 1K_125mW_5% R38
U 1 1 581AE33D
P 7050 4350
F 0 "R38" H 7350 4250 50  0000 C CNN
F 1 "1K_125mW_5%" H 7450 4350 50  0000 C CNN
F 2 "ResistorChipSMD:RchipSMD_0805_HandSold" V 6949 4350 50  0001 C CNN
F 3 "" H 7050 4350 30  0000 C CNN
	1    7050 4350
	1    0    0    1   
$EndComp
Wire Wire Line
	7050 3900 7050 4150
Wire Wire Line
	7050 4550 7050 4700
$Comp
L TestPoint TP8
U 1 1 581D3977
P 5900 3550
F 0 "TP8" H 5700 3550 50  0000 C CNN
F 1 "TP_VBAT" H 5900 3750 50  0000 C CNN
F 2 "TestPoint:TEST-POINT_0.5x2mm" H 5900 3350 50  0001 C CNN
F 3 "" H 5900 3250 60  0000 C CNN
	1    5900 3550
	1    0    0    -1  
$EndComp
$Comp
L GNDA #PWR087
U 1 1 58229992
P 8050 4200
F 0 "#PWR087" H 8050 3950 50  0001 C CNN
F 1 "GNDA" H 8050 4050 50  0000 C CNN
F 2 "" H 8050 4200 50  0000 C CNN
F 3 "" H 8050 4200 50  0000 C CNN
	1    8050 4200
	1    0    0    -1  
$EndComp
$Comp
L 10uF_10V_T491 C40
U 1 1 590A5450
P 4550 3900
F 0 "C40" H 4950 3650 50  0000 C CNN
F 1 "10uF_10V_T491" H 4950 3750 50  0000 C CNN
F 2 "CapacitorTantalumSMD:CtanlumSMD_Case-A_Hand" H 4950 3800 50  0001 C CNN
F 3 "" H 4750 4000 60  0000 C CNN
	1    4550 3900
	-1   0    0    -1  
$EndComp
Text Notes 2900 7400 0    60   ~ 0
BAT54 -> 800[mV] a 200[mA] {Imax}\nMBR0520 -> 300[mV] a 500[mA] {Imax}\nBAT54C -> 800[mV] a 200[mA] {Imax} / Xrama
$Comp
L BAT54C D18
U 1 1 59D34293
P 5200 3600
F 0 "D18" V 5350 3600 50  0000 C CNN
F 1 "BAT54C" V 5000 3600 50  0000 C CNN
F 2 "SOT_SMD:SOT23_Handsold" H 5100 3250 50  0001 C CNN
F 3 "" V 5150 3585 60  0000 C CNN
	1    5200 3600
	0    1    1    0   
$EndComp
Wire Wire Line
	5400 3550 5350 3550
Text Notes 2800 7000 0    60   ~ 0
 @ note :
Wire Notes Line
	2800 6850 2800 7500
Wire Notes Line
	2800 7500 5300 7500
Wire Notes Line
	5300 7500 5300 6850
Wire Notes Line
	5300 6850 2800 6850
Wire Notes Line
	2800 7050 5300 7050
$EndSCHEMATC
