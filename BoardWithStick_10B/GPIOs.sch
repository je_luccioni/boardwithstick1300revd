EESchema Schematic File Version 2
LIBS:74LVT
LIBS:74xgxx
LIBS:74xx
LIBS:ac-dc
LIBS:actel
LIBS:adc-dac
LIBS:ADC-DeltaSigma
LIBS:ADC-SAR
LIBS:Altera
LIBS:analog_devices
LIBS:AnalogMuxDemux
LIBS:analog_switches
LIBS:AopAudio
LIBS:AopComparador
LIBS:AopPrecision
LIBS:AopRailToRailPG
LIBS:atmel
LIBS:audio
LIBS:battery_management
LIBS:bbd
LIBS:BJT-GeneralPurpose 
LIBS:BOR-SIL
LIBS:BridgeUSB
LIBS:brooktre
LIBS:CapAluminioSMD
LIBS:CapCeramicoHVC
LIBS:CapCeramicos
LIBS:CapCer_THT
LIBS:CapElectroliticos
LIBS:CapPoly_THT
LIBS:CapTantalio
LIBS:Ccer_SMD
LIBS:cmos4000
LIBS:cmos_ieee
LIBS:CommonModeChoke 
LIBS:CompuertasLogicas
LIBS:CON-DIL_F
LIBS:CON-DIL_M
LIBS:Conectores
LIBS:ConectoresDB
LIBS:conn
LIBS:CON-SIL_F
LIBS:CON-SIL_M
LIBS:contrib
LIBS:CON-USB
LIBS:Cristal
LIBS:cypress
LIBS:dc-dc
LIBS:device
LIBS:digital-audio
LIBS:diode
LIBS:DiodoPequeñaSeñal
LIBS:DiodoRectificador
LIBS:DiodoSchottkyRect
LIBS:DiodosSupresorTransitorios
LIBS:DiodosZener
LIBS:display
LIBS:dsp
LIBS:EEpromSerie
LIBS:EIA232
LIBS:EIA485
LIBS:elec-unifil
LIBS:ESD_Protection
LIBS: EsdProtectionDiode
LIBS:FerriteBead
LIBS:FotoDiodos
LIBS:ftdi
LIBS:Fusibles
LIBS:gennum
LIBS:graphic
LIBS:hc11
LIBS:Inductor
LIBS:intel
LIBS:interface
LIBS:ir
LIBS:IRF-MOSFET
LIBS:jumpers
LIBS:Lattice
LIBS:LDR_THT
LIBS:LEDs
LIBS:linear
LIBS:LoadGeneric
LIBS:Logic-MOSFET
LIBS:logo
LIBS:LPC17xx
LIBS:LPC43xx
LIBS:MagneticsProtection
LIBS:maxim
LIBS:memory
LIBS:microchip
LIBS:microchip_dspic33dsc
LIBS:microchip_pic10mcu
LIBS:microchip_pic12mcu
LIBS:microchip_pic16mcu
LIBS:microchip_pic18mcu
LIBS:microchip_pic32mcu
LIBS:microcontrollers
LIBS:motor_drivers
LIBS:motorola
LIBS:msp430
LIBS:nordicsemi
LIBS:NTR-MOSFET
LIBS:nxp_armmcu
LIBS:onsemi
LIBS:opto
LIBS:OptoTransistor
LIBS:OptoTriac
LIBS:Oscillators
LIBS:philips
LIBS:PicoFusePTC
LIBS:Pilas
LIBS:Potenciometro
LIBS:power
LIBS:PowerDistributionSwitch
LIBS:powerint
LIBS:Power_Management
LIBS:pspice
LIBS:Puentes
LIBS:R
LIBS:references
LIBS:RegLinealPositivos
LIBS:regul
LIBS:ReguladorLDO
LIBS:ReguladorSwitcher
LIBS:relays
LIBS:rfcom
LIBS:RMII_Ethernet
LIBS:R_SMD
LIBS:R_ThruHole
LIBS:R_THT
LIBS:SensorCurrent
LIBS:sensors
LIBS:SensorTemperatura
LIBS:ShifterLevel
LIBS:silabs
LIBS:siliconi
LIBS:SourceCurrent
LIBS:StickLPC
LIBS:stm8
LIBS:stm32
LIBS:supertex
LIBS:switches
LIBS:TactSwitch
LIBS:TestPoint
LIBS:texas
LIBS:TrafoReductor
LIBS:TransceiverCAN
LIBS:transf
LIBS:TransistorArrays
LIBS:transistors
LIBS:Triac
LIBS:ttl_ieee
LIBS:valves
LIBS:Varistor
LIBS:video
LIBS:VoltageReference
LIBS:Worldsemi
LIBS:Xicor
LIBS:xilinx
LIBS:xtal
LIBS:Zilog
LIBS:MemoryZock
LIBS:BoardMicro-cache
EELAYER 25 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 21 21
Title "BoardWithStick1300revD"
Date "12-05-2017"
Rev "v1.10b"
Comp "J.E.L"
Comment1 "\\n Luccioni, Jesus Emanuel"
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L GND #PWR0171
U 1 1 59004D26
P 5100 4500
F 0 "#PWR0171" H 5100 4250 50  0001 C CNN
F 1 "GND" H 5100 4350 50  0000 C CNN
F 2 "" H 5100 4500 50  0000 C CNN
F 3 "" H 5100 4500 50  0000 C CNN
	1    5100 4500
	1    0    0    -1  
$EndComp
Text HLabel 6050 3700 2    60   BiDi ~ 0
GPIO.2
Wire Wire Line
	5100 4300 5100 4500
$Comp
L DIL-M_2x08PITCH2.54 CN22
U 1 1 59C38D43
P 5500 3950
F 0 "CN22" H 5500 4750 60  0000 C CNN
F 1 "DIL-M_2x08PITCH2.54" H 5500 4500 60  0000 C CNN
F 2 "CON-DIL-Male:DIL-M_2x08PITCH2.54" H 5500 3450 50  0001 C CNB
F 3 "" H 5650 3950 60  0001 C CNN
F 4 "GPIO.Fx" H 5500 4650 60  0000 C CNN "Name"
F 5 "Vcc" H 5250 4350 60  0000 C CNN "CN.1"
F 6 "GND" H 5250 3650 60  0000 C CNN "CN.15"
	1    5500 3950
	1    0    0    -1  
$EndComp
NoConn ~ 5800 3600
NoConn ~ 5800 4300
Text HLabel 4950 3700 0    60   BiDi ~ 0
GPIO.1
Text HLabel 4950 3800 0    60   BiDi ~ 0
GPIO.3
Text HLabel 4950 3900 0    60   BiDi ~ 0
GPIO.5
Text HLabel 4950 4000 0    60   BiDi ~ 0
GPIO.7
Text HLabel 4950 4100 0    60   BiDi ~ 0
GPIO.9
Text HLabel 4950 4200 0    60   BiDi ~ 0
GPIO.11
Text HLabel 6050 3800 2    60   BiDi ~ 0
GPIO.4
Text HLabel 6050 3900 2    60   BiDi ~ 0
GPIO.6
Text HLabel 6050 4000 2    60   BiDi ~ 0
GPIO.8
Text HLabel 6050 4100 2    60   BiDi ~ 0
GPIO.10
Text HLabel 6050 4200 2    60   BiDi ~ 0
GPIO.12
Wire Wire Line
	5200 4300 5100 4300
$Comp
L +3V3 #PWR0172
U 1 1 59C3B45F
P 5100 3200
F 0 "#PWR0172" H 5100 3050 50  0001 C CNN
F 1 "+3V3" H 5100 3340 50  0000 C CNN
F 2 "" H 5100 3200 50  0000 C CNN
F 3 "" H 5100 3200 50  0000 C CNN
	1    5100 3200
	1    0    0    -1  
$EndComp
Wire Wire Line
	5200 3600 5100 3600
Wire Wire Line
	5100 3600 5100 3200
Wire Wire Line
	6050 3700 5800 3700
Wire Wire Line
	5800 3800 6050 3800
Wire Wire Line
	6050 3900 5800 3900
Wire Wire Line
	5800 4000 6050 4000
Wire Wire Line
	6050 4100 5800 4100
Wire Wire Line
	5800 4200 6050 4200
Wire Wire Line
	4950 3700 5200 3700
Wire Wire Line
	5200 3800 4950 3800
Wire Wire Line
	4950 3900 5200 3900
Wire Wire Line
	5200 4000 4950 4000
Wire Wire Line
	4950 4100 5200 4100
Wire Wire Line
	5200 4200 4950 4200
$EndSCHEMATC
