EESchema Schematic File Version 2
LIBS:74LVT
LIBS:74xgxx
LIBS:74xx
LIBS:ac-dc
LIBS:actel
LIBS:adc-dac
LIBS:ADC-DeltaSigma
LIBS:ADC-SAR
LIBS:Altera
LIBS:analog_devices
LIBS:AnalogMuxDemux
LIBS:analog_switches
LIBS:AopAudio
LIBS:AopComparador
LIBS:AopPrecision
LIBS:AopRailToRailPG
LIBS:atmel
LIBS:audio
LIBS:battery_management
LIBS:bbd
LIBS:BJT-GeneralPurpose 
LIBS:BOR-SIL
LIBS:BridgeUSB
LIBS:brooktre
LIBS:CapAluminioSMD
LIBS:CapCeramicoHVC
LIBS:CapCeramicos
LIBS:CapCer_THT
LIBS:CapElectroliticos
LIBS:CapPoly_THT
LIBS:CapTantalio
LIBS:Ccer_SMD
LIBS:cmos4000
LIBS:cmos_ieee
LIBS:CommonModeChoke 
LIBS:CompuertasLogicas
LIBS:CON-DIL_F
LIBS:CON-DIL_M
LIBS:Conectores
LIBS:ConectoresDB
LIBS:conn
LIBS:CON-SIL_F
LIBS:CON-SIL_M
LIBS:contrib
LIBS:CON-USB
LIBS:Cristal
LIBS:cypress
LIBS:dc-dc
LIBS:device
LIBS:digital-audio
LIBS:diode
LIBS:DiodoPequeñaSeñal
LIBS:DiodoRectificador
LIBS:DiodoSchottkyRect
LIBS:DiodosSupresorTransitorios
LIBS:DiodosZener
LIBS:display
LIBS:dsp
LIBS:EEpromSerie
LIBS:EIA232
LIBS:EIA485
LIBS:elec-unifil
LIBS:ESD_Protection
LIBS: EsdProtectionDiode
LIBS:FerriteBead
LIBS:FotoDiodos
LIBS:ftdi
LIBS:Fusibles
LIBS:gennum
LIBS:graphic
LIBS:hc11
LIBS:Inductor
LIBS:intel
LIBS:interface
LIBS:ir
LIBS:IRF-MOSFET
LIBS:jumpers
LIBS:Lattice
LIBS:LDR_THT
LIBS:LEDs
LIBS:linear
LIBS:LoadGeneric
LIBS:Logic-MOSFET
LIBS:logo
LIBS:LPC17xx
LIBS:LPC43xx
LIBS:MagneticsProtection
LIBS:maxim
LIBS:memory
LIBS:microchip
LIBS:microchip_dspic33dsc
LIBS:microchip_pic10mcu
LIBS:microchip_pic12mcu
LIBS:microchip_pic16mcu
LIBS:microchip_pic18mcu
LIBS:microchip_pic32mcu
LIBS:microcontrollers
LIBS:motor_drivers
LIBS:motorola
LIBS:msp430
LIBS:nordicsemi
LIBS:NTR-MOSFET
LIBS:nxp_armmcu
LIBS:onsemi
LIBS:opto
LIBS:OptoTransistor
LIBS:OptoTriac
LIBS:Oscillators
LIBS:philips
LIBS:PicoFusePTC
LIBS:Pilas
LIBS:Potenciometro
LIBS:power
LIBS:PowerDistributionSwitch
LIBS:powerint
LIBS:Power_Management
LIBS:pspice
LIBS:Puentes
LIBS:R
LIBS:references
LIBS:RegLinealPositivos
LIBS:regul
LIBS:ReguladorLDO
LIBS:ReguladorSwitcher
LIBS:relays
LIBS:rfcom
LIBS:RMII_Ethernet
LIBS:R_SMD
LIBS:R_ThruHole
LIBS:R_THT
LIBS:SensorCurrent
LIBS:sensors
LIBS:SensorTemperatura
LIBS:ShifterLevel
LIBS:silabs
LIBS:siliconi
LIBS:SourceCurrent
LIBS:StickLPC
LIBS:stm8
LIBS:stm32
LIBS:supertex
LIBS:switches
LIBS:TactSwitch
LIBS:TestPoint
LIBS:texas
LIBS:TrafoReductor
LIBS:TransceiverCAN
LIBS:transf
LIBS:TransistorArrays
LIBS:transistors
LIBS:Triac
LIBS:ttl_ieee
LIBS:valves
LIBS:Varistor
LIBS:video
LIBS:VoltageReference
LIBS:Worldsemi
LIBS:Xicor
LIBS:xilinx
LIBS:xtal
LIBS:Zilog
LIBS:MemoryZock
LIBS:BoardMicro-cache
EELAYER 25 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 4 21
Title "BoardWithStick1300revD"
Date "12-05-2017"
Rev "v1.10b"
Comp "J.E.L"
Comment1 "\\n Luccioni, Jesus Emanuel"
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
Text HLabel 4700 2400 0    60   BiDi ~ 0
ETH.TXP
Text HLabel 4700 2700 0    60   BiDi ~ 0
ETH.TXN
Text HLabel 4700 3100 0    60   BiDi ~ 0
ETH.RXP
Text HLabel 4700 3400 0    60   BiDi ~ 0
ETH.RXN
Text Notes 950  7400 0    60   ~ 0
1 <=> RD+\n2 <=> RD-\n3 <=> TD+\n4 <=> NC\n5 <=> NC\n6 <=> TD-\n7 <=> NC\n8 <=> NC
Text Notes 1750 7400 0    60   ~ 0
1 <=> RD+\n2 <=> RD-\n3 <=> TD+\n4 <=> +VDC\n5 <=> +VDC\n6 <=> TD-\n7 <=> -VDC\n8 <=> -VDC
$Comp
L MMZ1608B601C FB1
U 1 1 578BB323
P 5450 2000
F 0 "FB1" H 5800 2000 50  0000 C CNN
F 1 "MMZ1608B601C" H 5850 1850 50  0000 C CNN
F 2 "FerriteBeadSMD:FbSMD_0603_HandSold" V 5250 2000 50  0001 C CNN
F 3 "" H 5450 2000 30  0000 C CNN
	1    5450 2000
	1    0    0    -1  
$EndComp
$Comp
L +3V3 #PWR033
U 1 1 578BB4F0
P 5450 1700
F 0 "#PWR033" H 5450 1550 50  0001 C CNN
F 1 "+3V3" H 5450 1850 39  0000 C CNN
F 2 "" H 5450 1700 50  0000 C CNN
F 3 "" H 5450 1700 50  0000 C CNN
	1    5450 1700
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR034
U 1 1 578BED62
P 5700 4050
F 0 "#PWR034" H 5700 3800 50  0001 C CNN
F 1 "GND" H 5700 3900 50  0000 C CNN
F 2 "" H 5700 4050 50  0000 C CNN
F 3 "" H 5700 4050 50  0000 C CNN
	1    5700 4050
	1    0    0    -1  
$EndComp
$Comp
L 1000pF_2KV_X7R C18
U 1 1 578C0E9E
P 7700 4600
F 0 "C18" H 7300 4600 50  0000 L CNN
F 1 "1000pF_2KV_X7R" H 6850 4500 50  0000 L CNN
F 2 "CapacitorCeramicSMD:CcSMD_1206_HandSold" H 6850 4400 50  0001 C CNN
F 3 "" H 7700 4600 60  0000 C CNN
	1    7700 4600
	1    0    0    -1  
$EndComp
$Comp
L 75.0R_125mW_2% R11
U 1 1 578C535A
P 7850 3950
F 0 "R11" H 7900 4000 50  0000 L CNN
F 1 "75.0R_125mW_2%" V 7750 3700 50  0001 L CNN
F 2 "ResistorChipSMD:RchipSMD_0805_HandSold" H 7950 3850 50  0001 L CNN
F 3 "" H 7850 3950 30  0000 C CNN
	1    7850 3950
	1    0    0    -1  
$EndComp
$Comp
L 75.0R_125mW_2% R10
U 1 1 578C5A76
P 7550 3950
F 0 "R10" H 7300 4000 50  0000 L CNN
F 1 "75.0R_125mW_2%" H 6800 3900 50  0000 L CNN
F 2 "ResistorChipSMD:RchipSMD_0805_HandSold" H 7200 3800 50  0001 L CNN
F 3 "" H 7550 3950 30  0000 C CNN
	1    7550 3950
	1    0    0    -1  
$EndComp
$Comp
L GNDPWR #PWR035
U 1 1 578C75FB
P 9450 5000
F 0 "#PWR035" H 9450 4800 50  0001 C CNN
F 1 "GNDPWR" H 9450 4850 50  0000 C CNN
F 2 "" H 9450 4950 50  0000 C CNN
F 3 "" H 9450 4950 50  0000 C CNN
	1    9450 5000
	1    0    0    -1  
$EndComp
$Comp
L PWR_FLAG #FLG036
U 1 1 578C76FD
P 9450 4900
F 0 "#FLG036" H 9450 4995 50  0001 C CNN
F 1 "PWR_FLAG" H 9450 5124 50  0000 C CNN
F 2 "" H 9450 4900 50  0000 C CNN
F 3 "" H 9450 4900 50  0000 C CNN
	1    9450 4900
	1    0    0    -1  
$EndComp
Wire Wire Line
	7650 3200 8750 3200
Wire Wire Line
	8300 3100 8750 3100
Wire Wire Line
	8750 3000 8000 3000
Wire Wire Line
	7050 2700 8750 2700
Wire Wire Line
	7050 3400 8300 3400
Wire Wire Line
	7050 3100 7650 3100
Wire Wire Line
	8000 2400 7050 2400
Wire Wire Line
	4700 2400 5950 2400
Wire Wire Line
	4700 2700 5950 2700
Wire Wire Line
	5450 1700 5450 1800
Wire Wire Line
	4700 3100 5950 3100
Wire Wire Line
	5450 3250 5950 3250
Wire Wire Line
	7550 4150 7550 4350
Wire Wire Line
	7550 3750 7550 3250
Wire Wire Line
	7550 3250 7050 3250
Wire Wire Line
	9450 4900 9450 5000
Wire Wire Line
	7700 4750 7700 4800
Wire Wire Line
	7850 2550 7050 2550
Wire Wire Line
	7700 4450 7700 4350
Wire Wire Line
	7850 4350 7850 4150
Wire Wire Line
	7850 2550 7850 3750
$Comp
L GNDPWR #PWR037
U 1 1 578C4325
P 7700 4800
F 0 "#PWR037" H 7700 4600 50  0001 C CNN
F 1 "GNDPWR" H 7700 4650 50  0000 C CNN
F 2 "" H 7700 4750 50  0000 C CNN
F 3 "" H 7700 4750 50  0000 C CNN
	1    7700 4800
	1    0    0    -1  
$EndComp
Wire Wire Line
	4700 3400 5950 3400
Wire Wire Line
	5950 2550 5450 2550
Connection ~ 5450 3250
Wire Wire Line
	5900 3650 5900 3250
Connection ~ 5900 3250
Wire Wire Line
	5450 3850 5450 3950
Wire Wire Line
	5450 3950 5900 3950
Wire Wire Line
	5900 3950 5900 3850
Connection ~ 5450 2550
Wire Wire Line
	5450 2200 5450 3650
$Comp
L RJ-Female CN2
U 1 1 57D60F80
P 9350 2850
F 0 "CN2" H 9250 3200 50  0000 C CNN
F 1 "Ethernet" V 8800 2850 50  0000 C CNN
F 2 "Conectores:CN_RJ45-8" H 13200 3250 50  0001 C CNN
F 3 "" H 13200 3350 50  0001 C CNN
F 4 "Ethernet" V 8700 2850 60  0001 C CNN "Name"
	1    9350 2850
	-1   0    0    1   
$EndComp
Wire Wire Line
	7550 4350 7850 4350
Connection ~ 7700 4350
NoConn ~ 8750 2500
NoConn ~ 8750 2600
NoConn ~ 8750 2900
NoConn ~ 8750 2800
Wire Wire Line
	8000 3000 8000 2400
Wire Wire Line
	7650 3100 7650 3200
Wire Wire Line
	8300 3400 8300 3100
$Comp
L GNDPWR #PWR038
U 1 1 59036700
P 9900 5850
F 0 "#PWR038" H 9900 5650 50  0001 C CNN
F 1 "GNDPWR" H 9900 5700 50  0000 C CNN
F 2 "" H 9900 5800 50  0000 C CNN
F 3 "" H 9900 5800 50  0000 C CNN
	1    9900 5850
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR039
U 1 1 59036731
P 9050 5850
F 0 "#PWR039" H 9050 5600 50  0001 C CNN
F 1 "GND" H 9050 5700 50  0000 C CNN
F 2 "" H 9050 5850 50  0000 C CNN
F 3 "" H 9050 5850 50  0000 C CNN
	1    9050 5850
	1    0    0    -1  
$EndComp
Wire Wire Line
	9250 5700 9050 5700
Wire Wire Line
	9050 5700 9050 5850
Wire Wire Line
	9650 5700 9900 5700
Wire Wire Line
	9900 5700 9900 5850
$Comp
L H1102NL MG1
U 1 1 59052DFE
P 6500 2900
F 0 "MG1" H 6500 3600 61  0000 C CNN
F 1 "H1102NL" H 6500 2200 61  0000 C CNN
F 2 "TrafoPoE:FP-H110X-HandSold" H 6500 2100 61  0001 C CNN
F 3 "" H 6500 3250 50  0000 C CNN
	1    6500 2900
	1    0    0    -1  
$EndComp
$Comp
L 18pF_50V_COG C16
U 1 1 59063247
P 5450 3750
F 0 "C16" H 5450 3450 50  0000 C CNN
F 1 "18pF_50V_COG" V 5450 3000 50  0000 C CNN
F 2 "CapacitorCeramicSMD:CcSMD_0603_HandSold" H 5700 3600 50  0001 C CNN
F 3 "" H 5450 3750 60  0000 C CNN
	1    5450 3750
	1    0    0    -1  
$EndComp
$Comp
L 18pF_50V_COG C17
U 1 1 59063304
P 5900 3750
F 0 "C17" H 5900 3450 50  0000 C CNN
F 1 "18pF_50V_COG" V 5900 3000 50  0000 C CNN
F 2 "CapacitorCeramicSMD:CcSMD_0603_HandSold" H 6150 3600 50  0001 C CNN
F 3 "" H 5900 3750 60  0000 C CNN
	1    5900 3750
	1    0    0    -1  
$EndComp
$Comp
L 0R_250mW_5% R122
U 1 1 590BF17B
P 9450 5700
F 0 "R122" V 9550 5700 50  0000 C CNN
F 1 "0R_250mW_5%" V 9350 5700 50  0000 C CNN
F 2 "ResistorChipSMD:RchipSMD_1206_HandSold" H 9450 5450 50  0001 C CNN
F 3 "" H 9450 5700 30  0000 C CNN
	1    9450 5700
	0    1    1    0   
$EndComp
Wire Wire Line
	5700 4050 5700 3950
Connection ~ 5700 3950
Wire Notes Line
	850  6150 850  7550
Wire Notes Line
	850  7550 4500 7550
Wire Notes Line
	4500 7550 4500 6150
Wire Notes Line
	4500 6150 850  6150
Wire Notes Line
	850  6350 4500 6350
Text Notes 900  6300 0    60   Italic 12
PinOut RJ45
Wire Notes Line
	1650 6350 1650 7550
Wire Notes Line
	2600 6350 2600 7550
Wire Notes Line
	850  6550 4500 6550
Text Notes 950  6500 0    60   ~ 12
Sin POE
Text Notes 1750 6500 0    60   ~ 12
Con POE, 13 [W]
Text Notes 2700 7400 0    60   ~ 0
1 <=> B.GREEN\n2 <=> GREEN\n3 <=> B.ORANGE\n4 <=> BLUE\n5 <=> B.BLUE\n6 <=> ORANGE\n7 <=> B.BROWN\n8 <=> BROWN
Text Notes 2700 6500 0    60   ~ 12
case A
Text Notes 3650 7400 0    60   ~ 0
1 <=> B.ORANGE\n2 <=> ORANGE\n3 <=> B.GREEN\n4 <=> BLUE\n5 <=> B.BLUE\n6 <=> GREEN\n7 <=> B.BROWN\n8 <=> BROWN
Wire Notes Line
	3550 6350 3550 7550
Text Notes 3650 6500 0    60   ~ 12
case B
$EndSCHEMATC
