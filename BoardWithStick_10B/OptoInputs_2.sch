EESchema Schematic File Version 2
LIBS:74LVT
LIBS:74xgxx
LIBS:74xx
LIBS:ac-dc
LIBS:actel
LIBS:adc-dac
LIBS:ADC-DeltaSigma
LIBS:ADC-SAR
LIBS:Altera
LIBS:analog_devices
LIBS:AnalogMuxDemux
LIBS:analog_switches
LIBS:AopAudio
LIBS:AopComparador
LIBS:AopPrecision
LIBS:AopRailToRailPG
LIBS:atmel
LIBS:audio
LIBS:battery_management
LIBS:bbd
LIBS:BJT-GeneralPurpose 
LIBS:BOR-SIL
LIBS:BridgeUSB
LIBS:brooktre
LIBS:CapAluminioSMD
LIBS:CapCeramicoHVC
LIBS:CapCeramicos
LIBS:CapCer_THT
LIBS:CapElectroliticos
LIBS:CapPoly_THT
LIBS:CapTantalio
LIBS:Ccer_SMD
LIBS:cmos4000
LIBS:cmos_ieee
LIBS:CommonModeChoke 
LIBS:CompuertasLogicas
LIBS:CON-DIL_F
LIBS:CON-DIL_M
LIBS:Conectores
LIBS:ConectoresDB
LIBS:conn
LIBS:CON-SIL_F
LIBS:CON-SIL_M
LIBS:contrib
LIBS:CON-USB
LIBS:Cristal
LIBS:cypress
LIBS:dc-dc
LIBS:device
LIBS:digital-audio
LIBS:diode
LIBS:DiodoPequeñaSeñal
LIBS:DiodoRectificador
LIBS:DiodoSchottkyRect
LIBS:DiodosSupresorTransitorios
LIBS:DiodosZener
LIBS:display
LIBS:dsp
LIBS:EEpromSerie
LIBS:EIA232
LIBS:EIA485
LIBS:elec-unifil
LIBS:ESD_Protection
LIBS: EsdProtectionDiode
LIBS:FerriteBead
LIBS:FotoDiodos
LIBS:ftdi
LIBS:Fusibles
LIBS:gennum
LIBS:graphic
LIBS:hc11
LIBS:Inductor
LIBS:intel
LIBS:interface
LIBS:ir
LIBS:IRF-MOSFET
LIBS:jumpers
LIBS:Lattice
LIBS:LDR_THT
LIBS:LEDs
LIBS:linear
LIBS:LoadGeneric
LIBS:Logic-MOSFET
LIBS:logo
LIBS:LPC17xx
LIBS:LPC43xx
LIBS:MagneticsProtection
LIBS:maxim
LIBS:memory
LIBS:microchip
LIBS:microchip_dspic33dsc
LIBS:microchip_pic10mcu
LIBS:microchip_pic12mcu
LIBS:microchip_pic16mcu
LIBS:microchip_pic18mcu
LIBS:microchip_pic32mcu
LIBS:microcontrollers
LIBS:motor_drivers
LIBS:motorola
LIBS:msp430
LIBS:nordicsemi
LIBS:NTR-MOSFET
LIBS:nxp_armmcu
LIBS:onsemi
LIBS:opto
LIBS:OptoTransistor
LIBS:OptoTriac
LIBS:Oscillators
LIBS:philips
LIBS:PicoFusePTC
LIBS:Pilas
LIBS:Potenciometro
LIBS:power
LIBS:PowerDistributionSwitch
LIBS:powerint
LIBS:Power_Management
LIBS:pspice
LIBS:Puentes
LIBS:R
LIBS:references
LIBS:RegLinealPositivos
LIBS:regul
LIBS:ReguladorLDO
LIBS:ReguladorSwitcher
LIBS:relays
LIBS:rfcom
LIBS:RMII_Ethernet
LIBS:R_SMD
LIBS:R_ThruHole
LIBS:R_THT
LIBS:SensorCurrent
LIBS:sensors
LIBS:SensorTemperatura
LIBS:ShifterLevel
LIBS:silabs
LIBS:siliconi
LIBS:SourceCurrent
LIBS:StickLPC
LIBS:stm8
LIBS:stm32
LIBS:supertex
LIBS:switches
LIBS:TactSwitch
LIBS:TestPoint
LIBS:texas
LIBS:TrafoReductor
LIBS:TransceiverCAN
LIBS:transf
LIBS:TransistorArrays
LIBS:transistors
LIBS:Triac
LIBS:ttl_ieee
LIBS:valves
LIBS:Varistor
LIBS:video
LIBS:VoltageReference
LIBS:Worldsemi
LIBS:Xicor
LIBS:xilinx
LIBS:xtal
LIBS:Zilog
LIBS:MemoryZock
LIBS:BoardMicro-cache
EELAYER 25 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 14 21
Title "BoardWithStick1300revD"
Date "12-05-2017"
Rev "v1.10b"
Comp "J.E.L"
Comment1 "\\n Luccioni, Jesus Emanuel"
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
Text HLabel 4750 1800 0    50   Input ~ 0
OptInp.CH4[in]
Text HLabel 4750 3600 0    50   Input ~ 0
OptInp.CH5[in]
Text HLabel 8000 1950 2    50   Output ~ 0
OptInp.CH4[ou]
Text HLabel 8000 3750 2    50   Output ~ 0
OptInp.CH5[ou]
$Comp
L 4.7K_125mW_5% R73
U 1 1 581C5096
P 5850 2100
F 0 "R73" H 5850 2500 50  0000 C CNN
F 1 "4.7K_125mW_5%" H 5850 2600 50  0000 C CNN
F 2 "ResistorChipSMD:RchipSMD_0805_HandSold" H 6100 1950 50  0001 C CNN
F 3 "" H 5850 2100 30  0000 C CNN
	1    5850 2100
	-1   0    0    -1  
$EndComp
$Comp
L +3V3 #PWR0118
U 1 1 581C50A4
P 7200 1000
F 0 "#PWR0118" H 7200 850 50  0001 C CNN
F 1 "+3V3" H 7200 1140 50  0000 C CNN
F 2 "" H 7200 1000 50  0000 C CNN
F 3 "" H 7200 1000 50  0000 C CNN
	1    7200 1000
	1    0    0    -1  
$EndComp
$Comp
L 100K_125mW_5% R76
U 1 1 581C50AA
P 7100 1300
F 0 "R76" H 7400 1350 50  0000 C CNN
F 1 "100K_125mW_5%" H 7550 1250 50  0000 C CNN
F 2 "ResistorChipSMD:RchipSMD_0805_HandSold" H 7300 1250 50  0001 C CNN
F 3 "" H 7100 1300 30  0000 C CNN
	1    7100 1300
	-1   0    0    -1  
$EndComp
$Comp
L 2.2K_125mW_5% R79
U 1 1 581C50B1
P 7300 1300
F 0 "R79" H 7450 1250 50  0000 C CNN
F 1 "2.2K_125mW_5%" H 7700 1350 50  0000 C CNN
F 2 "ResistorChipSMD:RchipSMD_0805_HandSold" H 7550 1150 50  0001 C CNN
F 3 "" H 7300 1300 30  0000 C CNN
	1    7300 1300
	1    0    0    -1  
$EndComp
Wire Wire Line
	7300 1050 7300 1100
Wire Wire Line
	7100 1050 7300 1050
Wire Wire Line
	7100 1050 7100 1100
Wire Wire Line
	7200 1000 7200 1050
Connection ~ 7200 1050
Wire Wire Line
	6950 1950 8000 1950
Wire Wire Line
	7300 1950 7300 1900
Wire Wire Line
	7100 1500 7100 1950
Connection ~ 7100 1950
$Comp
L GND #PWR0119
U 1 1 581C50C1
P 7100 2250
F 0 "#PWR0119" H 7100 2000 50  0001 C CNN
F 1 "GND" H 7100 2100 50  0000 C CNN
F 2 "" H 7100 2250 50  0000 C CNN
F 3 "" H 7100 2250 50  0000 C CNN
	1    7100 2250
	1    0    0    -1  
$EndComp
Wire Wire Line
	6950 2150 7100 2150
Wire Wire Line
	7100 2150 7100 2250
Wire Wire Line
	6050 2150 5950 2150
Wire Wire Line
	5950 2150 5950 2400
Wire Wire Line
	5950 2400 4750 2400
Wire Wire Line
	6050 1950 5950 1950
Wire Wire Line
	5950 1950 5950 1800
Wire Wire Line
	5950 1800 5250 1800
Wire Wire Line
	5850 1900 5850 1800
Connection ~ 5850 1800
Wire Wire Line
	5850 2400 5850 2300
Connection ~ 5850 2400
Wire Wire Line
	4850 1800 4750 1800
Connection ~ 7300 1950
Text HLabel 4750 2400 0    50   Input ~ 0
OptInp.CH4[COM]
Text HLabel 4750 4200 0    50   Input ~ 0
OptInp.CH5[COM]
$Comp
L LTST-C191KGKT D37
U 1 1 581C50D9
P 7300 1700
F 0 "D37" H 7450 1850 50  0000 C CNN
F 1 "LTST-C191KGKT" H 7700 1750 50  0000 C CNN
F 2 "LedSMD:LED_0805-HandSold" H 7600 1550 50  0001 C CNN
F 3 "" H 7500 1700 60  0001 C CNN
F 4 "GREEN" H 7500 1650 50  0000 C CNN "Colour"
	1    7300 1700
	1    0    0    -1  
$EndComp
Wire Wire Line
	5650 1900 5650 1800
Connection ~ 5650 1800
Wire Wire Line
	5650 2300 5650 2400
Connection ~ 5650 2400
Text HLabel 4750 5500 0    50   Input ~ 0
OptInp.CH6[in]
Text HLabel 8000 5650 2    50   Output ~ 0
OptInp.CH6[ou]
Text HLabel 4750 6100 0    50   Input ~ 0
OptInp.CH6[COM]
$Comp
L 4.7K_125mW_5% R74
U 1 1 5908A2B4
P 5850 3900
F 0 "R74" H 5850 4300 50  0000 C CNN
F 1 "4.7K_125mW_5%" H 5850 4400 50  0000 C CNN
F 2 "ResistorChipSMD:RchipSMD_0805_HandSold" H 6100 3750 50  0001 C CNN
F 3 "" H 5850 3900 30  0000 C CNN
	1    5850 3900
	-1   0    0    -1  
$EndComp
$Comp
L +3V3 #PWR0120
U 1 1 5908A2C0
P 7200 2800
F 0 "#PWR0120" H 7200 2650 50  0001 C CNN
F 1 "+3V3" H 7200 2940 50  0000 C CNN
F 2 "" H 7200 2800 50  0000 C CNN
F 3 "" H 7200 2800 50  0000 C CNN
	1    7200 2800
	1    0    0    -1  
$EndComp
$Comp
L 100K_125mW_5% R77
U 1 1 5908A2C6
P 7100 3100
F 0 "R77" H 7400 3150 50  0000 C CNN
F 1 "100K_125mW_5%" H 7550 3050 50  0000 C CNN
F 2 "ResistorChipSMD:RchipSMD_0805_HandSold" H 7300 3050 50  0001 C CNN
F 3 "" H 7100 3100 30  0000 C CNN
	1    7100 3100
	-1   0    0    -1  
$EndComp
$Comp
L 2.2K_125mW_5% R80
U 1 1 5908A2CC
P 7300 3100
F 0 "R80" H 7450 3050 50  0000 C CNN
F 1 "2.2K_125mW_5%" H 7700 3150 50  0000 C CNN
F 2 "ResistorChipSMD:RchipSMD_0805_HandSold" H 7550 2950 50  0001 C CNN
F 3 "" H 7300 3100 30  0000 C CNN
	1    7300 3100
	1    0    0    -1  
$EndComp
Wire Wire Line
	7300 2850 7300 2900
Wire Wire Line
	7100 2850 7300 2850
Wire Wire Line
	7100 2850 7100 2900
Wire Wire Line
	7200 2800 7200 2850
Connection ~ 7200 2850
Wire Wire Line
	6950 3750 8000 3750
Wire Wire Line
	7300 3750 7300 3700
Wire Wire Line
	7100 3300 7100 3750
Connection ~ 7100 3750
$Comp
L GND #PWR0121
U 1 1 5908A2DB
P 7100 4050
F 0 "#PWR0121" H 7100 3800 50  0001 C CNN
F 1 "GND" H 7100 3900 50  0000 C CNN
F 2 "" H 7100 4050 50  0000 C CNN
F 3 "" H 7100 4050 50  0000 C CNN
	1    7100 4050
	1    0    0    -1  
$EndComp
Wire Wire Line
	6950 3950 7100 3950
Wire Wire Line
	7100 3950 7100 4050
Wire Wire Line
	6050 3950 5950 3950
Wire Wire Line
	5950 3950 5950 4200
Wire Wire Line
	5950 4200 4750 4200
Wire Wire Line
	6050 3750 5950 3750
Wire Wire Line
	5950 3750 5950 3600
Wire Wire Line
	5950 3600 5250 3600
Wire Wire Line
	5850 3700 5850 3600
Connection ~ 5850 3600
Wire Wire Line
	5850 4200 5850 4100
Connection ~ 5850 4200
Connection ~ 7300 3750
$Comp
L LTST-C191KGKT D38
U 1 1 5908A2EF
P 7300 3500
F 0 "D38" H 7450 3650 50  0000 C CNN
F 1 "LTST-C191KGKT" H 7700 3550 50  0000 C CNN
F 2 "LedSMD:LED_0805-HandSold" H 7600 3350 50  0001 C CNN
F 3 "" H 7500 3500 60  0001 C CNN
F 4 "GREEN" H 7500 3450 50  0000 C CNN "Colour"
	1    7300 3500
	1    0    0    -1  
$EndComp
Wire Wire Line
	5650 3700 5650 3600
Connection ~ 5650 3600
Wire Wire Line
	5650 4100 5650 4200
Connection ~ 5650 4200
Wire Wire Line
	4850 3600 4750 3600
$Comp
L 4.7K_125mW_5% R75
U 1 1 5908B918
P 5850 5800
F 0 "R75" H 5850 6200 50  0000 C CNN
F 1 "4.7K_125mW_5%" H 5850 6300 50  0000 C CNN
F 2 "ResistorChipSMD:RchipSMD_0805_HandSold" H 6100 5650 50  0001 C CNN
F 3 "" H 5850 5800 30  0000 C CNN
	1    5850 5800
	-1   0    0    -1  
$EndComp
$Comp
L +3V3 #PWR0122
U 1 1 5908B926
P 7200 4700
F 0 "#PWR0122" H 7200 4550 50  0001 C CNN
F 1 "+3V3" H 7200 4840 50  0000 C CNN
F 2 "" H 7200 4700 50  0000 C CNN
F 3 "" H 7200 4700 50  0000 C CNN
	1    7200 4700
	1    0    0    -1  
$EndComp
$Comp
L 100K_125mW_5% R78
U 1 1 5908B92C
P 7100 5000
F 0 "R78" H 7400 5050 50  0000 C CNN
F 1 "100K_125mW_5%" H 7550 4950 50  0000 C CNN
F 2 "ResistorChipSMD:RchipSMD_0805_HandSold" H 7300 4950 50  0001 C CNN
F 3 "" H 7100 5000 30  0000 C CNN
	1    7100 5000
	-1   0    0    -1  
$EndComp
$Comp
L 2.2K_125mW_5% R81
U 1 1 5908B933
P 7300 5000
F 0 "R81" H 7450 4950 50  0000 C CNN
F 1 "2.2K_125mW_5%" H 7700 5050 50  0000 C CNN
F 2 "ResistorChipSMD:RchipSMD_0805_HandSold" H 7550 4850 50  0001 C CNN
F 3 "" H 7300 5000 30  0000 C CNN
	1    7300 5000
	1    0    0    -1  
$EndComp
Wire Wire Line
	7300 4750 7300 4800
Wire Wire Line
	7100 4750 7300 4750
Wire Wire Line
	7100 4750 7100 4800
Wire Wire Line
	7200 4700 7200 4750
Connection ~ 7200 4750
Wire Wire Line
	6950 5650 8000 5650
Wire Wire Line
	7300 5650 7300 5600
Wire Wire Line
	7100 5200 7100 5650
Connection ~ 7100 5650
$Comp
L GND #PWR0123
U 1 1 5908B943
P 7100 5950
F 0 "#PWR0123" H 7100 5700 50  0001 C CNN
F 1 "GND" H 7100 5800 50  0000 C CNN
F 2 "" H 7100 5950 50  0000 C CNN
F 3 "" H 7100 5950 50  0000 C CNN
	1    7100 5950
	1    0    0    -1  
$EndComp
Wire Wire Line
	6950 5850 7100 5850
Wire Wire Line
	7100 5850 7100 5950
Wire Wire Line
	6050 5850 5950 5850
Wire Wire Line
	5950 5850 5950 6100
Wire Wire Line
	5950 6100 4750 6100
Wire Wire Line
	6050 5650 5950 5650
Wire Wire Line
	5950 5650 5950 5500
Wire Wire Line
	5950 5500 5250 5500
Wire Wire Line
	5850 5600 5850 5500
Connection ~ 5850 5500
Wire Wire Line
	5850 6100 5850 6000
Connection ~ 5850 6100
Connection ~ 7300 5650
$Comp
L LTST-C191KGKT D39
U 1 1 5908B957
P 7300 5400
F 0 "D39" H 7450 5550 50  0000 C CNN
F 1 "LTST-C191KGKT" H 7700 5450 50  0000 C CNN
F 2 "LedSMD:LED_0805-HandSold" H 7600 5250 50  0001 C CNN
F 3 "" H 7500 5400 60  0001 C CNN
F 4 "GREEN" H 7500 5350 50  0000 C CNN "Colour"
	1    7300 5400
	1    0    0    -1  
$EndComp
Wire Wire Line
	5650 5600 5650 5500
Connection ~ 5650 5500
Wire Wire Line
	5650 6000 5650 6100
Connection ~ 5650 6100
Wire Wire Line
	4850 5500 4750 5500
Text Notes 700  7300 0    60   ~ 0
 @@ En caso de Utilizar Opto Con Entrada Unidireccional (ej. TLP183)\n  debemos contemplar los Diodos en Antiparalelo. Caso Contrario (ej. ps2505)\n  los mismo no son necesarios.
$Comp
L TLP183 U9
U 1 1 590945BD
P 6500 2050
F 0 "U9" H 6500 2300 50  0000 C CNN
F 1 "TLP183" H 6500 1800 50  0000 C CNN
F 2 "SSOP:SSOP4_4.5x3.9mm-HandSold" H 6500 1700 50  0001 C CIN
F 3 "" H 6525 2050 60  0000 C CNN
	1    6500 2050
	1    0    0    -1  
$EndComp
Text Notes 700  7000 0    60   ~ 0
 @@ Upgrade, De Opto :\n   # TLP185 -> No Recomendado p/Nuevos Diseños.\n   # TLP183 -> Reemplazo del Anterior.\n   # LTV-356T -> Futuros Diseños.\n  Footprint  Compatibles
Text Notes 700  7550 0    60   ~ 0
 @@ Cambio Diodos MBR0520(D. Schottkey) por \n  1N4148 (D. Rapido p/Señales) <=> "Tiempo de Desenganche"\n
$Comp
L TLP183 U10
U 1 1 590952BF
P 6500 3850
F 0 "U10" H 6500 4100 50  0000 C CNN
F 1 "TLP183" H 6500 3600 50  0000 C CNN
F 2 "SSOP:SSOP4_4.5x3.9mm-HandSold" H 6500 3500 50  0001 C CIN
F 3 "" H 6525 3850 60  0000 C CNN
	1    6500 3850
	1    0    0    -1  
$EndComp
$Comp
L TLP183 U11
U 1 1 5909538B
P 6500 5750
F 0 "U11" H 6500 6000 50  0000 C CNN
F 1 "TLP183" H 6500 5500 50  0000 C CNN
F 2 "SSOP:SSOP4_4.5x3.9mm-HandSold" H 6500 5400 50  0001 C CIN
F 3 "" H 6525 5750 60  0000 C CNN
	1    6500 5750
	1    0    0    -1  
$EndComp
$Comp
L 1N4148WTPMSTR D34
U 1 1 5909B147
P 5650 2100
F 0 "D34" H 5900 2100 50  0000 C CNN
F 1 "1N4148WTPMSTR" H 6000 1950 50  0000 C CNN
F 2 "DiodosSMD:SOD123-HandSold" H 5650 1850 50  0001 C CNN
F 3 "" V 5650 2100 60  0000 C CNN
	1    5650 2100
	-1   0    0    -1  
$EndComp
$Comp
L 1N4148WTPMSTR D35
U 1 1 5909B38D
P 5650 3900
F 0 "D35" H 5900 3900 50  0000 C CNN
F 1 "1N4148WTPMSTR" H 6000 3750 50  0000 C CNN
F 2 "DiodosSMD:SOD123-HandSold" H 5650 3650 50  0001 C CNN
F 3 "" V 5650 3900 60  0000 C CNN
	1    5650 3900
	-1   0    0    -1  
$EndComp
$Comp
L 1N4148WTPMSTR D36
U 1 1 5909B569
P 5650 5800
F 0 "D36" H 5900 5800 50  0000 C CNN
F 1 "1N4148WTPMSTR" H 6000 5650 50  0000 C CNN
F 2 "DiodosSMD:SOD123-HandSold" H 5650 5550 50  0001 C CNN
F 3 "" V 5650 5800 60  0000 C CNN
	1    5650 5800
	-1   0    0    -1  
$EndComp
$Comp
L 3.3K_250mW_5% R72
U 1 1 59D30570
P 5050 5500
F 0 "R72" V 5150 5500 50  0000 C CNN
F 1 "3.3K_250mW_5%" V 4950 5500 50  0000 C CNN
F 2 "ResistorChipSMD:RchipSMD_1206_HandSold" H 5050 5250 50  0001 C CNN
F 3 "" H 5050 5500 30  0000 C CNN
	1    5050 5500
	0    -1   -1   0   
$EndComp
$Comp
L 3.3K_250mW_5% R71
U 1 1 59D306DC
P 5050 3600
F 0 "R71" V 5150 3600 50  0000 C CNN
F 1 "3.3K_250mW_5%" V 4950 3600 50  0000 C CNN
F 2 "ResistorChipSMD:RchipSMD_1206_HandSold" H 5050 3350 50  0001 C CNN
F 3 "" H 5050 3600 30  0000 C CNN
	1    5050 3600
	0    -1   -1   0   
$EndComp
$Comp
L 3.3K_250mW_5% R70
U 1 1 59D30802
P 5050 1800
F 0 "R70" V 5150 1800 50  0000 C CNN
F 1 "3.3K_250mW_5%" V 4950 1800 50  0000 C CNN
F 2 "ResistorChipSMD:RchipSMD_1206_HandSold" H 5050 1550 50  0001 C CNN
F 3 "" H 5050 1800 30  0000 C CNN
	1    5050 1800
	0    -1   -1   0   
$EndComp
Text Notes 650  6450 0    60   ~ 0
 @ note :
Wire Notes Line
	650  6300 650  7650
Wire Notes Line
	650  7650 4550 7650
Wire Notes Line
	4550 7650 4550 6300
Wire Notes Line
	4550 6300 650  6300
Wire Notes Line
	650  6500 4550 6500
$EndSCHEMATC
