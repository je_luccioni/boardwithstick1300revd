EESchema Schematic File Version 2
LIBS:74LVT
LIBS:74xgxx
LIBS:74xx
LIBS:ac-dc
LIBS:actel
LIBS:adc-dac
LIBS:ADC-DeltaSigma
LIBS:ADC-SAR
LIBS:Altera
LIBS:analog_devices
LIBS:AnalogMuxDemux
LIBS:analog_switches
LIBS:AopAudio
LIBS:AopComparador
LIBS:AopPrecision
LIBS:AopRailToRailPG
LIBS:atmel
LIBS:audio
LIBS:battery_management
LIBS:bbd
LIBS:BJT-GeneralPurpose 
LIBS:BOR-SIL
LIBS:BridgeUSB
LIBS:brooktre
LIBS:CapAluminioSMD
LIBS:CapCeramicoHVC
LIBS:CapCeramicos
LIBS:CapCer_THT
LIBS:CapElectroliticos
LIBS:CapPoly_THT
LIBS:CapTantalio
LIBS:Ccer_SMD
LIBS:cmos4000
LIBS:cmos_ieee
LIBS:CommonModeChoke 
LIBS:CompuertasLogicas
LIBS:CON-DIL_F
LIBS:CON-DIL_M
LIBS:Conectores
LIBS:ConectoresDB
LIBS:conn
LIBS:CON-SIL_F
LIBS:CON-SIL_M
LIBS:contrib
LIBS:CON-USB
LIBS:Cristal
LIBS:cypress
LIBS:dc-dc
LIBS:device
LIBS:digital-audio
LIBS:diode
LIBS:DiodoPequeñaSeñal
LIBS:DiodoRectificador
LIBS:DiodoSchottkyRect
LIBS:DiodosSupresorTransitorios
LIBS:DiodosZener
LIBS:display
LIBS:dsp
LIBS:EEpromSerie
LIBS:EIA232
LIBS:EIA485
LIBS:elec-unifil
LIBS:ESD_Protection
LIBS: EsdProtectionDiode
LIBS:FerriteBead
LIBS:FotoDiodos
LIBS:ftdi
LIBS:Fusibles
LIBS:gennum
LIBS:graphic
LIBS:hc11
LIBS:Inductor
LIBS:intel
LIBS:interface
LIBS:ir
LIBS:IRF-MOSFET
LIBS:jumpers
LIBS:Lattice
LIBS:LDR_THT
LIBS:LEDs
LIBS:linear
LIBS:LoadGeneric
LIBS:Logic-MOSFET
LIBS:logo
LIBS:LPC17xx
LIBS:LPC43xx
LIBS:MagneticsProtection
LIBS:maxim
LIBS:memory
LIBS:microchip
LIBS:microchip_dspic33dsc
LIBS:microchip_pic10mcu
LIBS:microchip_pic12mcu
LIBS:microchip_pic16mcu
LIBS:microchip_pic18mcu
LIBS:microchip_pic32mcu
LIBS:microcontrollers
LIBS:motor_drivers
LIBS:motorola
LIBS:msp430
LIBS:nordicsemi
LIBS:NTR-MOSFET
LIBS:nxp_armmcu
LIBS:onsemi
LIBS:opto
LIBS:OptoTransistor
LIBS:OptoTriac
LIBS:Oscillators
LIBS:philips
LIBS:PicoFusePTC
LIBS:Pilas
LIBS:Potenciometro
LIBS:power
LIBS:PowerDistributionSwitch
LIBS:powerint
LIBS:Power_Management
LIBS:pspice
LIBS:Puentes
LIBS:R
LIBS:references
LIBS:RegLinealPositivos
LIBS:regul
LIBS:ReguladorLDO
LIBS:ReguladorSwitcher
LIBS:relays
LIBS:rfcom
LIBS:RMII_Ethernet
LIBS:R_SMD
LIBS:R_ThruHole
LIBS:R_THT
LIBS:SensorCurrent
LIBS:sensors
LIBS:SensorTemperatura
LIBS:ShifterLevel
LIBS:silabs
LIBS:siliconi
LIBS:SourceCurrent
LIBS:StickLPC
LIBS:stm8
LIBS:stm32
LIBS:supertex
LIBS:switches
LIBS:TactSwitch
LIBS:TestPoint
LIBS:texas
LIBS:TrafoReductor
LIBS:TransceiverCAN
LIBS:transf
LIBS:TransistorArrays
LIBS:transistors
LIBS:Triac
LIBS:ttl_ieee
LIBS:valves
LIBS:Varistor
LIBS:video
LIBS:VoltageReference
LIBS:Worldsemi
LIBS:Xicor
LIBS:xilinx
LIBS:xtal
LIBS:Zilog
LIBS:MemoryZock
LIBS:BoardMicro-cache
EELAYER 25 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 11 21
Title "BoardWithStick1300revD"
Date "12-05-2017"
Rev "v1.10b"
Comp "J.E.L"
Comment1 "\\n Luccioni, Jesus Emanuel"
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
Text HLabel 7900 3250 0    60   BiDi ~ 0
OneWire.1
Text HLabel 7900 4650 0    60   BiDi ~ 0
OneWire.2
$Comp
L GND #PWR0104
U 1 1 57BC0FE2
P 6100 3600
F 0 "#PWR0104" H 6100 3350 50  0001 C CNN
F 1 "GND" H 6100 3450 50  0000 C CNN
F 2 "" H 6100 3600 50  0000 C CNN
F 3 "" H 6100 3600 50  0000 C CNN
	1    6100 3600
	-1   0    0    -1  
$EndComp
Text Notes 6650 2050 0    60   ~ 0
One Wire, Protocol
$Comp
L BORx02-PITCH5mm CN8
U 1 1 57D5680E
P 5350 4050
F 0 "CN8" H 5350 4300 50  0000 C CNN
F 1 "OneWire2" H 4950 4050 50  0000 C CNN
F 2 "Connectors_Phoenix:PhoenixContact_MCV-G_02x3.81mm_Vertical" H 5400 3700 50  0001 C CNN
F 3 "" H 5400 3750 60  0001 C CNN
F 4 "OneWire-2" H 4750 4050 60  0001 C CNN "Name"
	1    5350 4050
	1    0    0    1   
$EndComp
Wire Wire Line
	5650 3950 5850 3950
Wire Wire Line
	5650 3500 6100 3500
Wire Wire Line
	5850 3950 5850 3500
Connection ~ 5850 3500
Wire Notes Line
	6550 1900 6550 5300
Wire Notes Line
	6550 5300 9100 5300
Wire Notes Line
	9100 5300 9100 1900
$Comp
L BORx02-PITCH5mm CN7
U 1 1 581CCCEA
P 5350 3400
F 0 "CN7" H 5350 3650 50  0000 C CNN
F 1 "OneWire1" H 4950 3400 50  0000 C CNN
F 2 "Connectors_Phoenix:PhoenixContact_MCV-G_02x3.81mm_Vertical" H 5400 3050 50  0001 C CNN
F 3 "" H 5400 3100 60  0001 C CNN
F 4 "OneWire-1" H 4750 3400 60  0001 C CNN "Name"
	1    5350 3400
	1    0    0    -1  
$EndComp
$Comp
L 4.7K_125mW_5% R50
U 1 1 59049E36
P 8000 2750
F 0 "R50" H 8200 2800 50  0000 C CNN
F 1 "4.7K_125mW_5%" H 8350 2700 50  0000 C CNN
F 2 "ResistorChipSMD:RchipSMD_0805_HandSold" H 8000 2500 50  0001 C CNN
F 3 "" H 8000 2750 30  0000 C CNN
	1    8000 2750
	-1   0    0    -1  
$EndComp
$Comp
L 4.7K_125mW_5% R51
U 1 1 59049F40
P 8000 4150
F 0 "R51" H 8200 4200 50  0000 C CNN
F 1 "4.7K_125mW_5%" H 8350 4100 50  0000 C CNN
F 2 "ResistorChipSMD:RchipSMD_0805_HandSold" H 8000 3900 50  0001 C CNN
F 3 "" H 8000 4150 30  0000 C CNN
	1    8000 4150
	-1   0    0    -1  
$EndComp
$Comp
L MMZ1608B102C FB7
U 1 1 59CFD392
P 7700 3050
F 0 "FB7" V 7600 3050 40  0000 C CNN
F 1 "MMZ1608B102C" V 7800 3050 40  0000 C CNN
F 2 "FerriteBeadSMD:FbSMD_0603_HandSold" V 7630 3050 30  0001 C CNN
F 3 "" H 7700 3050 30  0000 C CNN
	1    7700 3050
	0    -1   -1   0   
$EndComp
$Comp
L MMZ1608B102C FB8
U 1 1 59CFD4F2
P 7700 4450
F 0 "FB8" V 7600 4450 40  0000 C CNN
F 1 "MMZ1608B102C" V 7800 4450 40  0000 C CNN
F 2 "FerriteBeadSMD:FbSMD_0603_HandSold" V 7630 4450 30  0001 C CNN
F 3 "" H 7700 4450 30  0000 C CNN
	1    7700 4450
	0    -1   -1   0   
$EndComp
Wire Notes Line
	9100 1900 6550 1900
Wire Notes Line
	6550 2100 9100 2100
$Comp
L BAT54S D24
U 1 1 59D37258
P 8300 3050
F 0 "D24" H 8150 2900 50  0000 C CNN
F 1 "BAT54S" H 8000 3050 50  0000 C CNN
F 2 "SOT_SMD:SOT23_Handsold" H 8350 2750 50  0001 C CNN
F 3 "" V 8250 3035 60  0000 C CNN
	1    8300 3050
	-1   0    0    -1  
$EndComp
Wire Wire Line
	6100 3500 6100 3600
$Comp
L +3V3 #PWR0105
U 1 1 59D39103
P 8000 2450
F 0 "#PWR0105" H 8000 2300 50  0001 C CNN
F 1 "+3V3" H 8000 2590 50  0000 C CNN
F 2 "" H 8000 2450 50  0000 C CNN
F 3 "" H 8000 2450 50  0000 C CNN
	1    8000 2450
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR0106
U 1 1 59D3923F
P 8300 3350
F 0 "#PWR0106" H 8300 3100 50  0001 C CNN
F 1 "GND" H 8300 3200 50  0000 C CNN
F 2 "" H 8300 3350 50  0000 C CNN
F 3 "" H 8300 3350 50  0000 C CNN
	1    8300 3350
	1    0    0    -1  
$EndComp
Wire Wire Line
	8000 2450 8000 2550
Wire Wire Line
	8300 2850 8300 2500
Wire Wire Line
	8300 2500 8000 2500
Connection ~ 8000 2500
Wire Wire Line
	7900 3050 8150 3050
Wire Wire Line
	8300 3250 8300 3350
Wire Wire Line
	8000 2950 8000 3250
Connection ~ 8000 3050
Wire Wire Line
	8000 3250 7900 3250
Wire Wire Line
	6350 3050 7500 3050
$Comp
L BAT54S D25
U 1 1 59D3A9F9
P 8300 4450
F 0 "D25" H 8150 4300 50  0000 C CNN
F 1 "BAT54S" H 8000 4450 50  0000 C CNN
F 2 "SOT_SMD:SOT23_Handsold" H 8350 4150 50  0001 C CNN
F 3 "" V 8250 4435 60  0000 C CNN
	1    8300 4450
	-1   0    0    -1  
$EndComp
$Comp
L +3V3 #PWR0107
U 1 1 59D3AA00
P 8000 3850
F 0 "#PWR0107" H 8000 3700 50  0001 C CNN
F 1 "+3V3" H 8000 3990 50  0000 C CNN
F 2 "" H 8000 3850 50  0000 C CNN
F 3 "" H 8000 3850 50  0000 C CNN
	1    8000 3850
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR0108
U 1 1 59D3AA06
P 8300 4750
F 0 "#PWR0108" H 8300 4500 50  0001 C CNN
F 1 "GND" H 8300 4600 50  0000 C CNN
F 2 "" H 8300 4750 50  0000 C CNN
F 3 "" H 8300 4750 50  0000 C CNN
	1    8300 4750
	1    0    0    -1  
$EndComp
Wire Wire Line
	8000 3850 8000 3950
Wire Wire Line
	8300 4250 8300 3900
Wire Wire Line
	8300 3900 8000 3900
Connection ~ 8000 3900
Wire Wire Line
	7900 4450 8150 4450
Wire Wire Line
	8300 4650 8300 4750
Wire Wire Line
	8000 4350 8000 4650
Connection ~ 8000 4450
Wire Wire Line
	8000 4650 7900 4650
Wire Wire Line
	6350 4450 7500 4450
Wire Wire Line
	6350 4150 6350 4450
Wire Wire Line
	6350 3300 6350 3050
Wire Wire Line
	6350 4150 5650 4150
Wire Wire Line
	6350 3300 5650 3300
$EndSCHEMATC
