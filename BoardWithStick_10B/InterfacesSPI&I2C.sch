EESchema Schematic File Version 2
LIBS:74LVT
LIBS:74xgxx
LIBS:74xx
LIBS:ac-dc
LIBS:actel
LIBS:adc-dac
LIBS:ADC-DeltaSigma
LIBS:ADC-SAR
LIBS:Altera
LIBS:analog_devices
LIBS:AnalogMuxDemux
LIBS:analog_switches
LIBS:AopAudio
LIBS:AopComparador
LIBS:AopPrecision
LIBS:AopRailToRailPG
LIBS:atmel
LIBS:audio
LIBS:battery_management
LIBS:bbd
LIBS:BJT-GeneralPurpose 
LIBS:BOR-SIL
LIBS:BridgeUSB
LIBS:brooktre
LIBS:CapAluminioSMD
LIBS:CapCeramicoHVC
LIBS:CapCeramicos
LIBS:CapCer_THT
LIBS:CapElectroliticos
LIBS:CapPoly_THT
LIBS:CapTantalio
LIBS:Ccer_SMD
LIBS:cmos4000
LIBS:cmos_ieee
LIBS:CommonModeChoke 
LIBS:CompuertasLogicas
LIBS:CON-DIL_F
LIBS:CON-DIL_M
LIBS:Conectores
LIBS:ConectoresDB
LIBS:conn
LIBS:CON-SIL_F
LIBS:CON-SIL_M
LIBS:contrib
LIBS:CON-USB
LIBS:Cristal
LIBS:cypress
LIBS:dc-dc
LIBS:device
LIBS:digital-audio
LIBS:diode
LIBS:DiodoPequeñaSeñal
LIBS:DiodoRectificador
LIBS:DiodoSchottkyRect
LIBS:DiodosSupresorTransitorios
LIBS:DiodosZener
LIBS:display
LIBS:dsp
LIBS:EEpromSerie
LIBS:EIA232
LIBS:EIA485
LIBS:elec-unifil
LIBS:ESD_Protection
LIBS: EsdProtectionDiode
LIBS:FerriteBead
LIBS:FotoDiodos
LIBS:ftdi
LIBS:Fusibles
LIBS:gennum
LIBS:graphic
LIBS:hc11
LIBS:Inductor
LIBS:intel
LIBS:interface
LIBS:ir
LIBS:IRF-MOSFET
LIBS:jumpers
LIBS:Lattice
LIBS:LDR_THT
LIBS:LEDs
LIBS:linear
LIBS:LoadGeneric
LIBS:Logic-MOSFET
LIBS:logo
LIBS:LPC17xx
LIBS:LPC43xx
LIBS:MagneticsProtection
LIBS:maxim
LIBS:memory
LIBS:microchip
LIBS:microchip_dspic33dsc
LIBS:microchip_pic10mcu
LIBS:microchip_pic12mcu
LIBS:microchip_pic16mcu
LIBS:microchip_pic18mcu
LIBS:microchip_pic32mcu
LIBS:microcontrollers
LIBS:motor_drivers
LIBS:motorola
LIBS:msp430
LIBS:nordicsemi
LIBS:NTR-MOSFET
LIBS:nxp_armmcu
LIBS:onsemi
LIBS:opto
LIBS:OptoTransistor
LIBS:OptoTriac
LIBS:Oscillators
LIBS:philips
LIBS:PicoFusePTC
LIBS:Pilas
LIBS:Potenciometro
LIBS:power
LIBS:PowerDistributionSwitch
LIBS:powerint
LIBS:Power_Management
LIBS:pspice
LIBS:Puentes
LIBS:R
LIBS:references
LIBS:RegLinealPositivos
LIBS:regul
LIBS:ReguladorLDO
LIBS:ReguladorSwitcher
LIBS:relays
LIBS:rfcom
LIBS:RMII_Ethernet
LIBS:R_SMD
LIBS:R_ThruHole
LIBS:R_THT
LIBS:SensorCurrent
LIBS:sensors
LIBS:SensorTemperatura
LIBS:ShifterLevel
LIBS:silabs
LIBS:siliconi
LIBS:SourceCurrent
LIBS:StickLPC
LIBS:stm8
LIBS:stm32
LIBS:supertex
LIBS:switches
LIBS:TactSwitch
LIBS:TestPoint
LIBS:texas
LIBS:TrafoReductor
LIBS:TransceiverCAN
LIBS:transf
LIBS:TransistorArrays
LIBS:transistors
LIBS:Triac
LIBS:ttl_ieee
LIBS:valves
LIBS:Varistor
LIBS:video
LIBS:VoltageReference
LIBS:Worldsemi
LIBS:Xicor
LIBS:xilinx
LIBS:xtal
LIBS:Zilog
LIBS:BoardMicro-cache
EELAYER 25 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 15 21
Title "BoardWithStick1300revD"
Date "12-05-2017"
Rev "1.09"
Comp "Metrovias-SCP"
Comment1 "\\n Luccioni, Jesus Emanuel"
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L 100nF_25V_X7R C52
U 1 1 58FF3807
P 6100 4550
F 0 "C52" H 6100 4200 50  0000 C CNN
F 1 "100nF_25V_X7R" H 5700 4700 50  0000 C CNN
F 2 "CapacitorCeramicSMD:CcSMD_0805_HandSold" H 5750 4350 50  0001 C CNN
F 3 "" H 6100 4550 60  0000 C CNN
	1    6100 4550
	1    0    0    1   
$EndComp
$Comp
L GND #PWR0129
U 1 1 58FF3819
P 4400 4950
F 0 "#PWR0129" H 4400 4700 50  0001 C CNN
F 1 "GND" H 4400 4800 50  0000 C CNN
F 2 "" H 4400 4950 50  0000 C CNN
F 3 "" H 4400 4950 50  0000 C CNN
	1    4400 4950
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR0130
U 1 1 58FF383D
P 6250 4850
F 0 "#PWR0130" H 6250 4600 50  0001 C CNN
F 1 "GND" H 6250 4700 50  0000 C CNN
F 2 "" H 6250 4850 50  0000 C CNN
F 3 "" H 6250 4850 50  0000 C CNN
	1    6250 4850
	1    0    0    -1  
$EndComp
Text HLabel 5150 4600 2    60   BiDi ~ 0
SPI.MISO
Text HLabel 5150 4200 2    60   BiDi ~ 0
SPI.MOSI
Text HLabel 5150 4100 2    60   BiDi ~ 0
SPI.SSEL
Text HLabel 5150 4400 2    60   BiDi ~ 0
SPI.SCKL
$Comp
L +3V3 #PWR0131
U 1 1 590A5951
P 4200 3200
F 0 "#PWR0131" H 4200 3050 50  0001 C CNN
F 1 "+3V3" H 4200 3340 50  0000 C CNN
F 2 "" H 4200 3200 50  0000 C CNN
F 3 "" H 4200 3200 50  0000 C CNN
	1    4200 3200
	1    0    0    -1  
$EndComp
$Comp
L ZocaloMicroSD CON1
U 1 1 594B4FD0
P 3250 4400
F 0 "CON1" H 2600 5100 50  0000 C CNN
F 1 "ZocaloMicroSD" H 3900 5100 50  0000 R CNN
F 2 "" H 4400 4800 50  0000 C CNN
F 3 "" H 3250 4500 50  0000 C CNN
F 4 "name" H 2700 5200 50  0000 C CNN "name"
	1    3250 4400
	-1   0    0    -1  
$EndComp
$Comp
L 4.7K_125mW_5% R8
U 1 1 594B60E4
P 4450 3550
F 0 "R8" V 4450 4850 50  0000 C CNN
F 1 "4.7K_125mW_5%" V 4450 4400 50  0000 C CNN
F 2 "ResistorChipSMD:RchipSMD_0805_HandSold" H 4450 3300 50  0001 C CNN
F 3 "" H 4450 3550 30  0000 C CNN
	1    4450 3550
	-1   0    0    -1  
$EndComp
$Comp
L 4.7K_125mW_5% R9
U 1 1 594B6510
P 4550 3550
F 0 "R9" V 4550 4850 50  0000 C CNN
F 1 "4.7K_125mW_5%" V 4550 4400 50  0000 C CNN
F 2 "ResistorChipSMD:RchipSMD_0805_HandSold" H 4550 3300 50  0001 C CNN
F 3 "" H 4550 3550 30  0000 C CNN
	1    4550 3550
	-1   0    0    -1  
$EndComp
$Comp
L 4.7K_125mW_5% R26
U 1 1 594B6563
P 4650 3550
F 0 "R26" V 4650 4850 50  0000 C CNN
F 1 "4.7K_125mW_5%" V 4650 4400 50  0000 C CNN
F 2 "ResistorChipSMD:RchipSMD_0805_HandSold" H 4650 3300 50  0001 C CNN
F 3 "" H 4650 3550 30  0000 C CNN
	1    4650 3550
	-1   0    0    -1  
$EndComp
$Comp
L 4.7K_125mW_5% R27
U 1 1 594B65B5
P 4750 3550
F 0 "R27" V 4750 4850 50  0000 C CNN
F 1 "4.7K_125mW_5%" V 4750 4400 50  0000 C CNN
F 2 "ResistorChipSMD:RchipSMD_0805_HandSold" H 4750 3300 50  0001 C CNN
F 3 "" H 4750 3550 30  0000 C CNN
	1    4750 3550
	-1   0    0    -1  
$EndComp
$Comp
L 4.7K_125mW_5% R41
U 1 1 594B660A
P 4850 3550
F 0 "R41" V 4850 4850 50  0000 C CNN
F 1 "4.7K_125mW_5%" V 4850 4400 50  0000 C CNN
F 2 "ResistorChipSMD:RchipSMD_0805_HandSold" H 4850 3300 50  0001 C CNN
F 3 "" H 4850 3550 30  0000 C CNN
	1    4850 3550
	-1   0    0    -1  
$EndComp
$Comp
L 4.7K_125mW_5% R88
U 1 1 594B6662
P 4950 3550
F 0 "R88" V 4950 4850 50  0000 C CNN
F 1 "4.7K_125mW_5%" V 4950 4400 50  0000 C CNN
F 2 "ResistorChipSMD:RchipSMD_0805_HandSold" H 4950 3300 50  0001 C CNN
F 3 "" H 4950 3550 30  0000 C CNN
	1    4950 3550
	-1   0    0    -1  
$EndComp
Wire Wire Line
	6100 4650 6100 4750
Wire Wire Line
	6100 4750 6400 4750
Wire Wire Line
	6400 4750 6400 4650
Wire Wire Line
	6250 4850 6250 4750
Connection ~ 6250 4750
Wire Wire Line
	4950 3250 4950 3350
Wire Wire Line
	4200 3250 4950 3250
Wire Wire Line
	4200 3250 4200 3200
Wire Wire Line
	4450 3350 4450 3250
Connection ~ 4450 3250
Wire Wire Line
	4550 3350 4550 3250
Connection ~ 4550 3250
Wire Wire Line
	4650 3350 4650 3250
Connection ~ 4650 3250
Wire Wire Line
	4750 3350 4750 3250
Connection ~ 4750 3250
Wire Wire Line
	4850 3350 4850 3250
Connection ~ 4850 3250
Wire Wire Line
	4200 4100 5150 4100
Wire Wire Line
	4200 4200 5150 4200
Wire Wire Line
	4200 4400 5150 4400
Wire Wire Line
	4200 4600 5150 4600
Wire Wire Line
	4450 3750 4450 4000
Wire Wire Line
	4550 3750 4550 4100
Connection ~ 4550 4100
Wire Wire Line
	4650 3750 4650 4200
Connection ~ 4650 4200
Wire Wire Line
	4750 3750 4750 4400
Connection ~ 4750 4400
Wire Wire Line
	4850 3750 4850 4600
Connection ~ 4850 4600
Wire Wire Line
	4950 3750 4950 4700
Wire Wire Line
	4200 4800 4400 4800
Wire Wire Line
	4200 4500 4400 4500
Wire Wire Line
	4400 4500 4400 4950
$Comp
L NTR4101 Q9
U 1 1 594BA154
P 7450 4000
F 0 "Q9" H 7650 4000 50  0000 C CNN
F 1 "NTR4101" H 7750 4100 50  0000 C CNN
F 2 "SOT_SMD:SOT23_Handsold" H 7450 3700 50  0001 C CNN
F 3 "" H 7470 4005 60  0000 C CNN
	1    7450 4000
	-1   0    0    1   
$EndComp
$Comp
L 10K_125mW_5% R92
U 1 1 594BA354
P 7750 3800
F 0 "R92" H 7900 3750 50  0000 C CNN
F 1 "10K_125mW_5%" H 8150 3850 50  0000 C CNN
F 2 "ResistorChipSMD:RchipSMD_0805_HandSold" H 8200 3650 50  0001 C CNN
F 3 "" H 7750 3800 30  0000 C CNN
	1    7750 3800
	1    0    0    -1  
$EndComp
$Comp
L 1K_125mW_1% R93
U 1 1 594BA3DD
P 8000 4050
F 0 "R93" V 8200 4050 50  0000 C CNN
F 1 "1K_125mW_1%" V 8100 4050 50  0000 C CNN
F 2 "ResistorChipSMD:RchipSMD_0805_HandSold" H 8250 3950 50  0001 C CNN
F 3 "" H 8150 4150 30  0000 C CNN
	1    8000 4050
	0    1    1    0   
$EndComp
Wire Wire Line
	7700 4050 7800 4050
Wire Wire Line
	7750 4000 7750 4050
Connection ~ 7750 4050
$Comp
L 22uH_200mA L2
U 1 1 594BBDE2
P 7150 4300
F 0 "L2" V 7300 4300 50  0000 C CNN
F 1 "22uH_200mA" V 7200 4300 50  0000 C CNN
F 2 "none" V 7050 4300 50  0001 C CNN
F 3 "" H 7125 4300 50  0000 C CNN
	1    7150 4300
	0    1    1    0   
$EndComp
Wire Wire Line
	4200 4300 6950 4300
Wire Wire Line
	7450 4250 7450 4350
$Comp
L +3V3 #PWR0132
U 1 1 594BE343
P 7450 3500
F 0 "#PWR0132" H 7450 3350 50  0001 C CNN
F 1 "+3V3" H 7450 3650 39  0000 C CNN
F 2 "" H 7450 3500 50  0000 C CNN
F 3 "" H 7450 3500 50  0000 C CNN
	1    7450 3500
	1    0    0    -1  
$EndComp
Wire Wire Line
	7450 3500 7450 3750
Wire Wire Line
	7750 3600 7750 3550
Wire Wire Line
	7750 3550 7450 3550
Connection ~ 7450 3550
Text HLabel 8600 4050 2    60   Input ~ 0
PowerSD
Wire Wire Line
	8600 4050 8200 4050
Connection ~ 4400 4800
$Comp
L 4.7uF_10V_T491 C53
U 1 1 594C2BC2
P 6400 4550
F 0 "C53" H 6400 4900 50  0000 C CNN
F 1 "4.7uF_10V_T491" H 6750 4400 50  0000 C CNN
F 2 "CapacitorTantalumSMD:CtanlumSMD_Case-A_Hand" H 6800 4450 50  0001 C CNN
F 3 "" H 6600 4650 60  0000 C CNN
	1    6400 4550
	1    0    0    -1  
$EndComp
$Comp
L 560K_125mW_5% R89
U 1 1 594C2D33
P 7450 4550
F 0 "R89" H 7600 4500 50  0000 C CNN
F 1 "560K_125mW_5%" H 7850 4600 50  0000 C CNN
F 2 "ResistorChipSMD:RchipSMD_0805_HandSold" H 7900 4400 50  0001 C CNN
F 3 "" H 7450 4550 30  0000 C CNN
	1    7450 4550
	1    0    0    -1  
$EndComp
Connection ~ 7450 4300
$Comp
L LTST-C191KGKT D20
U 1 1 594C38A2
P 7450 4950
F 0 "D20" H 7600 5100 50  0000 C CNN
F 1 "LTST-C191KSKT" H 7850 5000 50  0000 C CNN
F 2 "LED_0603-HandSold" H 7750 4800 50  0001 C CNN
F 3 "" H 7650 4950 60  0001 C CNN
F 4 "AMBAR" H 7850 4900 50  0000 C CNN "Colour"
	1    7450 4950
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR0133
U 1 1 594C3B1A
P 7450 5150
F 0 "#PWR0133" H 7450 4900 50  0001 C CNN
F 1 "GND" H 7450 5000 50  0000 C CNN
F 2 "" H 7450 5150 50  0000 C CNN
F 3 "" H 7450 5150 50  0000 C CNN
	1    7450 5150
	1    0    0    -1  
$EndComp
Wire Wire Line
	7350 4300 7450 4300
Wire Wire Line
	6400 4450 6400 4300
Connection ~ 6400 4300
Wire Wire Line
	6100 4450 6100 4300
Connection ~ 6100 4300
Text Notes 2300 3450 0    60   ~ 0
 Bus SPI, p/MMC -> microSD\n   1- NC (Pull-Up only)\n   2- ~CS~ (Chip Select)\n   3- DI (MOSI)\n   4- Vdd\n   5- CLK (CLOCK)\n   6-  Vss\n   7- DO (MISO)\n   8- RSV (Pull-Up only)
Wire Wire Line
	4950 4700 4200 4700
Wire Wire Line
	4450 4000 4200 4000
$Comp
L PWR_FLAG #FLG0134
U 1 1 594AC0A3
P 6650 4050
F 0 "#FLG0134" H 6650 4145 50  0001 C CNN
F 1 "PWR_FLAG" H 6650 4230 50  0000 C CNN
F 2 "" H 6650 4050 50  0000 C CNN
F 3 "" H 6650 4050 50  0000 C CNN
	1    6650 4050
	1    0    0    -1  
$EndComp
Wire Wire Line
	6650 4050 6650 4300
Connection ~ 6650 4300
$EndSCHEMATC
