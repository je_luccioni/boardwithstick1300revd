EESchema Schematic File Version 2
LIBS:74LVT
LIBS:74xgxx
LIBS:74xx
LIBS:ac-dc
LIBS:actel
LIBS:adc-dac
LIBS:ADC-DeltaSigma
LIBS:ADC-SAR
LIBS:Altera
LIBS:analog_devices
LIBS:AnalogMuxDemux
LIBS:analog_switches
LIBS:AopAudio
LIBS:AopComparador
LIBS:AopPrecision
LIBS:AopRailToRailPG
LIBS:atmel
LIBS:audio
LIBS:battery_management
LIBS:bbd
LIBS:BJT-GeneralPurpose 
LIBS:BOR-SIL
LIBS:BridgeUSB
LIBS:brooktre
LIBS:CapAluminioSMD
LIBS:CapCeramicoHVC
LIBS:CapCeramicos
LIBS:CapCer_THT
LIBS:CapElectroliticos
LIBS:CapPoly_THT
LIBS:CapTantalio
LIBS:Ccer_SMD
LIBS:cmos4000
LIBS:cmos_ieee
LIBS:CommonModeChoke 
LIBS:CompuertasLogicas
LIBS:CON-DIL_F
LIBS:CON-DIL_M
LIBS:Conectores
LIBS:ConectoresDB
LIBS:conn
LIBS:CON-SIL_F
LIBS:CON-SIL_M
LIBS:contrib
LIBS:CON-USB
LIBS:Cristal
LIBS:cypress
LIBS:dc-dc
LIBS:device
LIBS:digital-audio
LIBS:diode
LIBS:DiodoPequeñaSeñal
LIBS:DiodoRectificador
LIBS:DiodoSchottkyRect
LIBS:DiodosSupresorTransitorios
LIBS:DiodosZener
LIBS:display
LIBS:dsp
LIBS:EEpromSerie
LIBS:EIA232
LIBS:EIA485
LIBS:elec-unifil
LIBS:ESD_Protection
LIBS: EsdProtectionDiode
LIBS:FerriteBead
LIBS:FotoDiodos
LIBS:ftdi
LIBS:Fusibles
LIBS:gennum
LIBS:graphic
LIBS:hc11
LIBS:Inductor
LIBS:intel
LIBS:interface
LIBS:ir
LIBS:IRF-MOSFET
LIBS:jumpers
LIBS:Lattice
LIBS:LDR_THT
LIBS:LEDs
LIBS:linear
LIBS:LoadGeneric
LIBS:Logic-MOSFET
LIBS:logo
LIBS:LPC17xx
LIBS:LPC43xx
LIBS:MagneticsProtection
LIBS:maxim
LIBS:memory
LIBS:microchip
LIBS:microchip_dspic33dsc
LIBS:microchip_pic10mcu
LIBS:microchip_pic12mcu
LIBS:microchip_pic16mcu
LIBS:microchip_pic18mcu
LIBS:microchip_pic32mcu
LIBS:microcontrollers
LIBS:motor_drivers
LIBS:motorola
LIBS:msp430
LIBS:nordicsemi
LIBS:NTR-MOSFET
LIBS:nxp_armmcu
LIBS:onsemi
LIBS:opto
LIBS:OptoTransistor
LIBS:OptoTriac
LIBS:Oscillators
LIBS:philips
LIBS:PicoFusePTC
LIBS:Pilas
LIBS:Potenciometro
LIBS:power
LIBS:PowerDistributionSwitch
LIBS:powerint
LIBS:Power_Management
LIBS:pspice
LIBS:Puentes
LIBS:R
LIBS:references
LIBS:RegLinealPositivos
LIBS:regul
LIBS:ReguladorLDO
LIBS:ReguladorSwitcher
LIBS:relays
LIBS:rfcom
LIBS:RMII_Ethernet
LIBS:R_SMD
LIBS:R_ThruHole
LIBS:R_THT
LIBS:SensorCurrent
LIBS:sensors
LIBS:SensorTemperatura
LIBS:ShifterLevel
LIBS:silabs
LIBS:siliconi
LIBS:SourceCurrent
LIBS:StickLPC
LIBS:stm8
LIBS:stm32
LIBS:supertex
LIBS:switches
LIBS:TactSwitch
LIBS:TestPoint
LIBS:texas
LIBS:TrafoReductor
LIBS:TransceiverCAN
LIBS:transf
LIBS:TransistorArrays
LIBS:transistors
LIBS:Triac
LIBS:ttl_ieee
LIBS:valves
LIBS:Varistor
LIBS:video
LIBS:VoltageReference
LIBS:Worldsemi
LIBS:Xicor
LIBS:xilinx
LIBS:xtal
LIBS:Zilog
LIBS:MemoryZock
LIBS:BoardMicro-cache
EELAYER 25 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 2 21
Title "BoardWithStick1300revD"
Date "12-05-2017"
Rev "v1.10b"
Comp "J.E.L"
Comment1 "\\n Luccioni, Jesus Emanuel"
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
Text GLabel 6100 4600 0    60   Input ~ 0
RESET
$Comp
L OM13000 STICK1
U 5 1 57D4C30E
P 7250 3400
F 0 "STICK1" H 7250 3250 60  0000 C CNN
F 1 "OM13000" H 7250 3400 79  0000 C CNB
F 2 "StickTHT:OM13000-THT" H 7090 3620 60  0001 C CNN
F 3 "" H 7090 3620 60  0000 C CNN
	5    7250 3400
	1    0    0    -1  
$EndComp
$Comp
L OM13000 STICK1
U 2 1 57D569EB
P 7200 4600
F 0 "STICK1" H 7200 4450 60  0000 C CNN
F 1 "OM13000" H 7200 4600 79  0000 C CNB
F 2 "StickTHT:OM13000-THT" H 7040 4820 60  0001 C CNN
F 3 "" H 7040 4820 60  0000 C CNN
	2    7200 4600
	1    0    0    -1  
$EndComp
NoConn ~ 6100 3050
NoConn ~ 6100 3150
NoConn ~ 6100 3250
NoConn ~ 6100 3350
NoConn ~ 6100 3450
NoConn ~ 6100 3550
NoConn ~ 6100 3650
NoConn ~ 6100 3750
Wire Wire Line
	6100 4600 6400 4600
$EndSCHEMATC
