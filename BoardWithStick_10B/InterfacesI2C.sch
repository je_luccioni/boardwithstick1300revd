EESchema Schematic File Version 2
LIBS:74LVT
LIBS:74xgxx
LIBS:74xx
LIBS:ac-dc
LIBS:actel
LIBS:adc-dac
LIBS:ADC-DeltaSigma
LIBS:ADC-SAR
LIBS:Altera
LIBS:analog_devices
LIBS:AnalogMuxDemux
LIBS:analog_switches
LIBS:AopAudio
LIBS:AopComparador
LIBS:AopPrecision
LIBS:AopRailToRailPG
LIBS:atmel
LIBS:audio
LIBS:battery_management
LIBS:bbd
LIBS:BJT-GeneralPurpose 
LIBS:BOR-SIL
LIBS:BridgeUSB
LIBS:brooktre
LIBS:CapAluminioSMD
LIBS:CapCeramicoHVC
LIBS:CapCeramicos
LIBS:CapCer_THT
LIBS:CapElectroliticos
LIBS:CapPoly_THT
LIBS:CapTantalio
LIBS:Ccer_SMD
LIBS:cmos4000
LIBS:cmos_ieee
LIBS:CommonModeChoke 
LIBS:CompuertasLogicas
LIBS:CON-DIL_F
LIBS:CON-DIL_M
LIBS:Conectores
LIBS:ConectoresDB
LIBS:conn
LIBS:CON-SIL_F
LIBS:CON-SIL_M
LIBS:contrib
LIBS:CON-USB
LIBS:Cristal
LIBS:cypress
LIBS:dc-dc
LIBS:device
LIBS:digital-audio
LIBS:diode
LIBS:DiodoPequeñaSeñal
LIBS:DiodoRectificador
LIBS:DiodoSchottkyRect
LIBS:DiodosSupresorTransitorios
LIBS:DiodosZener
LIBS:display
LIBS:dsp
LIBS:EEpromSerie
LIBS:EIA232
LIBS:EIA485
LIBS:elec-unifil
LIBS:ESD_Protection
LIBS: EsdProtectionDiode
LIBS:FerriteBead
LIBS:FotoDiodos
LIBS:ftdi
LIBS:Fusibles
LIBS:gennum
LIBS:graphic
LIBS:hc11
LIBS:Inductor
LIBS:intel
LIBS:interface
LIBS:ir
LIBS:IRF-MOSFET
LIBS:jumpers
LIBS:Lattice
LIBS:LDR_THT
LIBS:LEDs
LIBS:linear
LIBS:LoadGeneric
LIBS:Logic-MOSFET
LIBS:logo
LIBS:LPC17xx
LIBS:LPC43xx
LIBS:MagneticsProtection
LIBS:maxim
LIBS:memory
LIBS:microchip
LIBS:microchip_dspic33dsc
LIBS:microchip_pic10mcu
LIBS:microchip_pic12mcu
LIBS:microchip_pic16mcu
LIBS:microchip_pic18mcu
LIBS:microchip_pic32mcu
LIBS:microcontrollers
LIBS:motor_drivers
LIBS:motorola
LIBS:msp430
LIBS:nordicsemi
LIBS:NTR-MOSFET
LIBS:nxp_armmcu
LIBS:onsemi
LIBS:opto
LIBS:OptoTransistor
LIBS:OptoTriac
LIBS:Oscillators
LIBS:philips
LIBS:PicoFusePTC
LIBS:Pilas
LIBS:Potenciometro
LIBS:power
LIBS:PowerDistributionSwitch
LIBS:powerint
LIBS:Power_Management
LIBS:pspice
LIBS:Puentes
LIBS:R
LIBS:references
LIBS:RegLinealPositivos
LIBS:regul
LIBS:ReguladorLDO
LIBS:ReguladorSwitcher
LIBS:relays
LIBS:rfcom
LIBS:RMII_Ethernet
LIBS:R_SMD
LIBS:R_ThruHole
LIBS:R_THT
LIBS:SensorCurrent
LIBS:sensors
LIBS:SensorTemperatura
LIBS:ShifterLevel
LIBS:silabs
LIBS:siliconi
LIBS:SourceCurrent
LIBS:StickLPC
LIBS:stm8
LIBS:stm32
LIBS:supertex
LIBS:switches
LIBS:TactSwitch
LIBS:TestPoint
LIBS:texas
LIBS:TrafoReductor
LIBS:TransceiverCAN
LIBS:transf
LIBS:TransistorArrays
LIBS:transistors
LIBS:Triac
LIBS:ttl_ieee
LIBS:valves
LIBS:Varistor
LIBS:video
LIBS:VoltageReference
LIBS:Worldsemi
LIBS:Xicor
LIBS:xilinx
LIBS:xtal
LIBS:Zilog
LIBS:MemoryZock
LIBS:BoardMicro-cache
EELAYER 25 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 20 21
Title "BoardWithStick1300revD"
Date "12-05-2017"
Rev "v1.10b"
Comp "J.E.L"
Comment1 "\\n Luccioni, Jesus Emanuel"
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
Text HLabel 7050 3550 2    60   BiDi ~ 0
I2C.SCL
Text HLabel 7050 3450 2    60   BiDi ~ 0
I2C.SDA
$Comp
L +5V #PWR0167
U 1 1 594CF765
P 5700 3150
F 0 "#PWR0167" H 5700 3000 50  0001 C CNN
F 1 "+5V" H 5700 3290 50  0000 C CNN
F 2 "" H 5700 3150 50  0000 C CNN
F 3 "" H 5700 3150 50  0000 C CNN
	1    5700 3150
	-1   0    0    -1  
$EndComp
Text Notes 3400 7350 0    60   ~ 0
 @@ Para Futuro Diseño de Interfaces interna,\n   Control de Semaforos.\n   # PCA9626\n   # PCA9622
$Comp
L +3V3 #PWR0168
U 1 1 594CF7D1
P 5500 3150
F 0 "#PWR0168" H 5500 3000 50  0001 C CNN
F 1 "+3V3" H 5500 3290 50  0000 C CNN
F 2 "" H 5500 3150 50  0000 C CNN
F 3 "" H 5500 3150 50  0000 C CNN
	1    5500 3150
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR0169
U 1 1 59C148EE
P 5500 4100
F 0 "#PWR0169" H 5500 3850 50  0001 C CNN
F 1 "GND" H 5500 3950 50  0000 C CNN
F 2 "" H 5500 4100 50  0000 C CNN
F 3 "" H 5500 4100 50  0000 C CNN
	1    5500 4100
	1    0    0    -1  
$EndComp
$Comp
L DIL-M_2x05PITCH2.54 CN11
U 1 1 59C16B56
P 6100 3650
F 0 "CN11" H 6100 4000 60  0000 C CNN
F 1 "I2C-3V3-5V" H 6100 4100 60  0000 C CNN
F 2 "CON-DIL-Male:DIL-M_2x05PITCH2.54" H 6100 3150 50  0001 C CNB
F 3 "" H 6250 3500 60  0001 C CNN
F 4 "Name" H 6100 4100 60  0001 C CNN "Name"
F 5 "5V" H 5750 3900 50  0000 C CNN "CN.1"
F 6 "3V3" H 5750 3800 50  0000 C CNN "CN.3"
F 7 "GND" H 5750 3700 50  0000 C CNN "CN.5"
F 8 "SDA_3V3" H 5750 3600 50  0000 C CNN "CN.7"
F 9 "SCL_3V3" H 5750 3500 50  0000 C CNN "CN.9"
F 10 "SDA_3V3" H 6450 3900 50  0000 C CNN "CN.2"
F 11 "SCL_3V3" H 6450 3800 50  0000 C CNN "CN.4"
F 12 "GND" H 6450 3700 50  0000 C CNN "CN.6"
F 13 "3V3" H 6450 3600 50  0000 C CNN "CN.8"
F 14 "5V" H 6450 3500 50  0000 C CNN "CN.10"
	1    6100 3650
	1    0    0    -1  
$EndComp
Wire Wire Line
	6400 3750 6750 3750
Wire Wire Line
	6400 3850 6950 3850
Wire Wire Line
	7050 3450 6400 3450
Wire Wire Line
	7050 3550 6400 3550
Wire Wire Line
	6600 3650 6400 3650
Wire Wire Line
	5800 3450 5700 3450
Wire Wire Line
	5700 3450 5700 3150
Wire Wire Line
	5800 3550 5500 3550
Wire Wire Line
	5500 3550 5500 3150
Wire Wire Line
	5800 3650 5500 3650
Wire Wire Line
	5500 3650 5500 4100
Wire Wire Line
	5350 3750 5800 3750
Wire Wire Line
	5800 3850 5350 3850
NoConn ~ 5350 3750
NoConn ~ 5350 3850
NoConn ~ 6750 3750
NoConn ~ 6950 3850
$Comp
L GND #PWR0170
U 1 1 59C1F3F1
P 6600 4100
F 0 "#PWR0170" H 6600 3850 50  0001 C CNN
F 1 "GND" H 6600 3950 50  0000 C CNN
F 2 "" H 6600 4100 50  0000 C CNN
F 3 "" H 6600 4100 50  0000 C CNN
	1    6600 4100
	1    0    0    -1  
$EndComp
Wire Wire Line
	6600 3650 6600 4100
Text Notes 3350 6850 0    60   ~ 0
 @ note :
Wire Notes Line
	3350 6700 3350 7500
Wire Notes Line
	3350 7500 5800 7500
Wire Notes Line
	5800 7500 5800 6700
Wire Notes Line
	5800 6700 3350 6700
Wire Notes Line
	3350 6900 5800 6900
$EndSCHEMATC
