EESchema Schematic File Version 2
LIBS:74LVT
LIBS:74xgxx
LIBS:74xx
LIBS:ac-dc
LIBS:actel
LIBS:adc-dac
LIBS:ADC-DeltaSigma
LIBS:ADC-SAR
LIBS:Altera
LIBS:analog_devices
LIBS:AnalogMuxDemux
LIBS:analog_switches
LIBS:AopAudio
LIBS:AopComparador
LIBS:AopPrecision
LIBS:AopRailToRailPG
LIBS:atmel
LIBS:audio
LIBS:battery_management
LIBS:bbd
LIBS:BJT-GeneralPurpose 
LIBS:BOR-SIL
LIBS:BridgeUSB
LIBS:brooktre
LIBS:CapAluminioSMD
LIBS:CapCeramicoHVC
LIBS:CapCeramicos
LIBS:CapCer_THT
LIBS:CapElectroliticos
LIBS:CapPoly_THT
LIBS:CapTantalio
LIBS:Ccer_SMD
LIBS:cmos4000
LIBS:cmos_ieee
LIBS:CommonModeChoke 
LIBS:CompuertasLogicas
LIBS:CON-DIL_F
LIBS:CON-DIL_M
LIBS:Conectores
LIBS:ConectoresDB
LIBS:conn
LIBS:CON-SIL_F
LIBS:CON-SIL_M
LIBS:contrib
LIBS:CON-USB
LIBS:Cristal
LIBS:cypress
LIBS:dc-dc
LIBS:device
LIBS:digital-audio
LIBS:diode
LIBS:DiodoPequeñaSeñal
LIBS:DiodoRectificador
LIBS:DiodoSchottkyRect
LIBS:DiodosSupresorTransitorios
LIBS:DiodosZener
LIBS:display
LIBS:dsp
LIBS:EEpromSerie
LIBS:EIA232
LIBS:EIA485
LIBS:elec-unifil
LIBS:ESD_Protection
LIBS: EsdProtectionDiode
LIBS:FerriteBead
LIBS:FotoDiodos
LIBS:ftdi
LIBS:Fusibles
LIBS:gennum
LIBS:graphic
LIBS:hc11
LIBS:Inductor
LIBS:intel
LIBS:interface
LIBS:ir
LIBS:IRF-MOSFET
LIBS:jumpers
LIBS:Lattice
LIBS:LDR_THT
LIBS:LEDs
LIBS:linear
LIBS:LoadGeneric
LIBS:Logic-MOSFET
LIBS:logo
LIBS:LPC17xx
LIBS:LPC43xx
LIBS:MagneticsProtection
LIBS:maxim
LIBS:memory
LIBS:microchip
LIBS:microchip_dspic33dsc
LIBS:microchip_pic10mcu
LIBS:microchip_pic12mcu
LIBS:microchip_pic16mcu
LIBS:microchip_pic18mcu
LIBS:microchip_pic32mcu
LIBS:microcontrollers
LIBS:motor_drivers
LIBS:motorola
LIBS:msp430
LIBS:nordicsemi
LIBS:NTR-MOSFET
LIBS:nxp_armmcu
LIBS:onsemi
LIBS:opto
LIBS:OptoTransistor
LIBS:OptoTriac
LIBS:Oscillators
LIBS:philips
LIBS:PicoFusePTC
LIBS:Pilas
LIBS:Potenciometro
LIBS:power
LIBS:PowerDistributionSwitch
LIBS:powerint
LIBS:Power_Management
LIBS:pspice
LIBS:Puentes
LIBS:R
LIBS:references
LIBS:RegLinealPositivos
LIBS:regul
LIBS:ReguladorLDO
LIBS:ReguladorSwitcher
LIBS:relays
LIBS:rfcom
LIBS:RMII_Ethernet
LIBS:R_SMD
LIBS:R_ThruHole
LIBS:R_THT
LIBS:SensorCurrent
LIBS:sensors
LIBS:SensorTemperatura
LIBS:ShifterLevel
LIBS:silabs
LIBS:siliconi
LIBS:SourceCurrent
LIBS:StickLPC
LIBS:stm8
LIBS:stm32
LIBS:supertex
LIBS:switches
LIBS:TactSwitch
LIBS:TestPoint
LIBS:texas
LIBS:TrafoReductor
LIBS:TransceiverCAN
LIBS:transf
LIBS:TransistorArrays
LIBS:transistors
LIBS:Triac
LIBS:ttl_ieee
LIBS:valves
LIBS:Varistor
LIBS:video
LIBS:VoltageReference
LIBS:Worldsemi
LIBS:Xicor
LIBS:xilinx
LIBS:xtal
LIBS:Zilog
LIBS:MemoryZock
LIBS:BoardMicro-cache
EELAYER 25 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 21
Title "BoardWithStick1300revD"
Date "12-05-2017"
Rev "v1.10b"
Comp "J.E.L"
Comment1 "\\n Luccioni, Jesus Emanuel"
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Sheet
S 9150 4050 1250 500 
U 56FB6AEF
F0 "Osc&Reset&JTAG" 60
F1 "OscResetJTAG.sch" 60
$EndSheet
$Sheet
S 9150 4850 1250 500 
U 56FA8976
F0 "PowerSource" 60
F1 "PowerSource.sch" 60
$EndSheet
$Sheet
S 1950 2350 1400 600 
U 56FB3784
F0 "Ethernet&TCP-IP" 60
F1 "Ethernet.sch" 60
F2 "ETH.TXP" B R 3350 2500 60 
F3 "ETH.TXN" B R 3350 2600 60 
F4 "ETH.RXP" B R 3350 2700 60 
F5 "ETH.RXN" B R 3350 2800 60 
$EndSheet
$Sheet
S 1950 1750 1400 300 
U 56FEA0A3
F0 "ComunicacionPC" 60
F1 "CommunicationPC.sch" 60
F2 "BridgeUSB.RxD" I R 3350 1950 60 
F3 "BridgeUSB.TxD" O R 3350 1850 60 
$EndSheet
$Sheet
S 1950 4050 1400 300 
U 57299CBA
F0 "Pulsadores" 60
F1 "Pushbuttons.sch" 60
F2 "SW2" O R 3350 4150 60 
F3 "SW3" O R 3350 4250 60 
$EndSheet
Wire Wire Line
	3350 4800 3650 4800
Wire Wire Line
	3350 4900 3650 4900
Wire Wire Line
	3350 5000 3650 5000
Wire Wire Line
	3350 5100 3650 5100
Wire Wire Line
	3350 6100 3650 6100
Wire Wire Line
	3650 5900 3350 5900
Wire Wire Line
	3350 6000 3650 6000
$Sheet
S 1950 3250 1400 500 
U 579D78DC
F0 "AnalogInput" 60
F1 "AnalogInput.sch" 60
F2 "AnalogInp.1" O R 3350 3350 60 
F3 "AnalogInp.2" O R 3350 3450 60 
F4 "AnalogInp.3" O R 3350 3550 60 
F5 "AnalogInp.4" O R 3350 3650 60 
$EndSheet
Wire Wire Line
	3650 6200 3350 6200
Wire Wire Line
	3350 6300 3650 6300
$Sheet
S 1950 1000 1400 450 
U 57073E89
F0 "Expansión_1" 60
F1 "Expansion_1.sch" 60
F2 "OneWire.1" B R 3350 1150 60 
F3 "OneWire.2" B R 3350 1300 60 
$EndSheet
$Sheet
S 1950 5800 1400 700 
U 5733C89F
F0 "DigitalInput" 60
F1 "EntradaDigital.sch" 60
F2 "InpOpto.1" O R 3350 5900 60 
F3 "InpOpto.2" O R 3350 6000 60 
F4 "InpOpto.3" O R 3350 6100 60 
F5 "InpOpto.4" O R 3350 6200 60 
F6 "InpOpto.5" O R 3350 6300 60 
F7 "InpOpto.6" O R 3350 6400 60 
$EndSheet
Wire Wire Line
	3350 6400 3650 6400
$Sheet
S 1950 4650 1400 750 
U 58FF1395
F0 "InterfacesSPI" 60
F1 "InterfacesSPI.sch" 60
F2 "SPI.MISO" B R 3350 4900 60 
F3 "SPI.MOSI" B R 3350 4800 60 
F4 "SPI.SSEL" B R 3350 5100 60 
F5 "SPI.SCKL" B R 3350 5000 60 
F6 "PowerSD" I R 3350 5250 60 
$EndSheet
$Sheet
S 6750 5450 1450 900 
U 59033696
F0 "Rele&OpenColector" 60
F1 "Rele&OpenColector.sch" 60
F2 "Rele.1" I L 6750 5550 60 
F3 "Rele.2" I L 6750 5650 60 
F4 "OpenC.1" I L 6750 5800 60 
F5 "OpenC.2" I L 6750 5900 60 
F6 "OpenC.3" I L 6750 6000 60 
F7 "OpenC.4" I L 6750 6100 60 
F8 "OpenC.5" I L 6750 6200 60 
$EndSheet
Wire Wire Line
	6750 5550 6450 5550
Wire Wire Line
	6450 5650 6750 5650
Wire Wire Line
	6750 5800 6450 5800
Wire Wire Line
	6450 5900 6750 5900
Wire Wire Line
	6750 6000 6450 6000
Wire Wire Line
	6450 6100 6750 6100
Wire Wire Line
	6750 6200 6450 6200
Wire Wire Line
	3650 4250 3350 4250
Wire Wire Line
	3350 4150 3650 4150
Wire Wire Line
	3350 3650 3650 3650
Wire Wire Line
	3650 3550 3350 3550
Wire Wire Line
	3350 3450 3650 3450
Wire Wire Line
	3650 3350 3350 3350
Wire Wire Line
	3650 2500 3350 2500
Wire Wire Line
	3350 2600 3650 2600
Wire Wire Line
	3650 2700 3350 2700
Wire Wire Line
	3350 2800 3650 2800
Wire Wire Line
	3650 1850 3350 1850
Wire Wire Line
	3350 1950 3650 1950
Wire Wire Line
	3650 1150 3350 1150
Wire Wire Line
	3350 1300 3650 1300
Wire Wire Line
	6450 3800 6750 3800
Wire Wire Line
	6750 3900 6450 3900
Wire Wire Line
	6750 4150 6450 4150
Wire Wire Line
	6450 4250 6750 4250
Wire Wire Line
	6750 4350 6450 4350
$Sheet
S 6750 950  1450 600 
U 590200A4
F0 "USB_Host" 60
F1 "USB_Host.sch" 60
F2 "USB_D+" B L 6750 1050 60 
F3 "USB_D-" B L 6750 1150 60 
F4 "FlgFaultPowerUSB" O L 6750 1400 60 
F5 "EnablePowerUSB" I L 6750 1300 60 
$EndSheet
Wire Wire Line
	6450 1050 6750 1050
Wire Wire Line
	6750 1150 6450 1150
Wire Wire Line
	6450 1300 6750 1300
Wire Wire Line
	6750 1400 6450 1400
Wire Wire Line
	6450 2000 6750 2000
Wire Wire Line
	6450 2100 6750 2100
Wire Wire Line
	6750 2500 6450 2500
Wire Wire Line
	6450 2700 6750 2700
Wire Wire Line
	6750 2800 6450 2800
Wire Wire Line
	6450 2900 6750 2900
Wire Wire Line
	6450 3000 6750 3000
Wire Wire Line
	6750 3100 6450 3100
Wire Wire Line
	6750 3700 6450 3700
$Sheet
S 3650 750  2800 5750
U 56FA160B
F0 "Puertos" 60
F1 "Ports.sch" 60
F2 "P0.3_RXD0" I L 3650 1850 60 
F3 "P0.2_TXD0" O L 3650 1950 60 
F4 "P0.9_MOSI1" B L 3650 4800 60 
F5 "P0.8_MISO1" B L 3650 4900 60 
F6 "P0.7_SCK1" B L 3650 5000 60 
F7 "P0.6_SSEL1" B L 3650 5100 60 
F8 "P0.23_ADC0.0" I L 3650 3350 60 
F9 "P0.24_ADC0.1" I L 3650 3450 60 
F10 "P0.25_ADC0.2" I L 3650 3550 60 
F11 "P2.9_GPIO2.9" B L 3650 4250 60 
F12 "ETH.RD-" B L 3650 2800 60 
F13 "ETH.RD+" B L 3650 2700 60 
F14 "ETH.TD-" B L 3650 2600 60 
F15 "ETH.TD+" B L 3650 2500 60 
F16 "P1.19_CAP1.1" B L 3650 1150 60 
F17 "P0.1_CAN1.TDX" B R 6450 4250 60 
F18 "P0.0_CAN1.RDX" B R 6450 4150 60 
F19 "P1.27_CAP0.1" B L 3650 1300 60 
F20 "P0.27_I2C0.SDA" B R 6450 5000 60 
F21 "P0.28_I2C0.SCL" B R 6450 4900 60 
F22 "P1.23_GPIO1.23" B R 6450 2600 60 
F23 "P0.4_GPIO0.4" B R 6450 2100 60 
F24 "P1.24_GPIO1.24" B R 6450 2800 60 
F25 "P0.5_GPIO0.5" B R 6450 2300 60 
F26 "P1.30_VBUS" I R 6450 2500 60 
F27 "P1.25_GPIO1.25" B L 3650 4150 60 
F28 "P0.26_ADC0.3_AOUT_RXD3" B L 3650 3650 60 
F29 "P0.30_USB_D-" B R 6450 1150 60 
F30 "P0.29_USB_D+" B R 6450 1050 60 
F31 "P0.15_UART1.TX" O R 6450 3900 60 
F32 "P0.16_UART1.RX" I R 6450 3800 60 
F33 "P0.10_UART2.TX_I2C2.SDA" B R 6450 2000 60 
F34 "P0.11_UART2.RX_I2C2.SCL" B R 6450 2200 60 
F35 "P2.6_GPIO2.6" O R 6450 2400 60 
F36 "P2.7_GPIO2.7" O R 6450 4350 60 
F37 "P2.8_GPIO2.8" O R 6450 3700 60 
F38 "P0.17_GPIO0.17" T R 6450 2900 60 
F39 "P0.18_GPIO0.18" B R 6450 3000 60 
F40 "P0.21_GPIO0.21" B R 6450 3100 60 
F41 "P2.5_GPIO2.5" O R 6450 5900 60 
F42 "P2.0_GPIO2.0" O R 6450 5550 60 
F43 "P2.1_GPIO2.1" O R 6450 5650 60 
F44 "P2.3_PWM1.4" O R 6450 6100 60 
F45 "P2.4_PWM1.5" O R 6450 6200 60 
F46 "P2.2_GPIO2.2" O R 6450 5800 60 
F47 "P1.18_PWM1.1" O R 6450 6000 60 
F48 "P1.22_MAT1.0" I L 3650 6200 60 
F49 "P1.29_PCAP1.1" I L 3650 6400 60 
F50 "P1.26_CAP0.0" I L 3650 6300 60 
F51 "P2.12_EINT2" I L 3650 6000 60 
F52 "P2.11_EINT1" I L 3650 5900 60 
F53 "P2.13_EINT3" I L 3650 6100 60 
F54 "P1.31_ADC0.5" B R 6450 2700 60 
F55 "P1.20_PWM1.2" B L 3650 5250 60 
F56 "P4.28_GPIO4.28" B R 6450 1300 60 
F57 "P4.29_GPIO4.29" B R 6450 1400 60 
$EndSheet
Text Notes 9200 3750 0    79   ~ 0
01- Root                 \n  02- Osc&Reset&JTG      \n  03- PowerSource        \n  04- Ethernet&TCP-IP    \n  05- ComunicacionPC     \n  06- Pulsadores         \n  07- AnalogInput        \n    08- MedicionPila     \n    09- BrighnessSensing \n    10- InstrSenseCurrent\n  11- Expansion_1        \n  12- DigitalInput       \n    13- OptoEntradas_1   \n    14- OptoEntradas_2   \n  15- InterfacesSPI      \n  16- Rele&OpenColector  \n  17- USB_Host           \n  18- Puertos\n  19- CAN&RS485   \n  20- InterfacesI2C\n  21- GPIOs
Wire Wire Line
	6750 2600 6450 2600
$Sheet
S 6750 3550 1450 950 
U 59014DF4
F0 "CAN&RS485" 60
F1 "CAN&RS485.sch" 60
F2 "CAN_1.RDX" B L 6750 4150 60 
F3 "CAN_1.TDX" B L 6750 4250 60 
F4 "CAN_1.STR" I L 6750 4350 60 
F5 "RS485_1.RDX" O L 6750 3800 60 
F6 "RS485_1.TDX" I L 6750 3900 60 
F7 "RS485_1.Dir" I L 6750 3700 60 
$EndSheet
$Sheet
S 6750 4800 1450 350 
U 594CD2A5
F0 "InterfacesI2C" 60
F1 "InterfacesI2C.sch" 60
F2 "I2C.SCL" B L 6750 4900 60 
F3 "I2C.SDA" B L 6750 5000 60 
$EndSheet
Wire Wire Line
	6750 4900 6450 4900
Wire Wire Line
	6450 5000 6750 5000
Wire Wire Line
	3350 5250 3650 5250
Text Notes 9200 6300 0    60   ~ 0
Tamaño maximo 2.5 [dm]^2 \n	15[cm] x 16[cm] -> recomendado\n	15,5[cm] x 16[cm] -> Limite
$Sheet
S 6750 1850 1450 1400
U 59086A2B
F0 "GPIOs" 60
F1 "GPIOs.sch" 60
F2 "GPIO.2" B L 6750 2100 60 
F3 "GPIO.1" B L 6750 2000 60 
F4 "GPIO.3" B L 6750 2200 60 
F5 "GPIO.5" B L 6750 2400 60 
F6 "GPIO.7" B L 6750 2600 60 
F7 "GPIO.9" B L 6750 2800 60 
F8 "GPIO.11" B L 6750 3000 60 
F9 "GPIO.4" B L 6750 2300 60 
F10 "GPIO.6" B L 6750 2500 60 
F11 "GPIO.8" B L 6750 2700 60 
F12 "GPIO.10" B L 6750 2900 60 
F13 "GPIO.12" B L 6750 3100 60 
$EndSheet
Wire Wire Line
	6750 2200 6450 2200
Wire Wire Line
	6450 2300 6750 2300
Wire Wire Line
	6750 2400 6450 2400
Wire Notes Line
	9050 850  9050 3850
Wire Notes Line
	9050 3850 11050 3850
Wire Notes Line
	11050 3850 11050 850 
Wire Notes Line
	11050 850  9050 850 
Wire Notes Line
	9050 1050 11050 1050
Text Notes 9100 1000 0    60   ~ 0
iNDEX :
$EndSCHEMATC
