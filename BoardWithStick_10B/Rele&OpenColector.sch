EESchema Schematic File Version 2
LIBS:74LVT
LIBS:74xgxx
LIBS:74xx
LIBS:ac-dc
LIBS:actel
LIBS:adc-dac
LIBS:ADC-DeltaSigma
LIBS:ADC-SAR
LIBS:Altera
LIBS:analog_devices
LIBS:AnalogMuxDemux
LIBS:analog_switches
LIBS:AopAudio
LIBS:AopComparador
LIBS:AopPrecision
LIBS:AopRailToRailPG
LIBS:atmel
LIBS:audio
LIBS:battery_management
LIBS:bbd
LIBS:BJT-GeneralPurpose 
LIBS:BOR-SIL
LIBS:BridgeUSB
LIBS:brooktre
LIBS:CapAluminioSMD
LIBS:CapCeramicoHVC
LIBS:CapCeramicos
LIBS:CapCer_THT
LIBS:CapElectroliticos
LIBS:CapPoly_THT
LIBS:CapTantalio
LIBS:Ccer_SMD
LIBS:cmos4000
LIBS:cmos_ieee
LIBS:CommonModeChoke 
LIBS:CompuertasLogicas
LIBS:CON-DIL_F
LIBS:CON-DIL_M
LIBS:Conectores
LIBS:ConectoresDB
LIBS:conn
LIBS:CON-SIL_F
LIBS:CON-SIL_M
LIBS:contrib
LIBS:CON-USB
LIBS:Cristal
LIBS:cypress
LIBS:dc-dc
LIBS:device
LIBS:digital-audio
LIBS:diode
LIBS:DiodoPequeñaSeñal
LIBS:DiodoRectificador
LIBS:DiodoSchottkyRect
LIBS:DiodosSupresorTransitorios
LIBS:DiodosZener
LIBS:display
LIBS:dsp
LIBS:EEpromSerie
LIBS:EIA232
LIBS:EIA485
LIBS:elec-unifil
LIBS:ESD_Protection
LIBS: EsdProtectionDiode
LIBS:FerriteBead
LIBS:FotoDiodos
LIBS:ftdi
LIBS:Fusibles
LIBS:gennum
LIBS:graphic
LIBS:hc11
LIBS:Inductor
LIBS:intel
LIBS:interface
LIBS:ir
LIBS:IRF-MOSFET
LIBS:jumpers
LIBS:Lattice
LIBS:LDR_THT
LIBS:LEDs
LIBS:linear
LIBS:LoadGeneric
LIBS:Logic-MOSFET
LIBS:logo
LIBS:LPC17xx
LIBS:LPC43xx
LIBS:MagneticsProtection
LIBS:maxim
LIBS:memory
LIBS:microchip
LIBS:microchip_dspic33dsc
LIBS:microchip_pic10mcu
LIBS:microchip_pic12mcu
LIBS:microchip_pic16mcu
LIBS:microchip_pic18mcu
LIBS:microchip_pic32mcu
LIBS:microcontrollers
LIBS:motor_drivers
LIBS:motorola
LIBS:msp430
LIBS:nordicsemi
LIBS:NTR-MOSFET
LIBS:nxp_armmcu
LIBS:onsemi
LIBS:opto
LIBS:OptoTransistor
LIBS:OptoTriac
LIBS:Oscillators
LIBS:philips
LIBS:PicoFusePTC
LIBS:Pilas
LIBS:Potenciometro
LIBS:power
LIBS:PowerDistributionSwitch
LIBS:powerint
LIBS:Power_Management
LIBS:pspice
LIBS:Puentes
LIBS:R
LIBS:references
LIBS:RegLinealPositivos
LIBS:regul
LIBS:ReguladorLDO
LIBS:ReguladorSwitcher
LIBS:relays
LIBS:rfcom
LIBS:RMII_Ethernet
LIBS:R_SMD
LIBS:R_ThruHole
LIBS:R_THT
LIBS:SensorCurrent
LIBS:sensors
LIBS:SensorTemperatura
LIBS:ShifterLevel
LIBS:silabs
LIBS:siliconi
LIBS:SourceCurrent
LIBS:StickLPC
LIBS:stm8
LIBS:stm32
LIBS:supertex
LIBS:switches
LIBS:TactSwitch
LIBS:TestPoint
LIBS:texas
LIBS:TrafoReductor
LIBS:TransceiverCAN
LIBS:transf
LIBS:TransistorArrays
LIBS:transistors
LIBS:Triac
LIBS:ttl_ieee
LIBS:valves
LIBS:Varistor
LIBS:video
LIBS:VoltageReference
LIBS:Worldsemi
LIBS:Xicor
LIBS:xilinx
LIBS:xtal
LIBS:Zilog
LIBS:MemoryZock
LIBS:BoardMicro-cache
EELAYER 25 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 16 21
Title "BoardWithStick1300revD"
Date "12-05-2017"
Rev "v1.10b"
Comp "J.E.L"
Comment1 "\\n Luccioni, Jesus Emanuel"
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L +5V #PWR0131
U 1 1 58FF81A3
P 3350 1250
F 0 "#PWR0131" H 3350 1100 50  0001 C CNN
F 1 "+5V" H 3350 1390 50  0000 C CNN
F 2 "" H 3350 1250 50  0000 C CNN
F 3 "" H 3350 1250 50  0000 C CNN
	1    3350 1250
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR0132
U 1 1 58FF81C1
P 3350 2900
F 0 "#PWR0132" H 3350 2650 50  0001 C CNN
F 1 "GND" H 3350 2750 50  0000 C CNN
F 2 "" H 3350 2900 50  0000 C CNN
F 3 "" H 3350 2900 50  0000 C CNN
	1    3350 2900
	1    0    0    -1  
$EndComp
Text Label 4250 1950 0    60   ~ 0
ActRele1
Text Label 4250 2050 0    60   ~ 0
ActRele2
Text HLabel 2650 1950 0    60   Input ~ 0
Rele.1
Text HLabel 2650 2050 0    60   Input ~ 0
Rele.2
Text HLabel 2650 2150 0    60   Input ~ 0
OpenC.1
Text HLabel 2650 2250 0    60   Input ~ 0
OpenC.2
Text HLabel 2650 2350 0    60   Input ~ 0
OpenC.3
Text HLabel 2650 2450 0    60   Input ~ 0
OpenC.4
Text HLabel 2650 2550 0    60   Input ~ 0
OpenC.5
$Comp
L +5V #PWR0133
U 1 1 58FF8402
P 7450 4500
F 0 "#PWR0133" H 7450 4350 50  0001 C CNN
F 1 "+5V" H 7450 4640 50  0000 C CNN
F 2 "" H 7450 4500 50  0000 C CNN
F 3 "" H 7450 4500 50  0000 C CNN
	1    7450 4500
	1    0    0    -1  
$EndComp
Text Label 7300 5500 2    60   ~ 0
ActRele2
$Comp
L BORx03-PITCH5mm CN16
U 1 1 58FF96F3
P 10000 4950
F 0 "CN16" H 10000 5300 50  0000 C CNN
F 1 "Rele2" H 10000 5400 50  0000 C CNN
F 2 "Connectors_Phoenix:PhoenixContact_MCV-G_03x3.81mm_Vertical" H 10000 4450 50  0001 C CNN
F 3 "" H 10050 4750 60  0001 C CNN
F 4 "Rele2" H 10000 5450 60  0001 C CNN "Name"
	1    10000 4950
	-1   0    0    -1  
$EndComp
Text Label 9500 4750 2    60   ~ 0
Rele2.NA
Text Label 9500 5150 2    60   ~ 0
Rele2.NC
Text Label 9500 4950 2    60   ~ 0
Rele2.COM
Text Label 7600 4250 2    60   ~ 0
Rele2.NC
Text Label 8250 5700 0    60   ~ 0
Rele2.NC
Text Label 8500 5550 0    60   ~ 0
Rele2.NA
Text Label 8100 4250 0    60   ~ 0
Rele2.NA
Text Label 8350 4400 0    60   ~ 0
Rele2.COM
Text Label 7750 5650 2    60   ~ 0
Rele2.COM
$Comp
L PTS120616V035 PF5
U 1 1 58FFA262
P 4050 4550
F 0 "PF5" V 3950 4750 50  0000 C CNN
F 1 "PTS120616V035" V 4150 4850 50  0000 C CNN
F 2 "ResistorChipSMD:RchipSMD_1206_HandSold" H 4500 4450 50  0001 C CNN
F 3 "" H 4225 4650 30  0000 C CNN
	1    4050 4550
	0    1    1    0   
$EndComp
$Comp
L BORx06-PITCH5mm CN14
U 1 1 58FFAC66
P 2350 4850
F 0 "CN14" H 2350 5500 50  0000 C CNN
F 1 "OpenColectOutCh1-5" H 2350 5600 50  0000 C CNN
F 2 "Connectors_Phoenix:PhoenixContact_MCV-G_06x3.81mm_Vertical" H 2350 4050 50  0001 C CNN
F 3 "" H 2400 4950 60  0001 C CNN
F 4 "OpenColector" H 2350 5650 60  0001 C CNN "Name"
F 5 "VCC_5V" H 2000 5350 60  0000 C CNN "B.1"
F 6 "OC.1" H 2050 5150 60  0000 C CNN "B.2"
F 7 "OC.2" H 2050 4950 60  0000 C CNN "B.3"
F 8 "OC.3" H 2050 4750 60  0000 C CNN "B.4"
F 9 "OC.4" H 2050 4550 60  0000 C CNN "B.5"
F 10 "OC.5" H 2050 4350 60  0000 C CNN "B.6"
	1    2350 4850
	1    0    0    -1  
$EndComp
$Comp
L +5V #PWR0134
U 1 1 58FFAD3E
P 2900 3600
F 0 "#PWR0134" H 2900 3450 50  0001 C CNN
F 1 "+5V" H 2900 3740 50  0000 C CNN
F 2 "" H 2900 3600 50  0000 C CNN
F 3 "" H 2900 3600 50  0000 C CNN
	1    2900 3600
	1    0    0    -1  
$EndComp
$Comp
L PTS120615V050 PF1
U 1 1 58FFADCC
P 2900 3950
F 0 "PF1" H 3075 4050 50  0000 C CNN
F 1 "PTS120615V050" H 3300 3950 50  0000 C CNN
F 2 "ResistorChipSMD:RchipSMD_1206_HandSold" H 3350 3850 50  0001 C CNN
F 3 "" H 3075 4050 30  0000 C CNN
	1    2900 3950
	1    0    0    -1  
$EndComp
$Comp
L PTS120616V035 PF3
U 1 1 58FFB0B0
P 3550 4750
F 0 "PF3" V 3450 4950 50  0000 C CNN
F 1 "PTS120616V035" V 3650 5000 50  0000 C CNN
F 2 "ResistorChipSMD:RchipSMD_1206_HandSold" H 4000 4650 50  0001 C CNN
F 3 "" H 3725 4850 30  0000 C CNN
	1    3550 4750
	0    1    1    0   
$EndComp
$Comp
L PTS120616V035 PF2
U 1 1 58FFB0F3
P 3050 4950
F 0 "PF2" V 2950 5150 50  0000 C CNN
F 1 "PTS120616V035" V 3150 4950 50  0000 C CNN
F 2 "ResistorChipSMD:RchipSMD_1206_HandSold" H 3500 4850 50  0001 C CNN
F 3 "" H 3225 5050 30  0000 C CNN
	1    3050 4950
	0    1    1    0   
$EndComp
$Comp
L PTS120616V035 PF4
U 1 1 58FFB18B
P 3550 5150
F 0 "PF4" V 3450 5350 50  0000 C CNN
F 1 "PTS120616V035" V 3650 5150 50  0000 C CNN
F 2 "ResistorChipSMD:RchipSMD_1206_HandSold" H 4000 5050 50  0001 C CNN
F 3 "" H 3725 5250 30  0000 C CNN
	1    3550 5150
	0    1    1    0   
$EndComp
$Comp
L PTS120616V035 PF6
U 1 1 58FFB286
P 4050 5350
F 0 "PF6" V 3950 5550 50  0000 C CNN
F 1 "PTS120616V035" V 4150 5350 50  0000 C CNN
F 2 "ResistorChipSMD:RchipSMD_1206_HandSold" H 4500 5250 50  0001 C CNN
F 3 "" H 4225 5450 30  0000 C CNN
	1    4050 5350
	0    1    1    0   
$EndComp
Text Label 4250 2150 0    60   ~ 0
ActOpenC.1
Text Label 4250 2250 0    60   ~ 0
ActOpenC.2
Text Label 4250 2350 0    60   ~ 0
ActOpenC.3
Text Label 4250 2450 0    60   ~ 0
ActOpenC.4
Text Label 4250 2550 0    60   ~ 0
ActOpenC.5
Text Label 4700 5350 0    60   ~ 0
ActOpenC.1
Text Label 4700 5150 0    60   ~ 0
ActOpenC.2
Text Label 4700 4950 0    60   ~ 0
ActOpenC.3
Text Label 4700 4750 0    60   ~ 0
ActOpenC.4
Text Label 4700 4550 0    60   ~ 0
ActOpenC.5
$Comp
L HLS-4078-DC5V RL2
U 1 1 59029A7A
P 7850 4950
F 0 "RL2" H 6700 4850 50  0000 L CNN
F 1 "HLS-4078-DC5V" H 6550 5050 50  0000 L CNN
F 2 "Relays_THT:DIP-HLS4078-LongPad" H 7850 4350 50  0001 C CNN
F 3 "" H 7650 4950 50  0000 C CNN
	1    7850 4950
	1    0    0    -1  
$EndComp
$Comp
L ULN2003ADR U12
U 1 1 59075139
P 3350 2350
F 0 "U12" H 3500 2950 50  0000 C CNN
F 1 "ULN2003ADR" H 2850 2950 50  0000 C CNN
F 2 "SOIC:SOIC16_3.9x9.9mm_HandSold" H 4400 1950 50  0001 C CNN
F 3 "" H 3350 2350 50  0000 C CNN
	1    3350 2350
	1    0    0    -1  
$EndComp
Text Notes 2850 6500 0    60   ~ 0
Limitamos las Salidas\nOpenColector de 5V hasta 350mA\n(Maxima Corriente Individual por \nRama, La suma De todas las rama No\nDebe Superar los 500mA)
$Comp
L 100nF_25V_X7R C39
U 1 1 59071155
P 2300 1500
F 0 "C39" H 2600 1550 50  0000 C CNN
F 1 "100nF_25V_X7R" H 2750 1450 50  0000 C CNN
F 2 "CapacitorCeramicSMD:CcSMD_0805_HandSold" H 2700 1350 50  0001 C CNN
F 3 "" H 2300 1500 60  0000 C CNN
	1    2300 1500
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR0135
U 1 1 590714A7
P 2300 1600
F 0 "#PWR0135" H 2300 1350 50  0001 C CNN
F 1 "GND" H 2300 1450 50  0000 C CNN
F 2 "" H 2300 1600 50  0000 C CNN
F 3 "" H 2300 1600 50  0000 C CNN
	1    2300 1600
	1    0    0    -1  
$EndComp
Wire Wire Line
	4000 1950 4250 1950
Wire Wire Line
	4250 2050 4000 2050
Wire Wire Line
	7300 5500 7450 5500
Wire Wire Line
	7450 5500 7450 5400
Wire Wire Line
	8250 5700 8100 5700
Wire Wire Line
	8100 5700 8100 5400
Wire Wire Line
	8500 5550 8400 5550
Wire Wire Line
	8400 5550 8400 5400
Wire Wire Line
	7850 5400 7850 5650
Wire Wire Line
	7850 5650 7750 5650
Wire Wire Line
	8250 4500 8250 4400
Wire Wire Line
	8250 4400 8350 4400
Wire Wire Line
	8100 4250 8000 4250
Wire Wire Line
	8000 4250 8000 4500
Wire Wire Line
	7600 4250 7700 4250
Wire Wire Line
	7700 4250 7700 4500
Wire Wire Line
	9700 4750 9500 4750
Wire Wire Line
	9500 4950 9700 4950
Wire Wire Line
	9700 5150 9500 5150
Wire Wire Line
	2650 4350 2900 4350
Wire Wire Line
	2900 4350 2900 4150
Wire Wire Line
	2900 3600 2900 3750
Wire Wire Line
	3850 4550 2650 4550
Wire Wire Line
	2650 4750 3350 4750
Wire Wire Line
	2850 4950 2650 4950
Wire Wire Line
	2650 5150 3350 5150
Wire Wire Line
	3850 5350 2650 5350
Wire Wire Line
	4250 2150 4000 2150
Wire Wire Line
	4000 2250 4250 2250
Wire Wire Line
	4250 2350 4000 2350
Wire Wire Line
	4000 2450 4250 2450
Wire Wire Line
	4250 2550 4000 2550
Wire Wire Line
	2800 1950 2650 1950
Wire Wire Line
	2800 2050 2650 2050
Wire Wire Line
	2650 2150 2800 2150
Wire Wire Line
	2800 2250 2650 2250
Wire Wire Line
	2650 2350 2800 2350
Wire Wire Line
	2800 2450 2650 2450
Wire Wire Line
	2650 2550 2800 2550
Wire Wire Line
	4700 4550 4250 4550
Wire Wire Line
	3750 4750 4700 4750
Wire Wire Line
	4700 4950 3250 4950
Wire Wire Line
	3750 5150 4700 5150
Wire Wire Line
	4700 5350 4250 5350
Wire Wire Line
	3350 1250 3350 1600
Wire Wire Line
	2300 1400 2300 1300
Connection ~ 3350 1300
Wire Wire Line
	2300 1300 3350 1300
$Comp
L +5V #PWR0136
U 1 1 59C2D33A
P 7450 1850
F 0 "#PWR0136" H 7450 1700 50  0001 C CNN
F 1 "+5V" H 7450 1990 50  0000 C CNN
F 2 "" H 7450 1850 50  0000 C CNN
F 3 "" H 7450 1850 50  0000 C CNN
	1    7450 1850
	1    0    0    -1  
$EndComp
Text Label 7300 2850 2    60   ~ 0
ActRele1
$Comp
L BORx03-PITCH5mm CN15
U 1 1 59C2D342
P 10000 2300
F 0 "CN15" H 10000 2650 50  0000 C CNN
F 1 "Rele1" H 10000 2750 50  0000 C CNN
F 2 "Connectors_Phoenix:PhoenixContact_MCV-G_03x3.81mm_Vertical" H 10000 1800 50  0001 C CNN
F 3 "" H 10050 2100 60  0001 C CNN
F 4 "Rele2" H 10000 2800 60  0001 C CNN "Name"
	1    10000 2300
	-1   0    0    -1  
$EndComp
Text Label 9500 2100 2    60   ~ 0
Rele1.NA
Text Label 9500 2500 2    60   ~ 0
Rele1.NC
Text Label 9500 2300 2    60   ~ 0
Rele1.COM
Text Label 7600 1600 2    60   ~ 0
Rele1.NC
Text Label 8250 3050 0    60   ~ 0
Rele1.NC
Text Label 8500 2900 0    60   ~ 0
Rele1.NA
Text Label 8100 1600 0    60   ~ 0
Rele1.NA
Text Label 8350 1750 0    60   ~ 0
Rele1.COM
Text Label 7750 3000 2    60   ~ 0
Rele1.COM
$Comp
L HLS-4078-DC5V RL1
U 1 1 59C2D352
P 7850 2300
F 0 "RL1" H 6700 2200 50  0000 L CNN
F 1 "HLS-4078-DC5V" H 6550 2400 50  0000 L CNN
F 2 "Relays_THT:DIP-HLS4078-LongPad" H 7850 1700 50  0001 C CNN
F 3 "" H 7650 2300 50  0000 C CNN
	1    7850 2300
	1    0    0    -1  
$EndComp
Wire Wire Line
	7300 2850 7450 2850
Wire Wire Line
	7450 2850 7450 2750
Wire Wire Line
	8250 3050 8100 3050
Wire Wire Line
	8100 3050 8100 2750
Wire Wire Line
	8500 2900 8400 2900
Wire Wire Line
	8400 2900 8400 2750
Wire Wire Line
	7850 2750 7850 3000
Wire Wire Line
	7850 3000 7750 3000
Wire Wire Line
	8250 1850 8250 1750
Wire Wire Line
	8250 1750 8350 1750
Wire Wire Line
	8100 1600 8000 1600
Wire Wire Line
	8000 1600 8000 1850
Wire Wire Line
	7600 1600 7700 1600
Wire Wire Line
	7700 1600 7700 1850
Wire Wire Line
	9700 2100 9500 2100
Wire Wire Line
	9500 2300 9700 2300
Wire Wire Line
	9700 2500 9500 2500
Wire Notes Line
	2650 3750 2650 5550
Wire Notes Line
	2650 5550 4550 5550
Wire Notes Line
	4550 5550 4550 3750
Wire Notes Line
	4550 3750 2650 3750
Wire Notes Line
	3550 6000 3550 5550
Wire Notes Line
	2750 6000 4650 6000
Wire Notes Line
	4650 6000 4650 6100
Wire Notes Line
	2750 6000 2750 6100
Wire Notes Line
	3500 5950 3550 6000
Wire Notes Line
	3550 6000 3600 5950
$EndSCHEMATC
