EESchema Schematic File Version 2
LIBS:74LVT
LIBS:74xgxx
LIBS:74xx
LIBS:ac-dc
LIBS:actel
LIBS:adc-dac
LIBS:ADC-DeltaSigma
LIBS:ADC-SAR
LIBS:Altera
LIBS:analog_devices
LIBS:AnalogMuxDemux
LIBS:analog_switches
LIBS:AopAudio
LIBS:AopComparador
LIBS:AopPrecision
LIBS:AopRailToRailPG
LIBS:atmel
LIBS:audio
LIBS:battery_management
LIBS:bbd
LIBS:BJT-GeneralPurpose 
LIBS:BOR-SIL
LIBS:BridgeUSB
LIBS:brooktre
LIBS:CapAluminioSMD
LIBS:CapCeramicoHVC
LIBS:CapCeramicos
LIBS:CapCer_THT
LIBS:CapElectroliticos
LIBS:CapPoly_THT
LIBS:CapTantalio
LIBS:Ccer_SMD
LIBS:cmos4000
LIBS:cmos_ieee
LIBS:CommonModeChoke 
LIBS:CompuertasLogicas
LIBS:CON-DIL_F
LIBS:CON-DIL_M
LIBS:Conectores
LIBS:ConectoresDB
LIBS:conn
LIBS:CON-SIL_F
LIBS:CON-SIL_M
LIBS:contrib
LIBS:CON-USB
LIBS:Cristal
LIBS:cypress
LIBS:dc-dc
LIBS:device
LIBS:digital-audio
LIBS:diode
LIBS:DiodoPequeñaSeñal
LIBS:DiodoRectificador
LIBS:DiodoSchottkyRect
LIBS:DiodosSupresorTransitorios
LIBS:DiodosZener
LIBS:display
LIBS:dsp
LIBS:EEpromSerie
LIBS:EIA232
LIBS:EIA485
LIBS:elec-unifil
LIBS:ESD_Protection
LIBS: EsdProtectionDiode
LIBS:FerriteBead
LIBS:FotoDiodos
LIBS:ftdi
LIBS:Fusibles
LIBS:gennum
LIBS:graphic
LIBS:hc11
LIBS:Inductor
LIBS:intel
LIBS:interface
LIBS:ir
LIBS:IRF-MOSFET
LIBS:jumpers
LIBS:Lattice
LIBS:LDR_THT
LIBS:LEDs
LIBS:linear
LIBS:LoadGeneric
LIBS:Logic-MOSFET
LIBS:logo
LIBS:LPC17xx
LIBS:LPC43xx
LIBS:MagneticsProtection
LIBS:maxim
LIBS:memory
LIBS:microchip
LIBS:microchip_dspic33dsc
LIBS:microchip_pic10mcu
LIBS:microchip_pic12mcu
LIBS:microchip_pic16mcu
LIBS:microchip_pic18mcu
LIBS:microchip_pic32mcu
LIBS:microcontrollers
LIBS:motor_drivers
LIBS:motorola
LIBS:msp430
LIBS:nordicsemi
LIBS:NTR-MOSFET
LIBS:nxp_armmcu
LIBS:onsemi
LIBS:opto
LIBS:OptoTransistor
LIBS:OptoTriac
LIBS:Oscillators
LIBS:philips
LIBS:PicoFusePTC
LIBS:Pilas
LIBS:Potenciometro
LIBS:power
LIBS:PowerDistributionSwitch
LIBS:powerint
LIBS:Power_Management
LIBS:pspice
LIBS:Puentes
LIBS:R
LIBS:references
LIBS:RegLinealPositivos
LIBS:regul
LIBS:ReguladorLDO
LIBS:ReguladorSwitcher
LIBS:relays
LIBS:rfcom
LIBS:RMII_Ethernet
LIBS:R_SMD
LIBS:R_ThruHole
LIBS:R_THT
LIBS:SensorCurrent
LIBS:sensors
LIBS:SensorTemperatura
LIBS:ShifterLevel
LIBS:silabs
LIBS:siliconi
LIBS:SourceCurrent
LIBS:StickLPC
LIBS:stm8
LIBS:stm32
LIBS:supertex
LIBS:switches
LIBS:TactSwitch
LIBS:TestPoint
LIBS:texas
LIBS:TrafoReductor
LIBS:TransceiverCAN
LIBS:transf
LIBS:TransistorArrays
LIBS:transistors
LIBS:Triac
LIBS:ttl_ieee
LIBS:valves
LIBS:Varistor
LIBS:video
LIBS:VoltageReference
LIBS:Worldsemi
LIBS:Xicor
LIBS:xilinx
LIBS:xtal
LIBS:Zilog
LIBS:MemoryZock
LIBS:BoardMicro-cache
EELAYER 25 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 9 21
Title "BoardWithStick1300revD"
Date "12-05-2017"
Rev "v1.10b"
Comp "J.E.L"
Comment1 "\\n Luccioni, Jesus Emanuel"
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L 1nF_25V_X7R C47
U 1 1 5905B0B7
P 4600 4100
F 0 "C47" H 4750 4150 50  0000 C CNN
F 1 "1nF_25V_X7R" H 4600 4500 50  0000 C CNN
F 2 "CapacitorCeramicSMD:CcSMD_0805_HandSold" H 4850 3950 50  0001 C CNN
F 3 "" H 4600 4100 60  0000 C CNN
	1    4600 4100
	-1   0    0    -1  
$EndComp
$Comp
L SMBJ5.0A D22
U 1 1 5905B0BE
P 5100 4150
F 0 "D22" H 5250 4150 50  0000 C CNN
F 1 "SMBJ5.0A" V 4950 4150 50  0000 C CNN
F 2 "DiodosSMD:SMB-Handsold" V 5100 4100 60  0001 C CNN
F 3 "" V 5100 4100 60  0000 C CNN
	1    5100 4150
	1    0    0    -1  
$EndComp
$Comp
L 470pF_10V_X7R C48
U 1 1 5905B0CC
P 6000 4000
F 0 "C48" H 6300 4000 50  0000 C CNN
F 1 "470pF_10V_X7R" H 5550 4000 50  0000 C CNN
F 2 "CapacitorCeramicSMD:CcSMD_0805_HandSold" H 6250 3850 50  0001 C CNN
F 3 "" H 6000 4000 60  0000 C CNN
	1    6000 4000
	-1   0    0    -1  
$EndComp
$Comp
L 4.7R_125mW_5% R46
U 1 1 5905B0D3
P 6000 4300
F 0 "R46" H 5850 4300 50  0000 C CNN
F 1 "4.7R_125mW_5%" H 6400 4300 50  0000 C CNN
F 2 "ResistorChipSMD:RchipSMD_0805_HandSold" V 5930 4300 30  0001 C CNN
F 3 "" H 6000 4300 30  0000 C CNN
	1    6000 4300
	-1   0    0    -1  
$EndComp
$Comp
L GNDA #PWR088
U 1 1 5905B0DA
P 6000 4500
F 0 "#PWR088" H 6000 4250 50  0001 C CNN
F 1 "GNDA" H 6000 4350 50  0000 C CNN
F 2 "" H 6000 4500 50  0000 C CNN
F 3 "" H 6000 4500 50  0000 C CNN
	1    6000 4500
	1    0    0    -1  
$EndComp
$Comp
L GNDA #PWR089
U 1 1 5905B0E0
P 5100 4400
F 0 "#PWR089" H 5100 4150 50  0001 C CNN
F 1 "GNDA" H 5100 4250 50  0000 C CNN
F 2 "" H 5100 4400 50  0000 C CNN
F 3 "" H 5100 4400 50  0000 C CNN
	1    5100 4400
	1    0    0    -1  
$EndComp
$Comp
L GNDA #PWR090
U 1 1 5905B0E6
P 4600 4350
F 0 "#PWR090" H 4600 4100 50  0001 C CNN
F 1 "GNDA" H 4600 4200 50  0000 C CNN
F 2 "" H 4600 4350 50  0000 C CNN
F 3 "" H 4600 4350 50  0000 C CNN
	1    4600 4350
	1    0    0    -1  
$EndComp
Text HLabel 5850 3350 0    60   Output ~ 0
LightSense
Text HLabel 4150 3850 0    60   Input ~ 0
LDRin
$Comp
L 392R_125mW_1% R49
U 1 1 5905F333
P 7500 3800
F 0 "R49" H 7650 3900 50  0000 C CNN
F 1 "392R_125mW_1%" H 7950 3800 50  0000 C CNN
F 2 "ResistorChipSMD:RchipSMD_0805_HandSold" H 7750 3700 50  0001 C CNN
F 3 "" H 7650 3900 30  0000 C CNN
	1    7500 3800
	1    0    0    -1  
$EndComp
$Comp
L BC807-40 Q4
U 1 1 590603F2
P 7200 3550
AR Path="/590603F2" Ref="Q4"  Part="1" 
AR Path="/579D78DC/5905AB09/590603F2" Ref="Q4"  Part="1" 
F 0 "Q4" H 7400 3625 50  0000 L CNN
F 1 "BC807-40" H 7400 3550 50  0000 L CNN
F 2 "SOT_SMD:SOT23_Handsold" H 7400 3475 50  0001 L CIN
F 3 "" H 7200 3550 50  0000 L CNN
	1    7200 3550
	-1   0    0    1   
$EndComp
$Comp
L 2.2K_125mW_5% R47
U 1 1 59060628
P 7100 3000
F 0 "R47" H 7250 2950 50  0000 C CNN
F 1 "2.2K_125mW_5%" H 7450 3100 50  0000 C CNN
F 2 "ResistorChipSMD:RchipSMD_0805_HandSold" H 7550 2850 50  0001 C CNN
F 3 "" H 7100 3000 30  0000 C CNN
	1    7100 3000
	-1   0    0    -1  
$EndComp
$Comp
L +5VA #PWR091
U 1 1 59060718
P 7100 2700
F 0 "#PWR091" H 7100 2550 50  0001 C CNN
F 1 "+5VA" H 7100 2840 50  0000 C CNN
F 2 "" H 7100 2700 50  0000 C CNN
F 3 "" H 7100 2700 50  0000 C CNN
	1    7100 2700
	-1   0    0    -1  
$EndComp
$Comp
L GNDA #PWR092
U 1 1 59060903
P 7100 4450
F 0 "#PWR092" H 7100 4200 50  0001 C CNN
F 1 "GNDA" H 7100 4300 50  0000 C CNN
F 2 "" H 7100 4450 50  0000 C CNN
F 3 "" H 7100 4450 50  0000 C CNN
	1    7100 4450
	-1   0    0    -1  
$EndComp
Text HLabel 4150 4250 0    60   Input ~ 0
LDRou
$Comp
L GNDA #PWR093
U 1 1 59062058
P 4300 4350
F 0 "#PWR093" H 4300 4100 50  0001 C CNN
F 1 "GNDA" H 4300 4200 50  0000 C CNN
F 2 "" H 4300 4350 50  0000 C CNN
F 3 "" H 4300 4350 50  0000 C CNN
	1    4300 4350
	1    0    0    -1  
$EndComp
Wire Wire Line
	4150 3850 7100 3850
Wire Wire Line
	4600 4000 4600 3850
Connection ~ 4600 3850
Wire Wire Line
	5100 3900 5100 3850
Connection ~ 5100 3850
Wire Wire Line
	4600 4200 4600 4350
Wire Wire Line
	6000 3350 6000 3900
Connection ~ 6000 3850
Wire Wire Line
	7100 3200 7100 3300
Wire Wire Line
	7100 3800 7100 3950
Wire Wire Line
	7500 3500 7500 3600
Wire Wire Line
	7400 3550 7500 3550
Connection ~ 7500 3550
Wire Wire Line
	7100 2700 7100 2800
Wire Wire Line
	7500 3100 7500 2750
Wire Wire Line
	7500 2750 7100 2750
Connection ~ 7100 2750
Wire Wire Line
	7100 4350 7100 4450
Wire Wire Line
	7500 4000 7500 4400
Wire Wire Line
	7500 4400 7100 4400
Connection ~ 7100 4400
Connection ~ 7100 3850
Wire Wire Line
	5850 3350 6000 3350
Wire Wire Line
	4150 4250 4300 4250
Wire Wire Line
	4300 4250 4300 4350
$Comp
L 470K_125mW_5% R48
U 1 1 59067D04
P 7100 4150
F 0 "R48" H 7500 4150 50  0000 C CNN
F 1 "470K_125mW_5%" H 7500 4250 50  0000 C CNN
F 2 "ResistorChipSMD:RchipSMD_0805_HandSold" H 7550 4000 50  0001 C CNN
F 3 "" H 7100 4150 30  0000 C CNN
	1    7100 4150
	-1   0    0    1   
$EndComp
$Comp
L BZT52C2V7 D23
U 1 1 59C2FDD2
P 7500 3300
F 0 "D23" H 7700 3350 50  0000 C CNN
F 1 "BZT52C2V7" H 7850 3250 50  0000 C CNN
F 2 "DiodosSMD:SOD123-HandSold" H 7750 3150 50  0001 C CNN
F 3 "" V 7500 3300 60  0000 C CNN
	1    7500 3300
	1    0    0    -1  
$EndComp
$EndSCHEMATC
