EESchema Schematic File Version 2
LIBS:74LVT
LIBS:74xgxx
LIBS:74xx
LIBS:ac-dc
LIBS:actel
LIBS:adc-dac
LIBS:ADC-DeltaSigma
LIBS:ADC-SAR
LIBS:Altera
LIBS:analog_devices
LIBS:AnalogMuxDemux
LIBS:analog_switches
LIBS:AopAudio
LIBS:AopComparador
LIBS:AopPrecision
LIBS:AopRailToRailPG
LIBS:atmel
LIBS:audio
LIBS:battery_management
LIBS:bbd
LIBS:BJT-GeneralPurpose 
LIBS:BOR-SIL
LIBS:BridgeUSB
LIBS:brooktre
LIBS:CapAluminioSMD
LIBS:CapCeramicoHVC
LIBS:CapCeramicos
LIBS:CapCer_THT
LIBS:CapElectroliticos
LIBS:CapPoly_THT
LIBS:CapTantalio
LIBS:Ccer_SMD
LIBS:cmos4000
LIBS:cmos_ieee
LIBS:CommonModeChoke 
LIBS:CompuertasLogicas
LIBS:CON-DIL_F
LIBS:CON-DIL_M
LIBS:Conectores
LIBS:ConectoresDB
LIBS:conn
LIBS:CON-SIL_F
LIBS:CON-SIL_M
LIBS:contrib
LIBS:CON-USB
LIBS:Cristal
LIBS:cypress
LIBS:dc-dc
LIBS:device
LIBS:digital-audio
LIBS:diode
LIBS:DiodoPequeñaSeñal
LIBS:DiodoRectificador
LIBS:DiodoSchottkyRect
LIBS:DiodosSupresorTransitorios
LIBS:DiodosZener
LIBS:display
LIBS:dsp
LIBS:EEpromSerie
LIBS:EIA232
LIBS:EIA485
LIBS:elec-unifil
LIBS:ESD_Protection
LIBS: EsdProtectionDiode
LIBS:FerriteBead
LIBS:FotoDiodos
LIBS:ftdi
LIBS:Fusibles
LIBS:gennum
LIBS:graphic
LIBS:hc11
LIBS:Inductor
LIBS:intel
LIBS:interface
LIBS:ir
LIBS:IRF-MOSFET
LIBS:jumpers
LIBS:Lattice
LIBS:LDR_THT
LIBS:LEDs
LIBS:linear
LIBS:LoadGeneric
LIBS:Logic-MOSFET
LIBS:logo
LIBS:LPC17xx
LIBS:LPC43xx
LIBS:MagneticsProtection
LIBS:maxim
LIBS:memory
LIBS:microchip
LIBS:microchip_dspic33dsc
LIBS:microchip_pic10mcu
LIBS:microchip_pic12mcu
LIBS:microchip_pic16mcu
LIBS:microchip_pic18mcu
LIBS:microchip_pic32mcu
LIBS:microcontrollers
LIBS:motor_drivers
LIBS:motorola
LIBS:msp430
LIBS:nordicsemi
LIBS:NTR-MOSFET
LIBS:nxp_armmcu
LIBS:onsemi
LIBS:opto
LIBS:OptoTransistor
LIBS:OptoTriac
LIBS:Oscillators
LIBS:philips
LIBS:PicoFusePTC
LIBS:Pilas
LIBS:Potenciometro
LIBS:power
LIBS:PowerDistributionSwitch
LIBS:powerint
LIBS:Power_Management
LIBS:pspice
LIBS:Puentes
LIBS:R
LIBS:references
LIBS:RegLinealPositivos
LIBS:regul
LIBS:ReguladorLDO
LIBS:ReguladorSwitcher
LIBS:relays
LIBS:rfcom
LIBS:RMII_Ethernet
LIBS:R_SMD
LIBS:R_ThruHole
LIBS:R_THT
LIBS:SensorCurrent
LIBS:sensors
LIBS:SensorTemperatura
LIBS:ShifterLevel
LIBS:silabs
LIBS:siliconi
LIBS:SourceCurrent
LIBS:StickLPC
LIBS:stm8
LIBS:stm32
LIBS:supertex
LIBS:switches
LIBS:TactSwitch
LIBS:TestPoint
LIBS:texas
LIBS:TrafoReductor
LIBS:TransceiverCAN
LIBS:transf
LIBS:TransistorArrays
LIBS:transistors
LIBS:Triac
LIBS:ttl_ieee
LIBS:valves
LIBS:Varistor
LIBS:video
LIBS:VoltageReference
LIBS:Worldsemi
LIBS:Xicor
LIBS:xilinx
LIBS:xtal
LIBS:Zilog
LIBS:MemoryZock
LIBS:BoardMicro-cache
EELAYER 25 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 19 21
Title "BoardWithStick1300revD"
Date "12-05-2017"
Rev "v1.10b"
Comp "J.E.L"
Comment1 "\\n Luccioni, Jesus Emanuel"
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
Text HLabel 4200 2300 0    60   Output ~ 0
RS485_1.RDX
Text HLabel 4200 2500 0    60   Input ~ 0
RS485_1.TDX
Text HLabel 1500 5100 0    60   BiDi ~ 0
CAN_1.RDX
Text HLabel 1500 4700 0    60   BiDi ~ 0
CAN_1.TDX
Text HLabel 1500 4900 0    60   Input ~ 0
CAN_1.STR
$Comp
L BORx03-PITCH5mm CN19
U 1 1 5904E9E1
P 10100 2500
F 0 "CN19" H 10100 2850 50  0000 C CNN
F 1 "RS485-5V" H 10050 2100 50  0000 C CNN
F 2 "Connectors_Phoenix:PhoenixContact_MCV-G_03x3.81mm_Vertical" H 10100 2000 50  0001 C CNN
F 3 "" H 10150 2300 60  0001 C CNN
F 4 "RS485_1" H 10100 2000 60  0001 C CNN "Name"
F 5 "D+" H 9850 2700 50  0000 C CNN "P.1"
F 6 "D-" H 9850 2500 50  0000 C CNN "P.2"
F 7 "GND" H 9850 2300 50  0000 C CNN "P.3"
	1    10100 2500
	-1   0    0    -1  
$EndComp
$Comp
L GND #PWR0149
U 1 1 5904F39C
P 9650 3350
F 0 "#PWR0149" H 9650 3100 50  0001 C CNN
F 1 "GND" H 9650 3200 50  0000 C CNN
F 2 "" H 9650 3350 50  0000 C CNN
F 3 "" H 9650 3350 50  0000 C CNN
	1    9650 3350
	-1   0    0    -1  
$EndComp
$Comp
L 100K_125mW_5% R100
U 1 1 59050527
P 6900 1950
F 0 "R100" H 7100 2000 50  0000 C CNN
F 1 "100K_125mW_5%" H 7300 1900 50  0000 C CNN
F 2 "ResistorChipSMD:RchipSMD_0805_HandSold" H 6900 1700 50  0001 C CNN
F 3 "" H 6900 1950 30  0000 C CNN
	1    6900 1950
	1    0    0    -1  
$EndComp
$Comp
L 100K_125mW_5% R101
U 1 1 59050574
P 6900 2850
F 0 "R101" H 7100 2900 50  0000 C CNN
F 1 "100K_125mW_5%" H 7300 2800 50  0000 C CNN
F 2 "ResistorChipSMD:RchipSMD_0805_HandSold" H 6900 2600 50  0001 C CNN
F 3 "" H 6900 2850 30  0000 C CNN
	1    6900 2850
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR0150
U 1 1 59051574
P 6900 3200
F 0 "#PWR0150" H 6900 2950 50  0001 C CNN
F 1 "GND" H 6900 3050 50  0000 C CNN
F 2 "" H 6900 3200 50  0000 C CNN
F 3 "" H 6900 3200 50  0000 C CNN
	1    6900 3200
	1    0    0    -1  
$EndComp
Text HLabel 4200 2800 0    60   Input ~ 0
RS485_1.Dir
$Comp
L MF-USMF020 PF9
U 1 1 5906A90E
P 8750 2000
F 0 "PF9" V 9000 2000 50  0000 C CNN
F 1 "MF-USMF020" V 8900 2000 50  0000 C CNN
F 2 "ResistorChipSMD:RchipSMD_1210_HandSold" H 9200 1900 50  0001 C CNN
F 3 "" H 8925 2100 30  0000 C CNN
	1    8750 2000
	0    -1   -1   0   
$EndComp
$Comp
L MF-USMF020 PF10
U 1 1 5906ADC1
P 8750 2800
F 0 "PF10" V 8600 2750 50  0000 C CNN
F 1 "MF-USMF020" V 8900 2800 50  0000 C CNN
F 2 "ResistorChipSMD:RchipSMD_1210_HandSold" H 9200 2700 50  0001 C CNN
F 3 "" H 8925 2900 30  0000 C CNN
	1    8750 2800
	0    -1   1    0   
$EndComp
$Comp
L PSD12C D43
U 1 1 5906C80D
P 8300 1600
F 0 "D43" H 8100 1600 50  0000 C CNN
F 1 "PSD12C" H 8050 1900 50  0000 C CNN
F 2 "DiodosSMD:SOD323_HandSold" H 8300 1300 50  0001 C CNN
F 3 "" V 8300 1550 60  0000 C CNN
	1    8300 1600
	-1   0    0    -1  
$EndComp
$Comp
L PSD12C D45
U 1 1 5906C9A2
P 8300 3200
F 0 "D45" H 8500 3200 50  0000 C CNN
F 1 "PSD12C" H 8300 5100 50  0001 C CNN
F 2 "DiodosSMD:SOD323_HandSold" H 8300 2900 50  0001 C CNN
F 3 "" V 8300 3150 60  0000 C CNN
	1    8300 3200
	1    0    0    -1  
$EndComp
$Comp
L PSD12C D44
U 1 1 5906CCDE
P 8300 2400
F 0 "D44" H 8100 2400 50  0000 C CNN
F 1 "PSD12C" H 8300 1200 50  0001 C CNN
F 2 "DiodosSMD:SOD323_HandSold" H 8300 2100 50  0001 C CNN
F 3 "" V 8300 2350 60  0000 C CNN
	1    8300 2400
	-1   0    0    -1  
$EndComp
Text Notes 2650 7150 0    60   ~ 0
 @@ RS485- Profibus Tolerant 5[V]\n   # Pins LPC176x/5x tolerant TTL 5[V]\n ' In case To imput Pulled to up 3v3 '
$Comp
L 4.7K_125mW_5% R99
U 1 1 590146B7
P 4550 3050
F 0 "R99" H 4750 3100 50  0000 C CNN
F 1 "4.7K_125mW_5%" H 4900 3000 50  0000 C CNN
F 2 "ResistorChipSMD:RchipSMD_0805_HandSold" H 4550 2800 50  0001 C CNN
F 3 "" H 4550 3050 30  0000 C CNN
	1    4550 3050
	-1   0    0    -1  
$EndComp
$Comp
L +5V #PWR0151
U 1 1 59015119
P 6250 1350
F 0 "#PWR0151" H 6250 1200 50  0001 C CNN
F 1 "+5V" H 6250 1490 50  0000 C CNN
F 2 "" H 6250 1350 50  0000 C CNN
F 3 "" H 6250 1350 50  0000 C CNN
	1    6250 1350
	1    0    0    -1  
$EndComp
$Comp
L 220R_125mW_5% R104
U 1 1 59016D81
P 8050 2550
F 0 "R104" H 8200 2600 50  0000 C CNN
F 1 "220R_125mW_5%" H 8450 2500 50  0000 C CNN
F 2 "ResistorChipSMD:RchipSMD_0805_HandSold" H 8050 2300 50  0001 C CNN
F 3 "" H 8050 2550 30  0000 C CNN
	1    8050 2550
	-1   0    0    -1  
$EndComp
$Comp
L 390R_125mW_5% R105
U 1 1 59016EDF
P 8050 3350
F 0 "R105" H 8250 3400 50  0000 C CNN
F 1 "390R_125mW_5%" H 8450 3300 50  0000 C CNN
F 2 "ResistorChipSMD:RchipSMD_0805_HandSold" H 8050 3100 50  0001 C CNN
F 3 "" H 8050 3350 30  0000 C CNN
	1    8050 3350
	-1   0    0    -1  
$EndComp
$Comp
L J_1x1 J12
U 1 1 59016F80
P 8050 3000
F 0 "J12" V 8050 2850 50  0000 C CNN
F 1 "J_1x1" V 8100 2850 50  0001 C CNN
F 2 "Jumper:J-1x1-SMD" H 8050 2800 50  0001 C CNN
F 3 "" V 8050 2900 60  0000 C CNN
	1    8050 3000
	0    1    1    0   
$EndComp
$Comp
L J_1x1 J11
U 1 1 590171F3
P 8050 2200
F 0 "J11" V 8050 2050 50  0000 C CNN
F 1 "TerminatorOrMaster" V 8050 2950 50  0000 C CNN
F 2 "Jumper:J-1x1-SMD" H 8050 2000 50  0001 C CNN
F 3 "" V 8050 2100 60  0000 C CNN
	1    8050 2200
	0    1    1    0   
$EndComp
$Comp
L 390R_125mW_5% R103
U 1 1 59017D52
P 8050 1450
F 0 "R103" H 8250 1500 50  0000 C CNN
F 1 "390R_125mW_5%" H 8450 1400 50  0000 C CNN
F 2 "ResistorChipSMD:RchipSMD_0805_HandSold" H 8050 1200 50  0001 C CNN
F 3 "" H 8050 1450 30  0000 C CNN
	1    8050 1450
	-1   0    0    -1  
$EndComp
$Comp
L J_1x1 J10
U 1 1 59017DE1
P 8050 1800
F 0 "J10" V 8050 1650 50  0000 C CNN
F 1 "J_1x1" V 8100 1650 50  0001 C CNN
F 2 "Jumper:J-1x1-SMD" H 8050 1600 50  0001 C CNN
F 3 "" V 8050 1700 60  0000 C CNN
	1    8050 1800
	0    1    1    0   
$EndComp
$Comp
L GND #PWR0152
U 1 1 59018869
P 8050 3650
F 0 "#PWR0152" H 8050 3400 50  0001 C CNN
F 1 "GND" H 8050 3500 50  0000 C CNN
F 2 "" H 8050 3650 50  0000 C CNN
F 3 "" H 8050 3650 50  0000 C CNN
	1    8050 3650
	1    0    0    -1  
$EndComp
$Comp
L SN65HVD1176 U16
U 1 1 590193E1
P 6250 2400
F 0 "U16" H 6400 2900 60  0000 C CNN
F 1 "SN65HVD1176" H 5850 1900 50  0000 C CNN
F 2 "SOIC:SOIC8_3.9x4.9mm_HandSold" H 6250 1700 60  0001 C CNN
F 3 "" H 7100 3050 60  0000 C CNN
	1    6250 2400
	1    0    0    -1  
$EndComp
$Comp
L +5V #PWR0153
U 1 1 59019E69
P 8050 1150
F 0 "#PWR0153" H 8050 1000 50  0001 C CNN
F 1 "+5V" H 8050 1290 50  0000 C CNN
F 2 "" H 8050 1150 50  0000 C CNN
F 3 "" H 8050 1150 50  0000 C CNN
	1    8050 1150
	1    0    0    -1  
$EndComp
$Comp
L 100R_500mW_5% R106
U 1 1 5904F395
P 9650 3050
F 0 "R106" H 9500 3000 50  0000 C CNN
F 1 "100R_500mW_5%" H 9250 3100 50  0000 C CNN
F 2 "ResistorChipSMD:RchipSMD_1210_HandSold" H 9650 2750 50  0001 C CNN
F 3 "" H 9650 3050 30  0000 C CNN
	1    9650 3050
	1    0    0    1   
$EndComp
Text Notes 6100 3750 0    60   ~ 0
3's JUMP For Terminator or Master\nIn the Bus
$Comp
L 100nF_25V_X7R C62
U 1 1 5901E079
P 6000 1650
F 0 "C62" V 6000 1950 50  0000 C CNN
F 1 "100nF_25V_X7R" V 6000 2350 50  0000 C CNN
F 2 "CapacitorCeramicSMD:CcSMD_0805_HandSold" H 6400 1500 50  0001 C CNN
F 3 "" H 6000 1650 60  0000 C CNN
	1    6000 1650
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR0154
U 1 1 5901E73C
P 5750 1800
F 0 "#PWR0154" H 5750 1550 50  0001 C CNN
F 1 "GND" H 5750 1650 50  0000 C CNN
F 2 "" H 5750 1800 50  0000 C CNN
F 3 "" H 5750 1800 50  0000 C CNN
	1    5750 1800
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR0155
U 1 1 5901E7BF
P 6000 1750
F 0 "#PWR0155" H 6000 1500 50  0001 C CNN
F 1 "GND" H 6000 1600 50  0000 C CNN
F 2 "" H 6000 1750 50  0000 C CNN
F 3 "" H 6000 1750 50  0000 C CNN
	1    6000 1750
	1    0    0    -1  
$EndComp
$Comp
L 270R_125mW_5% R86
U 1 1 59018E88
P 1750 5100
F 0 "R86" V 1850 5100 50  0000 C CNN
F 1 "270R_125mW_5%" V 1650 5100 50  0000 C CNN
F 2 "ResistorChipSMD:RchipSMD_0805_HandSold" H 1750 4850 50  0001 C CNN
F 3 "" H 1750 5100 30  0000 C CNN
	1    1750 5100
	0    1    1    0   
$EndComp
$Comp
L 100nF_25V_X7R C57
U 1 1 59019189
P 2250 4250
F 0 "C57" V 2250 4550 50  0000 C CNN
F 1 "100nF_25V_X7R" V 2250 5000 50  0000 C CNN
F 2 "CapacitorCeramicSMD:CcSMD_0805_HandSold" H 2650 4100 50  0001 C CNN
F 3 "" H 2250 4250 60  0000 C CNN
	1    2250 4250
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR0156
U 1 1 590195B4
P 2000 4400
F 0 "#PWR0156" H 2000 4150 50  0001 C CNN
F 1 "GND" H 2000 4250 50  0000 C CNN
F 2 "" H 2000 4400 50  0000 C CNN
F 3 "" H 2000 4400 50  0000 C CNN
	1    2000 4400
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR0157
U 1 1 5901966E
P 2250 4350
F 0 "#PWR0157" H 2250 4100 50  0001 C CNN
F 1 "GND" H 2250 4200 50  0000 C CNN
F 2 "" H 2250 4350 50  0000 C CNN
F 3 "" H 2250 4350 50  0000 C CNN
	1    2250 4350
	1    0    0    -1  
$EndComp
$Comp
L +5V #PWR0158
U 1 1 5901975B
P 2700 3950
F 0 "#PWR0158" H 2700 3800 50  0001 C CNN
F 1 "+5V" H 2700 4090 50  0000 C CNN
F 2 "" H 2700 3950 50  0000 C CNN
F 3 "" H 2700 3950 50  0000 C CNN
	1    2700 3950
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR0159
U 1 1 5901A80A
P 2700 5500
F 0 "#PWR0159" H 2700 5250 50  0001 C CNN
F 1 "GND" H 2700 5350 50  0000 C CNN
F 2 "" H 2700 5500 50  0000 C CNN
F 3 "" H 2700 5500 50  0000 C CNN
	1    2700 5500
	1    0    0    -1  
$EndComp
$Comp
L 4.7K_125mW_5% R87
U 1 1 5901A929
P 2100 5350
F 0 "R87" H 2300 5400 50  0000 C CNN
F 1 "4.7K_125mW_5%" H 2450 5300 50  0000 C CNN
F 2 "ResistorChipSMD:RchipSMD_0805_HandSold" H 2100 5100 50  0001 C CNN
F 3 "" H 2100 5350 30  0000 C CNN
	1    2100 5350
	-1   0    0    -1  
$EndComp
$Comp
L GND #PWR0160
U 1 1 5901AEA2
P 2100 5550
F 0 "#PWR0160" H 2100 5300 50  0001 C CNN
F 1 "GND" H 2100 5400 50  0000 C CNN
F 2 "" H 2100 5550 50  0000 C CNN
F 3 "" H 2100 5550 50  0000 C CNN
	1    2100 5550
	1    0    0    -1  
$EndComp
$Comp
L 1.5K_125mW_5% R90
U 1 1 5901C1F1
P 3500 4600
F 0 "R90" H 3700 4650 50  0000 C CNN
F 1 "1.5K_125mW_5%" H 3850 4550 50  0000 C CNN
F 2 "ResistorChipSMD:RchipSMD_0805_HandSold" H 3500 4350 50  0001 C CNN
F 3 "" H 3500 4600 30  0000 C CNN
	1    3500 4600
	1    0    0    -1  
$EndComp
$Comp
L 1.5K_125mW_5% R91
U 1 1 5901C34D
P 3500 5200
F 0 "R91" H 3700 5250 50  0000 C CNN
F 1 "1.5K_125mW_5%" H 3850 5150 50  0000 C CNN
F 2 "ResistorChipSMD:RchipSMD_0805_HandSold" H 3500 4950 50  0001 C CNN
F 3 "" H 3500 5200 30  0000 C CNN
	1    3500 5200
	1    0    0    -1  
$EndComp
$Comp
L 59R_125mW_2% R94
U 1 1 5901C564
P 4550 4600
F 0 "R94" H 4750 4650 50  0000 C CNN
F 1 "59R_125mW_2%" H 4900 4550 50  0000 C CNN
F 2 "ResistorChipSMD:RchipSMD_0805_HandSold" H 4550 4350 50  0001 C CNN
F 3 "" H 4550 4600 30  0000 C CNN
	1    4550 4600
	1    0    0    1   
$EndComp
$Comp
L 59R_125mW_2% R95
U 1 1 5901C6E5
P 4550 5200
F 0 "R95" H 4750 5250 50  0000 C CNN
F 1 "59R_125mW_2%" H 4900 5150 50  0000 C CNN
F 2 "ResistorChipSMD:RchipSMD_0805_HandSold" H 4550 4950 50  0001 C CNN
F 3 "" H 4550 5200 30  0000 C CNN
	1    4550 5200
	1    0    0    1   
$EndComp
$Comp
L J_1x1 J5
U 1 1 5901C976
P 4300 4850
F 0 "J5" H 4300 4750 50  0000 C CNN
F 1 "TerminatorOrMaster" H 3650 4900 50  0000 C CNN
F 2 "Jumper:J-1x1-SMD" H 4300 4650 50  0001 C CNN
F 3 "" V 4300 4750 60  0000 C CNN
	1    4300 4850
	-1   0    0    1   
$EndComp
$Comp
L J_1x1 J6
U 1 1 5901CB57
P 4300 4950
F 0 "J6" H 4300 4850 50  0000 C CNN
F 1 "J_1x1" H 4700 4950 50  0001 C CNN
F 2 "Jumper:J-1x1-SMD" H 4300 4750 50  0001 C CNN
F 3 "" V 4300 4850 60  0000 C CNN
	1    4300 4950
	1    0    0    -1  
$EndComp
$Comp
L 100nF_25V_X7R C60
U 1 1 5901CBD8
P 4100 5650
F 0 "C60" H 4300 5650 50  0000 C CNN
F 1 "100nF_25V_X7R" H 4500 5550 50  0000 C CNN
F 2 "CapacitorCeramicSMD:CcSMD_0805_HandSold" H 4500 5500 50  0001 C CNN
F 3 "" H 4100 5650 60  0000 C CNN
	1    4100 5650
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR0161
U 1 1 5901E8B2
P 4100 5750
F 0 "#PWR0161" H 4100 5500 50  0001 C CNN
F 1 "GND" H 4100 5600 50  0000 C CNN
F 2 "" H 4100 5750 50  0000 C CNN
F 3 "" H 4100 5750 50  0000 C CNN
	1    4100 5750
	1    0    0    -1  
$EndComp
$Comp
L BORx03-PITCH5mm CN17
U 1 1 5901F239
P 6700 4900
F 0 "CN17" H 6700 5250 50  0000 C CNN
F 1 "CAN-5V" H 6650 4500 50  0000 C CNN
F 2 "Connectors_Phoenix:PhoenixContact_MCV-G_03x3.81mm_Vertical" H 6700 4400 50  0001 C CNN
F 3 "" H 6750 4700 60  0001 C CNN
F 4 "CAN" H 6700 4400 60  0001 C CNN "Name"
F 5 "CANH" H 6400 5100 50  0000 C CNN "P.1"
F 6 "CANL" H 6400 4900 50  0000 C CNN "P.2"
F 7 "GND" H 6400 4700 50  0000 C CNN "P.3"
	1    6700 4900
	-1   0    0    -1  
$EndComp
$Comp
L GND #PWR0162
U 1 1 5901F50C
P 6300 5200
F 0 "#PWR0162" H 6300 4950 50  0001 C CNN
F 1 "GND" H 6300 5050 50  0000 C CNN
F 2 "" H 6300 5200 50  0000 C CNN
F 3 "" H 6300 5200 50  0000 C CNN
	1    6300 5200
	1    0    0    -1  
$EndComp
Text Notes 3800 4150 0    60   ~ 0
2's JUMP For Terminator \nNodes In the Bus
$Comp
L PESD1CAN-U U15
U 1 1 590247D0
P 5850 5400
F 0 "U15" H 5550 5300 50  0000 C CNN
F 1 "PESD1CAN-U" H 5450 5150 50  0000 C CNN
F 2 "SOT_SMD:SOT323_SC-70_Handsoldering" H 5850 5875 50  0001 C CNN
F 3 "" H 5600 5675 50  0001 C CNN
	1    5850 5400
	-1   0    0    -1  
$EndComp
$Comp
L GND #PWR0163
U 1 1 59024B97
P 5850 5700
F 0 "#PWR0163" H 5850 5450 50  0001 C CNN
F 1 "GND" H 5850 5550 50  0000 C CNN
F 2 "" H 5850 5700 50  0000 C CNN
F 3 "" H 5850 5700 50  0000 C CNN
	1    5850 5700
	1    0    0    -1  
$EndComp
Wire Wire Line
	9800 2700 9650 2700
Wire Wire Line
	6750 2300 7700 2300
Wire Wire Line
	6750 2500 7700 2500
Wire Wire Line
	6900 2500 6900 2650
Connection ~ 6900 2500
Wire Wire Line
	6900 2300 6900 2150
Connection ~ 6900 2300
Wire Wire Line
	6900 3050 6900 3200
Wire Wire Line
	6250 3000 6250 3100
Connection ~ 6900 3100
Wire Wire Line
	6250 1350 6250 1800
Wire Wire Line
	6250 1700 6900 1700
Wire Wire Line
	4200 2300 5750 2300
Wire Wire Line
	4200 2500 5750 2500
Wire Wire Line
	4550 2650 5750 2650
Wire Wire Line
	6250 3100 6900 3100
Wire Wire Line
	8050 1950 8050 2050
Wire Wire Line
	8300 1850 8300 2150
Wire Wire Line
	7700 2300 7700 2000
Wire Wire Line
	7700 2000 8500 2000
Connection ~ 8050 2000
Connection ~ 8300 2000
Wire Wire Line
	7700 2500 7700 2800
Wire Wire Line
	8050 2750 8050 2850
Connection ~ 8050 2800
Wire Wire Line
	8300 2650 8300 2950
Connection ~ 8300 2800
Wire Wire Line
	7700 2800 8500 2800
Wire Wire Line
	8050 3550 8050 3650
Wire Wire Line
	8300 3450 8300 3600
Wire Wire Line
	8300 3600 8050 3600
Connection ~ 8050 3600
Wire Wire Line
	9000 2800 9200 2800
Wire Wire Line
	9200 2800 9200 2500
Wire Wire Line
	9000 2000 9200 2000
Wire Wire Line
	9200 2000 9200 2300
Wire Wire Line
	8050 1150 8050 1250
Wire Wire Line
	8050 1200 8300 1200
Wire Wire Line
	8300 1200 8300 1350
Connection ~ 8050 1200
Wire Wire Line
	9650 3250 9650 3350
Wire Wire Line
	9650 2700 9650 2850
Wire Wire Line
	9200 2500 9800 2500
Wire Wire Line
	9200 2300 9800 2300
Wire Notes Line
	8200 1350 8400 1350
Wire Notes Line
	8400 1350 8400 3500
Wire Notes Line
	8400 3500 8200 3500
Wire Notes Line
	8200 3500 8200 1350
Wire Wire Line
	5750 1550 5750 1450
Wire Wire Line
	5750 1450 6250 1450
Connection ~ 6250 1700
Wire Wire Line
	6000 1550 6000 1450
Connection ~ 6000 1450
Connection ~ 6250 1450
Wire Wire Line
	6900 1700 6900 1750
Wire Wire Line
	2000 4050 2700 4050
Wire Wire Line
	2000 4050 2000 4150
Wire Wire Line
	2250 4150 2250 4050
Connection ~ 2250 4050
Wire Wire Line
	2700 3950 2700 4300
Connection ~ 2700 4050
Wire Wire Line
	1500 4900 2200 4900
Wire Wire Line
	2200 4700 1500 4700
Wire Wire Line
	1500 5100 1550 5100
Wire Wire Line
	2200 5100 1950 5100
Wire Wire Line
	2100 5150 2100 4900
Connection ~ 2100 4900
Wire Wire Line
	3500 4800 3500 5000
Connection ~ 3500 4900
Wire Wire Line
	4150 4850 4100 4850
Wire Wire Line
	4100 4850 4100 5550
Wire Wire Line
	4100 4950 4150 4950
Connection ~ 4100 4900
Wire Wire Line
	3200 5100 3200 5450
Wire Wire Line
	3200 5450 5500 5450
Wire Wire Line
	4550 5450 4550 5400
Wire Wire Line
	3500 5400 3500 5450
Connection ~ 3500 5450
Wire Wire Line
	3200 4700 3200 4350
Wire Wire Line
	3200 4350 5500 4350
Wire Wire Line
	4550 4350 4550 4400
Wire Wire Line
	3500 4400 3500 4350
Connection ~ 3500 4350
Wire Wire Line
	4450 4850 4550 4850
Wire Wire Line
	4550 4850 4550 4800
Wire Wire Line
	4450 4950 4550 4950
Wire Wire Line
	4550 4950 4550 5000
Wire Wire Line
	5500 4900 6400 4900
Wire Wire Line
	5500 5450 5500 4900
Connection ~ 4550 5450
Wire Wire Line
	5500 4700 6400 4700
Wire Wire Line
	5500 4350 5500 4700
Connection ~ 4550 4350
Wire Wire Line
	6300 5200 6300 5100
Wire Wire Line
	6300 5100 6400 5100
Wire Wire Line
	5750 5100 5750 4700
Connection ~ 5750 4700
Wire Wire Line
	5950 5100 5950 4900
Connection ~ 5950 4900
Text Notes 2650 7500 0    60   ~ 0
 @@ Especificaciones p/CI ( interfaces CAN ) :\n   # TJA1040 (current specification)\n   # TJF1051T [no /3] , for CAN FD (Upgrade, sin R90,R91 y R7)
$Comp
L 10uF_10V_T491 C56
U 1 1 590AB4B8
P 2000 4250
F 0 "C56" V 2000 4550 50  0000 C CNN
F 1 "10uF_10V_T491" V 2000 5000 50  0000 C CNN
F 2 "CapacitorTantalumSMD:CtanlumSMD_Case-A_Hand" H 2400 4150 50  0001 C CNN
F 3 "" H 2200 4350 60  0000 C CNN
	1    2000 4250
	-1   0    0    -1  
$EndComp
Wire Wire Line
	2000 4400 2000 4350
$Comp
L 10uF_10V_T491 C61
U 1 1 590ACAE3
P 5750 1650
F 0 "C61" V 5750 2000 50  0000 C CNN
F 1 "10uF_10V_T491" V 5750 2400 50  0000 C CNN
F 2 "CapacitorTantalumSMD:CtanlumSMD_Case-A_Hand" H 6150 1550 50  0001 C CNN
F 3 "" H 5950 1750 60  0000 C CNN
	1    5750 1650
	-1   0    0    -1  
$EndComp
Wire Wire Line
	5750 1750 5750 1800
$Comp
L TJA1040 U13
U 1 1 5915CA30
P 2700 4900
F 0 "U13" H 2850 5400 60  0000 C CNN
F 1 "TJA1040" H 2700 5000 50  0000 C CNN
F 2 "SOIC:SOIC8_3.9x4.9mm_HandSold" H 2700 4200 60  0001 C CNN
F 3 "" H 3550 5550 60  0000 C CNN
	1    2700 4900
	1    0    0    -1  
$EndComp
Connection ~ 4100 4950
$Comp
L 4.7K_125mW_5% R15
U 1 1 59BF974A
P 5150 3050
F 0 "R15" H 5350 3100 50  0000 C CNN
F 1 "4.7K_125mW_5%" H 5500 3000 50  0000 C CNN
F 2 "ResistorChipSMD:RchipSMD_0805_HandSold" H 5150 2800 50  0001 C CNN
F 3 "" H 5150 3050 30  0000 C CNN
	1    5150 3050
	1    0    0    -1  
$EndComp
$Comp
L J_1x1 J1
U 1 1 59BF9961
P 4850 2800
F 0 "J1" H 4850 2900 50  0000 C CNN
F 1 "J_1x1" H 4850 2700 50  0000 C CNN
F 2 "Jumper:J-1x1-SMD" H 4850 2600 50  0001 C CNN
F 3 "" V 4850 2700 60  0000 C CNN
	1    4850 2800
	1    0    0    -1  
$EndComp
Wire Wire Line
	4200 2800 4700 2800
Connection ~ 4550 2800
$Comp
L GND #PWR0164
U 1 1 59BF9DFE
P 5150 3250
F 0 "#PWR0164" H 5150 3000 50  0001 C CNN
F 1 "GND" H 5150 3100 50  0000 C CNN
F 2 "" H 5150 3250 50  0000 C CNN
F 3 "" H 5150 3250 50  0000 C CNN
	1    5150 3250
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR0165
U 1 1 59BF9EBD
P 4550 3250
F 0 "#PWR0165" H 4550 3000 50  0001 C CNN
F 1 "GND" H 4550 3100 50  0000 C CNN
F 2 "" H 4550 3250 50  0000 C CNN
F 3 "" H 4550 3250 50  0000 C CNN
	1    4550 3250
	1    0    0    -1  
$EndComp
$Comp
L +3V3 #PWR0166
U 1 1 59051662
P 5000 1450
F 0 "#PWR0166" H 5000 1300 50  0001 C CNN
F 1 "+3V3" H 5000 1590 50  0000 C CNN
F 2 "" H 5000 1450 50  0000 C CNN
F 3 "" H 5000 1450 50  0000 C CNN
	1    5000 1450
	1    0    0    -1  
$EndComp
$Comp
L 4.7K_125mW_5% R14
U 1 1 59BFC456
P 5000 1750
F 0 "R14" H 5200 1800 50  0000 C CNN
F 1 "4.7K_125mW_5%" H 5350 1700 50  0000 C CNN
F 2 "ResistorChipSMD:RchipSMD_0805_HandSold" H 5000 1500 50  0001 C CNN
F 3 "" H 5000 1750 30  0000 C CNN
	1    5000 1750
	-1   0    0    -1  
$EndComp
Wire Wire Line
	5000 1950 5000 2300
Connection ~ 5000 2300
Wire Wire Line
	5000 1550 5000 1450
Text Notes 6350 900  0    60   ~ 0
100K -> SN65HVD1176\n560R ~~ 1 K -> SN75176B
Wire Notes Line
	6700 1400 6700 3150
Wire Notes Line
	6700 3150 7100 3150
Wire Notes Line
	7100 3150 7100 1400
Wire Notes Line
	7100 1400 6700 1400
Wire Wire Line
	5150 2150 5150 2850
Wire Wire Line
	5150 2150 5750 2150
Wire Wire Line
	4550 2650 4550 2850
Wire Wire Line
	5000 2800 5150 2800
Connection ~ 5150 2800
Text Notes 8800 1500 0    60   ~ 0
Proteccion 
$Comp
L 0R_125mW_5% R7
U 1 1 59CF2D75
P 3800 4900
F 0 "R7" V 3900 4850 50  0000 C CNN
F 1 "0R_125mW_5%" V 3700 4900 50  0000 C CNN
F 2 "ResistorChipSMD:RchipSMD_0805_HandSold" H 4250 4750 50  0001 C CNN
F 3 "" H 3800 4900 30  0000 C CNN
	1    3800 4900
	0    1    1    0   
$EndComp
Wire Wire Line
	4100 4900 4000 4900
Wire Wire Line
	3200 4900 3600 4900
Wire Notes Line
	8850 1350 8750 1350
Wire Notes Line
	8750 1350 8750 1550
Wire Notes Line
	8750 1550 8850 1550
Wire Notes Line
	8750 1450 8400 1450
Wire Notes Line
	6300 950  7650 950 
Wire Notes Line
	6950 950  6950 1400
Wire Notes Line
	4150 4550 4150 5150
Wire Notes Line
	4150 5150 4450 5150
Wire Notes Line
	4450 5150 4450 4550
Wire Notes Line
	4450 4550 4150 4550
Wire Notes Line
	3750 4100 3750 4200
Wire Notes Line
	3750 4200 5000 4200
Wire Notes Line
	5000 4200 5000 4100
Wire Notes Line
	4300 4500 4300 4200
Wire Notes Line
	8150 1650 7800 1650
Wire Notes Line
	7800 1650 7800 3150
Wire Notes Line
	7800 3150 8150 3150
Wire Notes Line
	8150 3150 8150 1650
Wire Notes Line
	7800 3000 7250 3000
Wire Notes Line
	7250 3000 7250 3500
Wire Notes Line
	6000 3600 6000 3500
Wire Notes Line
	6000 3500 7750 3500
Wire Notes Line
	7750 3500 7750 3600
Text Notes 2600 6750 0    60   ~ 0
 @ note :
Wire Notes Line
	2500 6600 2500 7650
Wire Notes Line
	2500 7650 5950 7650
Wire Notes Line
	5950 7650 5950 6600
Wire Notes Line
	5950 6600 2500 6600
Wire Notes Line
	2500 6800 5950 6800
Wire Notes Line
	6900 1000 6950 950 
Wire Notes Line
	6950 950  7000 1000
Wire Notes Line
	8700 1500 8750 1450
Wire Notes Line
	8750 1450 8700 1400
Wire Notes Line
	7250 3500 7200 3450
Wire Notes Line
	7250 3500 7300 3450
Wire Notes Line
	4300 4200 4250 4250
Wire Notes Line
	4300 4200 4350 4250
Wire Notes Line
	7650 950  7650 850 
Wire Notes Line
	6300 950  6300 850 
$EndSCHEMATC
