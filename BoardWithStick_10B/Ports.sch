EESchema Schematic File Version 2
LIBS:74LVT
LIBS:74xgxx
LIBS:74xx
LIBS:ac-dc
LIBS:actel
LIBS:adc-dac
LIBS:ADC-DeltaSigma
LIBS:ADC-SAR
LIBS:Altera
LIBS:analog_devices
LIBS:AnalogMuxDemux
LIBS:analog_switches
LIBS:AopAudio
LIBS:AopComparador
LIBS:AopPrecision
LIBS:AopRailToRailPG
LIBS:atmel
LIBS:audio
LIBS:battery_management
LIBS:bbd
LIBS:BJT-GeneralPurpose 
LIBS:BOR-SIL
LIBS:BridgeUSB
LIBS:brooktre
LIBS:CapAluminioSMD
LIBS:CapCeramicoHVC
LIBS:CapCeramicos
LIBS:CapCer_THT
LIBS:CapElectroliticos
LIBS:CapPoly_THT
LIBS:CapTantalio
LIBS:Ccer_SMD
LIBS:cmos4000
LIBS:cmos_ieee
LIBS:CommonModeChoke 
LIBS:CompuertasLogicas
LIBS:CON-DIL_F
LIBS:CON-DIL_M
LIBS:Conectores
LIBS:ConectoresDB
LIBS:conn
LIBS:CON-SIL_F
LIBS:CON-SIL_M
LIBS:contrib
LIBS:CON-USB
LIBS:Cristal
LIBS:cypress
LIBS:dc-dc
LIBS:device
LIBS:digital-audio
LIBS:diode
LIBS:DiodoPequeñaSeñal
LIBS:DiodoRectificador
LIBS:DiodoSchottkyRect
LIBS:DiodosSupresorTransitorios
LIBS:DiodosZener
LIBS:display
LIBS:dsp
LIBS:EEpromSerie
LIBS:EIA232
LIBS:EIA485
LIBS:elec-unifil
LIBS:ESD_Protection
LIBS: EsdProtectionDiode
LIBS:FerriteBead
LIBS:FotoDiodos
LIBS:ftdi
LIBS:Fusibles
LIBS:gennum
LIBS:graphic
LIBS:hc11
LIBS:Inductor
LIBS:intel
LIBS:interface
LIBS:ir
LIBS:IRF-MOSFET
LIBS:jumpers
LIBS:Lattice
LIBS:LDR_THT
LIBS:LEDs
LIBS:linear
LIBS:LoadGeneric
LIBS:Logic-MOSFET
LIBS:logo
LIBS:LPC17xx
LIBS:LPC43xx
LIBS:MagneticsProtection
LIBS:maxim
LIBS:memory
LIBS:microchip
LIBS:microchip_dspic33dsc
LIBS:microchip_pic10mcu
LIBS:microchip_pic12mcu
LIBS:microchip_pic16mcu
LIBS:microchip_pic18mcu
LIBS:microchip_pic32mcu
LIBS:microcontrollers
LIBS:motor_drivers
LIBS:motorola
LIBS:msp430
LIBS:nordicsemi
LIBS:NTR-MOSFET
LIBS:nxp_armmcu
LIBS:onsemi
LIBS:opto
LIBS:OptoTransistor
LIBS:OptoTriac
LIBS:Oscillators
LIBS:philips
LIBS:PicoFusePTC
LIBS:Pilas
LIBS:Potenciometro
LIBS:power
LIBS:PowerDistributionSwitch
LIBS:powerint
LIBS:Power_Management
LIBS:pspice
LIBS:Puentes
LIBS:R
LIBS:references
LIBS:RegLinealPositivos
LIBS:regul
LIBS:ReguladorLDO
LIBS:ReguladorSwitcher
LIBS:relays
LIBS:rfcom
LIBS:RMII_Ethernet
LIBS:R_SMD
LIBS:R_ThruHole
LIBS:R_THT
LIBS:SensorCurrent
LIBS:sensors
LIBS:SensorTemperatura
LIBS:ShifterLevel
LIBS:silabs
LIBS:siliconi
LIBS:SourceCurrent
LIBS:StickLPC
LIBS:stm8
LIBS:stm32
LIBS:supertex
LIBS:switches
LIBS:TactSwitch
LIBS:TestPoint
LIBS:texas
LIBS:TrafoReductor
LIBS:TransceiverCAN
LIBS:transf
LIBS:TransistorArrays
LIBS:transistors
LIBS:Triac
LIBS:ttl_ieee
LIBS:valves
LIBS:Varistor
LIBS:video
LIBS:VoltageReference
LIBS:Worldsemi
LIBS:Xicor
LIBS:xilinx
LIBS:xtal
LIBS:Zilog
LIBS:MemoryZock
LIBS:BoardMicro-cache
EELAYER 25 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 18 21
Title "BoardWithStick1300revD"
Date "12-05-2017"
Rev "v1.10b"
Comp "J.E.L"
Comment1 "\\n Luccioni, Jesus Emanuel"
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
Text HLabel 3900 2550 0    60   Input ~ 0
P0.23_ADC0.0
Text HLabel 7850 1650 2    60   BiDi ~ 0
ETH.RD-
Text HLabel 7850 1750 2    60   BiDi ~ 0
ETH.RD+
Text HLabel 7850 1850 2    60   BiDi ~ 0
ETH.TD-
Text HLabel 7850 1950 2    60   BiDi ~ 0
ETH.TD+
Text HLabel 3900 3250 0    60   Input ~ 0
P0.3_RXD0
Text HLabel 8050 5100 2    60   BiDi ~ 0
P1.19_CAP1.1
Text HLabel 8050 5200 2    60   BiDi ~ 0
P1.20_PWM1.2
Text HLabel 8050 5400 2    60   Input ~ 0
P1.22_MAT1.0
Text HLabel 8050 5500 2    60   BiDi ~ 0
P1.23_GPIO1.23
Text HLabel 3800 5750 0    60   Input ~ 0
P1.29_PCAP1.1
Text HLabel 8050 5900 2    60   BiDi ~ 0
P1.27_CAP0.1
Text HLabel 7850 3150 2    60   Output ~ 0
P2.5_GPIO2.5
Text HLabel 7850 3250 2    60   Output ~ 0
P2.6_GPIO2.6
Text HLabel 7850 3350 2    60   Output ~ 0
P2.7_GPIO2.7
Text HLabel 7850 3450 2    60   Output ~ 0
P2.8_GPIO2.8
Text HLabel 3900 3150 0    60   Output ~ 0
P0.2_TXD0
Text HLabel 3900 2950 0    60   Input ~ 0
P1.30_VBUS
Text HLabel 3900 1750 0    60   BiDi ~ 0
P0.7_SCK1
Text HLabel 3900 1850 0    60   BiDi ~ 0
P0.6_SSEL1
Text HLabel 7850 2850 2    60   Output ~ 0
P2.2_GPIO2.2
Text HLabel 7850 2950 2    60   Output ~ 0
P2.3_PWM1.4
Text HLabel 7850 3050 2    60   Output ~ 0
P2.4_PWM1.5
Text HLabel 7850 2250 2    60   BiDi ~ 0
P0.4_GPIO0.4
Text HLabel 8050 5600 2    60   BiDi ~ 0
P1.24_GPIO1.24
Text HLabel 8050 5700 2    60   BiDi ~ 0
P1.25_GPIO1.25
Text HLabel 8050 5800 2    60   Input ~ 0
P1.26_CAP0.0
Text HLabel 8050 5000 2    60   Output ~ 0
P1.18_PWM1.1
Text HLabel 7850 3750 2    60   Input ~ 0
P2.12_EINT2
Text HLabel 3800 5450 0    60   BiDi ~ 0
P4.28_GPIO4.28
Text HLabel 3800 5350 0    60   BiDi ~ 0
P4.29_GPIO4.29
Text HLabel 7850 2650 2    60   Output ~ 0
P2.0_GPIO2.0
Text HLabel 7850 2750 2    60   Output ~ 0
P2.1_GPIO2.1
Text HLabel 3900 2050 0    60   BiDi ~ 0
P0.1_CAN1.TDX
Text HLabel 3900 2250 0    60   3State ~ 0
P0.17_GPIO0.17
Text HLabel 3800 5050 0    60   BiDi ~ 0
P2.9_GPIO2.9
Text HLabel 3900 2650 0    60   Input ~ 0
P0.24_ADC0.1
Text HLabel 3900 2750 0    60   Input ~ 0
P0.25_ADC0.2
Text HLabel 3900 2850 0    60   BiDi ~ 0
P0.26_ADC0.3_AOUT_RXD3
Text HLabel 3900 3550 0    60   BiDi ~ 0
P0.27_I2C0.SDA
Text HLabel 3900 3650 0    60   BiDi ~ 0
P0.28_I2C0.SCL
Text HLabel 3900 2150 0    60   BiDi ~ 0
P0.18_GPIO0.18
Text HLabel 3900 2350 0    60   Output ~ 0
P0.15_UART1.TX
Text HLabel 3900 2450 0    60   Input ~ 0
P0.16_UART1.RX
Text HLabel 7850 3650 2    60   Input ~ 0
P2.11_EINT1
Text HLabel 3900 3750 0    60   Input ~ 0
P2.13_EINT3
Text HLabel 3900 1950 0    60   BiDi ~ 0
P0.0_CAN1.RDX
Text HLabel 3900 1550 0    60   BiDi ~ 0
P0.9_MOSI1
Text HLabel 3900 1650 0    60   BiDi ~ 0
P0.8_MISO1
Text HLabel 7850 2050 2    60   BiDi ~ 0
P0.30_USB_D-
Text HLabel 7850 2150 2    60   BiDi ~ 0
P0.29_USB_D+
Text HLabel 3900 3050 0    60   BiDi ~ 0
P1.31_ADC0.5
Text HLabel 7850 2350 2    60   BiDi ~ 0
P0.5_GPIO0.5
Text HLabel 3900 3350 0    60   BiDi ~ 0
P0.21_GPIO0.21
Text GLabel 8550 3550 2    60   Input ~ 0
ISP
Text GLabel 2950 5850 0    60   Output ~ 0
HabMedPila
$Comp
L OM13000 STICK1
U 3 1 57D3810A
P 5900 2750
F 0 "STICK1" H 5900 2600 60  0000 C CNN
F 1 "OM13000" H 5900 2750 79  0000 C CNB
F 2 "StickTHT:OM13000-THT" H 5740 2970 60  0001 C CNN
F 3 "" H 5740 2970 60  0000 C CNN
	3    5900 2750
	1    0    0    -1  
$EndComp
Wire Wire Line
	4000 3150 3900 3150
Wire Wire Line
	3900 3250 4000 3250
Wire Wire Line
	4000 3350 3900 3350
Wire Wire Line
	3900 3450 4000 3450
Wire Wire Line
	4000 1550 3900 1550
Wire Wire Line
	3900 1650 4000 1650
Wire Wire Line
	4000 1750 3900 1750
Wire Wire Line
	3900 1850 4000 1850
Wire Wire Line
	4000 2550 3900 2550
Wire Wire Line
	3900 2650 4000 2650
Wire Wire Line
	4000 2750 3900 2750
Wire Wire Line
	3900 2850 4000 2850
Wire Wire Line
	3900 3550 4000 3550
Wire Wire Line
	4000 3650 3900 3650
Wire Wire Line
	4000 2450 3900 2450
Wire Wire Line
	4000 2350 3900 2350
Wire Wire Line
	4000 2250 3900 2250
Wire Wire Line
	4000 2150 3900 2150
Wire Wire Line
	4000 2050 3900 2050
Wire Wire Line
	4000 1950 3900 1950
Wire Wire Line
	7850 2350 7750 2350
Wire Wire Line
	7850 2250 7750 2250
Wire Wire Line
	7850 2050 7750 2050
Wire Wire Line
	7750 2150 7850 2150
Wire Wire Line
	7850 2750 7750 2750
Wire Wire Line
	7750 2850 7850 2850
Wire Wire Line
	7850 2950 7750 2950
Wire Wire Line
	7750 3050 7850 3050
Wire Wire Line
	7850 3150 7750 3150
Wire Wire Line
	7750 3250 7850 3250
Wire Wire Line
	7850 3350 7750 3350
Wire Wire Line
	7750 3450 7850 3450
Wire Wire Line
	7850 2650 7750 2650
Wire Wire Line
	7850 3650 7750 3650
Wire Wire Line
	7850 3750 7750 3750
Wire Wire Line
	3900 3750 4000 3750
Wire Wire Line
	7750 3550 8550 3550
Wire Wire Line
	4000 3050 3900 3050
Wire Wire Line
	3900 2950 4000 2950
Wire Wire Line
	7850 1650 7750 1650
Wire Wire Line
	7750 1750 7850 1750
Wire Wire Line
	7850 1850 7750 1850
Wire Wire Line
	7750 1950 7850 1950
$Comp
L OM13000 STICK1
U 4 1 57D49C25
P 5900 5450
F 0 "STICK1" H 5900 5300 60  0000 C CNN
F 1 "OM13000" H 5900 5450 79  0000 C CNB
F 2 "StickTHT:OM13000-THT" H 5740 5670 60  0001 C CNN
F 3 "" H 5740 5670 60  0000 C CNN
	4    5900 5450
	1    0    0    -1  
$EndComp
Wire Wire Line
	3900 5350 3800 5350
Wire Wire Line
	3800 5450 3900 5450
Wire Wire Line
	3800 5550 3900 5550
Wire Wire Line
	3900 5650 3800 5650
Wire Wire Line
	3900 5050 3800 5050
Wire Wire Line
	3900 5750 3800 5750
Wire Wire Line
	3900 5850 2950 5850
Wire Wire Line
	8050 5000 7950 5000
Wire Wire Line
	8050 5100 7950 5100
Wire Wire Line
	7950 5200 8050 5200
Wire Wire Line
	8050 5300 7950 5300
Wire Wire Line
	7950 5400 8050 5400
Wire Wire Line
	8050 5500 7950 5500
Wire Wire Line
	8050 5600 7950 5600
Wire Wire Line
	7950 5700 8050 5700
Wire Wire Line
	8050 5800 7950 5800
Wire Wire Line
	7950 5900 8050 5900
Text HLabel 7850 2450 2    60   BiDi ~ 0
P0.10_UART2.TX_I2C2.SDA
Text HLabel 7850 2550 2    60   BiDi ~ 0
P0.11_UART2.RX_I2C2.SCL
NoConn ~ 3900 5150
NoConn ~ 3900 5250
Text Notes 2750 5250 0    60   ~ 0
Use EEPROM Over Stick
Wire Wire Line
	7850 2450 7750 2450
Wire Wire Line
	7850 2550 7750 2550
Text Notes 1250 6500 0    60   ~ 0
PCAP1.1 : Entrada de captura \npara PWM1.1
NoConn ~ 3900 3450
NoConn ~ 8050 5300
NoConn ~ 3800 5550
NoConn ~ 3800 5650
Text Notes 2800 5650 0    50   ~ 0
Led RGB, Sobre el Stick
Text Notes 2800 3500 0    50   ~ 0
Led RGB, Sobre el Stick
$EndSCHEMATC
