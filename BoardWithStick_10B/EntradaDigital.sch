EESchema Schematic File Version 2
LIBS:74LVT
LIBS:74xgxx
LIBS:74xx
LIBS:ac-dc
LIBS:actel
LIBS:adc-dac
LIBS:ADC-DeltaSigma
LIBS:ADC-SAR
LIBS:Altera
LIBS:analog_devices
LIBS:AnalogMuxDemux
LIBS:analog_switches
LIBS:AopAudio
LIBS:AopComparador
LIBS:AopPrecision
LIBS:AopRailToRailPG
LIBS:atmel
LIBS:audio
LIBS:battery_management
LIBS:bbd
LIBS:BJT-GeneralPurpose 
LIBS:BOR-SIL
LIBS:BridgeUSB
LIBS:brooktre
LIBS:CapAluminioSMD
LIBS:CapCeramicoHVC
LIBS:CapCeramicos
LIBS:CapCer_THT
LIBS:CapElectroliticos
LIBS:CapPoly_THT
LIBS:CapTantalio
LIBS:Ccer_SMD
LIBS:cmos4000
LIBS:cmos_ieee
LIBS:CommonModeChoke 
LIBS:CompuertasLogicas
LIBS:CON-DIL_F
LIBS:CON-DIL_M
LIBS:Conectores
LIBS:ConectoresDB
LIBS:conn
LIBS:CON-SIL_F
LIBS:CON-SIL_M
LIBS:contrib
LIBS:CON-USB
LIBS:Cristal
LIBS:cypress
LIBS:dc-dc
LIBS:device
LIBS:digital-audio
LIBS:diode
LIBS:DiodoPequeñaSeñal
LIBS:DiodoRectificador
LIBS:DiodoSchottkyRect
LIBS:DiodosSupresorTransitorios
LIBS:DiodosZener
LIBS:display
LIBS:dsp
LIBS:EEpromSerie
LIBS:EIA232
LIBS:EIA485
LIBS:elec-unifil
LIBS:ESD_Protection
LIBS: EsdProtectionDiode
LIBS:FerriteBead
LIBS:FotoDiodos
LIBS:ftdi
LIBS:Fusibles
LIBS:gennum
LIBS:graphic
LIBS:hc11
LIBS:Inductor
LIBS:intel
LIBS:interface
LIBS:ir
LIBS:IRF-MOSFET
LIBS:jumpers
LIBS:Lattice
LIBS:LDR_THT
LIBS:LEDs
LIBS:linear
LIBS:LoadGeneric
LIBS:Logic-MOSFET
LIBS:logo
LIBS:LPC17xx
LIBS:LPC43xx
LIBS:MagneticsProtection
LIBS:maxim
LIBS:memory
LIBS:microchip
LIBS:microchip_dspic33dsc
LIBS:microchip_pic10mcu
LIBS:microchip_pic12mcu
LIBS:microchip_pic16mcu
LIBS:microchip_pic18mcu
LIBS:microchip_pic32mcu
LIBS:microcontrollers
LIBS:motor_drivers
LIBS:motorola
LIBS:msp430
LIBS:nordicsemi
LIBS:NTR-MOSFET
LIBS:nxp_armmcu
LIBS:onsemi
LIBS:opto
LIBS:OptoTransistor
LIBS:OptoTriac
LIBS:Oscillators
LIBS:philips
LIBS:PicoFusePTC
LIBS:Pilas
LIBS:Potenciometro
LIBS:power
LIBS:PowerDistributionSwitch
LIBS:powerint
LIBS:Power_Management
LIBS:pspice
LIBS:Puentes
LIBS:R
LIBS:references
LIBS:RegLinealPositivos
LIBS:regul
LIBS:ReguladorLDO
LIBS:ReguladorSwitcher
LIBS:relays
LIBS:rfcom
LIBS:RMII_Ethernet
LIBS:R_SMD
LIBS:R_ThruHole
LIBS:R_THT
LIBS:SensorCurrent
LIBS:sensors
LIBS:SensorTemperatura
LIBS:ShifterLevel
LIBS:silabs
LIBS:siliconi
LIBS:SourceCurrent
LIBS:StickLPC
LIBS:stm8
LIBS:stm32
LIBS:supertex
LIBS:switches
LIBS:TactSwitch
LIBS:TestPoint
LIBS:texas
LIBS:TrafoReductor
LIBS:TransceiverCAN
LIBS:transf
LIBS:TransistorArrays
LIBS:transistors
LIBS:Triac
LIBS:ttl_ieee
LIBS:valves
LIBS:Varistor
LIBS:video
LIBS:VoltageReference
LIBS:Worldsemi
LIBS:Xicor
LIBS:xilinx
LIBS:xtal
LIBS:Zilog
LIBS:MemoryZock
LIBS:BoardMicro-cache
EELAYER 25 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 12 21
Title "BoardWithStick1300revD"
Date "12-05-2017"
Rev "v1.10b"
Comp "J.E.L"
Comment1 "\\n Luccioni, Jesus Emanuel"
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
Text HLabel 6450 2400 2    60   Output ~ 0
InpOpto.1
Text HLabel 6450 2700 2    60   Output ~ 0
InpOpto.2
Text HLabel 6450 3000 2    60   Output ~ 0
InpOpto.3
$Comp
L +3V3 #PWR0109
U 1 1 58208378
P 5750 5900
F 0 "#PWR0109" H 5750 5750 50  0001 C CNN
F 1 "+3V3" H 5750 6040 50  0000 C CNN
F 2 "" H 5750 5900 50  0000 C CNN
F 3 "" H 5750 5900 50  0000 C CNN
	1    5750 5900
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR0110
U 1 1 57A33720
P 5750 6700
F 0 "#PWR0110" H 5750 6450 50  0001 C CNN
F 1 "GND" H 5750 6550 50  0000 C CNN
F 2 "" H 5750 6700 50  0000 C CNN
F 3 "" H 5750 6700 50  0000 C CNN
	1    5750 6700
	1    0    0    -1  
$EndComp
$Comp
L 100nF_10V_X7R C49
U 1 1 57A35A71
P 6250 6300
F 0 "C49" H 6400 6350 50  0000 C CNN
F 1 "100nF_10V_X7R" H 6650 6250 50  0000 C CNN
F 2 "CapacitorCeramicSMD:CcSMD_0805_HandSold" H 6500 6150 50  0001 C CNN
F 3 "" H 6250 6300 60  0000 C CNN
	1    6250 6300
	1    0    0    -1  
$EndComp
$Comp
L SN74LV05 U5
U 1 1 57D5C9B4
P 4850 2400
F 0 "U5" H 5150 2550 50  0000 C CNN
F 1 "SN74LV05" H 5450 2550 50  0001 C CNN
F 2 "SOIC:SOIC14_3.9x8.7mm_HandSold" H 4850 2200 50  0001 C CNN
F 3 "" H 5385 2435 60  0000 C CNN
	1    4850 2400
	1    0    0    -1  
$EndComp
$Comp
L SN74LV05 U5
U 5 1 57D5CB31
P 5250 4450
F 0 "U5" H 5350 4600 50  0000 C CNN
F 1 "SN74LV05" H 5700 4600 50  0000 C CNN
F 2 "SOIC:SOIC14_3.9x8.7mm_HandSold" H 5250 4250 50  0001 C CNN
F 3 "" H 5785 4485 60  0000 C CNN
	5    5250 4450
	1    0    0    -1  
$EndComp
$Comp
L SN74LV05 U5
U 2 1 57D5CC83
P 5250 2700
F 0 "U5" H 5650 2800 50  0000 C CNN
F 1 "SN74LV05" H 6000 2800 50  0001 C CNN
F 2 "SOIC:SOIC14_3.9x8.7mm_HandSold" H 5250 2500 50  0001 C CNN
F 3 "" H 5785 2735 60  0000 C CNN
	2    5250 2700
	1    0    0    -1  
$EndComp
$Comp
L 10K_125mW_5% R52
U 1 1 57D5D4D1
P 5800 1950
F 0 "R52" V 5800 2400 50  0000 C CNN
F 1 "10K_125mW_5%" V 5800 2900 50  0000 C CNN
F 2 "ResistorChipSMD:RchipSMD_0805_HandSold" H 6000 1900 50  0001 C CNN
F 3 "" H 5800 1950 30  0000 C CNN
	1    5800 1950
	1    0    0    -1  
$EndComp
$Comp
L +3V3 #PWR0111
U 1 1 57D5DD69
P 6500 1400
F 0 "#PWR0111" H 6500 1250 50  0001 C CNN
F 1 "+3V3" H 6500 1540 50  0000 C CNN
F 2 "" H 6500 1400 50  0000 C CNN
F 3 "" H 6500 1400 50  0000 C CNN
	1    6500 1400
	1    0    0    -1  
$EndComp
$Comp
L SN74LV05 U5
U 7 1 5820837C
P 5750 6300
F 0 "U5" H 6050 6450 50  0000 C CNN
F 1 "SN74LV05" H 6150 6150 50  0000 C CNN
F 2 "SOIC:SOIC14_3.9x8.7mm_HandSold" H 5750 6100 50  0001 C CNN
F 3 "" H 6285 6335 60  0000 C CNN
	7    5750 6300
	-1   0    0    -1  
$EndComp
$Comp
L SN74LV05 U5
U 6 1 5820837D
P 4850 4150
F 0 "U5" H 5150 4300 50  0000 C CNN
F 1 "SN74LV05" H 5550 4300 50  0001 C CNN
F 2 "SOIC:SOIC14_3.9x8.7mm_HandSold" H 4850 3950 50  0001 C CNN
F 3 "" H 5385 4185 60  0000 C CNN
	6    4850 4150
	1    0    0    -1  
$EndComp
$Comp
L SN74LV05 U5
U 3 1 5820837F
P 4850 3000
F 0 "U5" H 5100 2900 50  0000 C CNN
F 1 "SN74LV05" H 5500 2900 50  0000 C CNN
F 2 "SOIC:SOIC14_3.9x8.7mm_HandSold" H 4850 2800 50  0001 C CNN
F 3 "" H 5385 3035 60  0000 C CNN
	3    4850 3000
	1    0    0    -1  
$EndComp
Text HLabel 6450 4150 2    60   Output ~ 0
InpOpto.4
Text HLabel 6450 4450 2    60   Output ~ 0
InpOpto.5
$Comp
L BORx06-PITCH5mm CN10
U 1 1 581F379E
P 2100 4450
F 0 "CN10" H 1600 4550 50  0000 C CNN
F 1 "OptosInpCh4-6" H 1550 4450 50  0000 C CNN
F 2 "Connectors_Phoenix:PhoenixContact_MCV-G_06x3.81mm_Vertical" H 2100 3650 50  0001 C CNN
F 3 "" H 2150 4550 60  0001 C CNN
F 4 "EntradaOptoaislada" H 1500 4350 60  0001 C CNN "Name"
	1    2100 4450
	1    0    0    -1  
$EndComp
$Comp
L SN74LV05 U5
U 4 1 5820837E
P 4850 4750
F 0 "U5" H 4900 4950 50  0000 C CNN
F 1 "SN74LV05" H 5250 4600 50  0001 C CNN
F 2 "SOIC:SOIC14_3.9x8.7mm_HandSold" H 4850 4550 50  0001 C CNN
F 3 "" H 5385 4785 60  0000 C CNN
	4    4850 4750
	1    0    0    -1  
$EndComp
$Sheet
S 2650 2000 1500 1450
U 581C39C0
F0 "OptosEntrada_1" 60
F1 "OptoInputs_1.sch" 60
F2 "OptInp.CH1[in]" I L 2650 2200 50 
F3 "OptInp.CH2[in]" I L 2650 2600 50 
F4 "OptInp.CH1[ou]" O R 4150 2400 50 
F5 "OptInp.CH2[ou]" O R 4150 2700 50 
F6 "OptInp.CH1[COM]" I L 2650 2400 50 
F7 "OptInp.CH2[COM]" I L 2650 2800 60 
F8 "OptInp.CH3[in]" I L 2650 3000 50 
F9 "OptInp.CH3[ou]" O R 4150 3000 50 
F10 "OptInp.CH3[COM]" I L 2650 3200 50 
$EndSheet
$Sheet
S 2650 3750 1500 1450
U 581C3A6D
F0 "OptosEntrada_2" 60
F1 "OptoInputs_2.sch" 60
F2 "OptInp.CH4[in]" I L 2650 3950 50 
F3 "OptInp.CH5[in]" I L 2650 4350 50 
F4 "OptInp.CH4[ou]" O R 4150 4150 50 
F5 "OptInp.CH5[ou]" O R 4150 4450 50 
F6 "OptInp.CH4[COM]" I L 2650 4150 50 
F7 "OptInp.CH5[COM]" I L 2650 4550 50 
F8 "OptInp.CH6[in]" I L 2650 4750 50 
F9 "OptInp.CH6[ou]" O R 4150 4750 50 
F10 "OptInp.CH6[COM]" I L 2650 4950 50 
$EndSheet
$Comp
L BORx06-PITCH5mm CN9
U 1 1 582159EE
P 2100 2700
F 0 "CN9" H 1550 2850 50  0000 C CNN
F 1 "OptosInpCh1-3" H 1550 2750 50  0000 C CNN
F 2 "Connectors_Phoenix:PhoenixContact_MCV-G_06x3.81mm_Vertical" H 2100 1900 50  0001 C CNN
F 3 "" H 2150 2800 60  0001 C CNN
F 4 "EntradaOptoaislada" H 1500 2650 60  0001 C CNN "Name"
	1    2100 2700
	1    0    0    -1  
$EndComp
Text HLabel 6450 4750 2    60   Output ~ 0
InpOpto.6
Wire Wire Line
	5200 2400 6450 2400
Wire Wire Line
	5600 2700 6450 2700
Wire Wire Line
	5200 3000 6450 3000
Wire Wire Line
	4150 2400 4500 2400
Wire Wire Line
	4150 3000 4500 3000
Wire Wire Line
	5200 4750 6450 4750
Wire Wire Line
	5750 6600 5750 6700
Wire Wire Line
	5750 5900 5750 6000
Wire Wire Line
	6250 6200 6250 5950
Wire Wire Line
	6250 5950 5750 5950
Connection ~ 5750 5950
Wire Wire Line
	6250 6400 6250 6650
Wire Wire Line
	6250 6650 5750 6650
Connection ~ 5750 6650
Wire Wire Line
	4150 2700 4900 2700
Wire Wire Line
	6000 2150 6000 3000
Connection ~ 6000 3000
Wire Wire Line
	5900 2150 5900 2700
Connection ~ 5900 2700
Wire Wire Line
	5800 2150 5800 2400
Connection ~ 5800 2400
Wire Wire Line
	6500 1650 6500 1400
Wire Wire Line
	5800 1650 6500 1650
Wire Wire Line
	5800 1650 5800 1750
Wire Wire Line
	5900 1750 5900 1650
Connection ~ 5900 1650
Wire Wire Line
	6000 1750 6000 1650
Connection ~ 6000 1650
Wire Wire Line
	2650 2200 2400 2200
Wire Wire Line
	2400 2400 2650 2400
Wire Wire Line
	2650 2600 2400 2600
Wire Wire Line
	2400 2800 2650 2800
Wire Wire Line
	2650 3000 2400 3000
Wire Wire Line
	2400 3200 2650 3200
Wire Wire Line
	5600 4450 6450 4450
Wire Wire Line
	2650 3950 2400 3950
Wire Wire Line
	2400 4150 2650 4150
Wire Wire Line
	2650 4350 2400 4350
Wire Wire Line
	2400 4550 2650 4550
Wire Wire Line
	6100 1750 6100 1650
Connection ~ 6100 1650
Wire Wire Line
	6200 1750 6200 1650
Connection ~ 6200 1650
Wire Wire Line
	6300 1750 6300 1650
Connection ~ 6300 1650
Wire Wire Line
	5200 4150 6450 4150
Wire Wire Line
	4150 4450 4900 4450
Wire Wire Line
	4500 4150 4150 4150
Wire Wire Line
	4500 4750 4150 4750
Wire Wire Line
	6300 2150 6300 4750
Connection ~ 6300 4750
Wire Wire Line
	6200 2150 6200 4450
Connection ~ 6200 4450
Wire Wire Line
	6100 2150 6100 4150
Connection ~ 6100 4150
Wire Wire Line
	2400 4750 2650 4750
Wire Wire Line
	2650 4950 2400 4950
$Comp
L 10K_125mW_5% R53
U 1 1 590804AF
P 5900 1950
F 0 "R53" V 5900 2400 50  0000 C CNN
F 1 "10K_125mW_5%" V 5900 2900 50  0000 C CNN
F 2 "ResistorChipSMD:RchipSMD_0805_HandSold" H 6100 1900 50  0001 C CNN
F 3 "" H 5900 1950 30  0000 C CNN
	1    5900 1950
	1    0    0    -1  
$EndComp
$Comp
L 10K_125mW_5% R54
U 1 1 5908054F
P 6000 1950
F 0 "R54" V 6000 2400 50  0000 C CNN
F 1 "10K_125mW_5%" V 6000 2900 50  0000 C CNN
F 2 "ResistorChipSMD:RchipSMD_0805_HandSold" H 6200 1900 50  0001 C CNN
F 3 "" H 6000 1950 30  0000 C CNN
	1    6000 1950
	1    0    0    -1  
$EndComp
$Comp
L 10K_125mW_5% R55
U 1 1 590805F6
P 6100 1950
F 0 "R55" V 6100 2400 50  0000 C CNN
F 1 "10K_125mW_5%" V 6100 2900 50  0000 C CNN
F 2 "ResistorChipSMD:RchipSMD_0805_HandSold" H 6300 1900 50  0001 C CNN
F 3 "" H 6100 1950 30  0000 C CNN
	1    6100 1950
	1    0    0    -1  
$EndComp
$Comp
L 10K_125mW_5% R56
U 1 1 5908069C
P 6200 1950
F 0 "R56" V 6200 2400 50  0000 C CNN
F 1 "10K_125mW_5%" V 6200 2900 50  0000 C CNN
F 2 "ResistorChipSMD:RchipSMD_0805_HandSold" H 6400 1900 50  0001 C CNN
F 3 "" H 6200 1950 30  0000 C CNN
	1    6200 1950
	1    0    0    -1  
$EndComp
$Comp
L 10K_125mW_5% R57
U 1 1 59080745
P 6300 1950
F 0 "R57" V 6300 2400 50  0000 C CNN
F 1 "10K_125mW_5%" V 6300 2900 50  0000 C CNN
F 2 "ResistorChipSMD:RchipSMD_0805_HandSold" H 6500 1900 50  0001 C CNN
F 3 "" H 6300 1950 30  0000 C CNN
	1    6300 1950
	1    0    0    -1  
$EndComp
$EndSCHEMATC
